﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using Service;
using System.IO;
using Semawed;
using System.Text.RegularExpressions;
using System.Xml;
using System.Net;
using System.Xml.Xsl;
using System.Xml.Linq;
using System.Runtime.ExceptionServices;

namespace ServiceHost
{
    class Program
    {
        static void Main(string[] args)
        {
            Task t = null;
            while (true)
            {
                try
                {
                    t = Task.Run(new Action(ServiceRunner));
                    t.Wait();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Service have been fallen with exception: " + ex.Message);
                    if (ex.InnerException != null)
                        Console.WriteLine("inner exception: " + ex.InnerException.Message);
                    if (t != null)
                        t.Dispose();
                    Thread.Sleep(10000);
                }
            }
        }

        static void ServiceRunner()
        {
            WebServiceHost host = new WebServiceHost(typeof(Searcher), new Uri("http://localhost:8200/"));
            try
            {
                WebHttpBinding binding = new WebHttpBinding();
                binding.MaxBufferSize = Int32.MaxValue;
                binding.MaxReceivedMessageSize = Int32.MaxValue;
                ServiceEndpoint ep = host.AddServiceEndpoint(typeof(ISearcher), binding, "http://localhost:8200/");
                ep.Binding.OpenTimeout = new TimeSpan(0, 2, 0);
                ep.Binding.ReceiveTimeout = new TimeSpan(0, 2, 0);
                ep.Binding.SendTimeout = new TimeSpan(0, 2, 0);
                ep.Binding.CloseTimeout = new TimeSpan(0, 2, 0);
                host.Open();

                Console.WriteLine("Service is running...");
                bool serviceRunning = true;
                while (serviceRunning)
                {
                    string command = Console.ReadLine();
                    DateTime before = DateTime.Now;
                    switch (command)
                    {
                        case "Stop":
                            serviceRunning = false;
                            break;
                        case "IndexThem":
                            IndexThem();
                            Console.WriteLine("Done in " + (DateTime.Now - before).TotalMilliseconds);
                            break;
                        case "IndexWolfram":
                            Console.WriteLine("Index wolfram perofrming...");
                            IndexWolfram("result2.txt");
                            IndexWolfram("result1.txt");
                            Console.WriteLine("Done in " + (DateTime.Now - before).TotalMilliseconds);
                            break;
                        case "MassTest":
                            Console.WriteLine("MassTest performing...");
                            MassTest();
                            Console.WriteLine("Done in " + (DateTime.Now - before).TotalMilliseconds);
                            break;
                        case "ClearCache":
                            Console.WriteLine("ClearCache performing...");
                            string returnString = new Searcher().ClearCache("61D43594-3623-46FF-A9AB-07011A0FDD71");
                            Console.WriteLine("Done with return string: " + returnString);
                            break;
                        case "Init":
                            Console.WriteLine("Init performing...");
                            new Searcher().Init();
                            Console.WriteLine("Done in " + (DateTime.Now - before).TotalMilliseconds);
                            break;
                        case "Query":
                            Console.WriteLine("Input Query\r\n>");
                            string query = Console.ReadLine();
                            Searcher s = new Searcher();
                            var results = s.S(query);
                            Console.WriteLine("<ol type=\"1\">");
                            foreach (var result in results.Results)
                            {
                                Console.WriteLine("<li class=\"borderlist\">Weight: " + result.Weight + " $$" + result.Presentation + "$$ " + result.Proof + "</li>");
                            }
                            Console.WriteLine("Next results...");
                            var nextresults = s.Search(query, 10, 20);
                            foreach (var result in nextresults.Results)
                            {
                                Console.WriteLine("<li class=\"borderlist\">Weight: " + result.Weight + " $$" + result.Presentation + "$$ " + result.Proof + "</li>");
                            }
                            Console.WriteLine("</ol>");

                            break;
                        case "CreateRules":
                            Console.WriteLine("Creating rules...");
                            WaveRuleCreation();
                            Console.WriteLine("Done");
                            break;
                        case "CheckEqualAndInclusion":
                            Expression e1 = new Expression("<math><apply><sin/><apply><plus/><ci>z</ci><apply><times/><ci>n</ci><ci>x</ci></apply></apply></apply></math>");
                            Expression e2 = new Expression("<math><apply><plus/><apply><times/><ci>k</ci><ci>x</ci></apply><ci>b</ci></apply></math>");
                            SearchEntry e = new SearchEntry(e1, e2, "dummy", "dummy", "dummy");
                            Console.WriteLine(e1);
                            Console.WriteLine(e2);
                            Console.WriteLine(e.CheckEqual() + " done");
                            break;
                        case "IndexTeXDump":
                            Console.WriteLine("IndexTeXDump...");
                            IndexTeXDump();
                            break;
                        case "ImproveTest":
                            Console.WriteLine("Improve test performing...");
                            ImproveTest();
                            break;
                        case "MassImproveTest":
                            Console.WriteLine("Mass improve test performing...");
                            MassImproveTest();
                            break;
                        case "Final":
                            Console.WriteLine("Final performing...");
                            IndexTeXDumpWithPosDescription();
                            break;
                        case "IndexGenerated":
                            Console.WriteLine("IndexGenerated performing...");
                            IndexGenerated();
                            Console.WriteLine("Finished");
                            break;
                        default:
                            Console.WriteLine("Command not recognized");
                            continue;
                    }
                }
                Console.WriteLine("Press <ENTER> to terminate");
                Console.ReadLine();
                host.Close();
            }
            catch (CommunicationException cex)
            {
                Console.WriteLine("An exception occurred: {0}", cex.Message);
                host.Abort();
                Console.ReadLine();
            }
        }

        public static void WaveRuleCreation()
        {
            string path = "rules";
            try
            {
                Directory.Delete(path, true);
            }
            catch { }
            Directory.CreateDirectory(path);
            int a = WaveRule.CreateWaveRules("WR/0divide1.txt", "WR/0divide2.txt", "divide1", "divide2", path, false);
            int b = WaveRule.CreateWaveRules("WR/0divide2.txt", "WR/0divide1.txt", "divide2", "divide1", path, false);
            Console.WriteLine(a + " " + b);
            WaveRule.CreateWaveRules("WR/1sumcommutativity1.txt", "WR/1sumcommutativity2.txt", "sumcommut1", "sumcommut2", path, true);
            WaveRule.CreateWaveRules("WR/1sumcommutativity2.txt", "WR/1sumcommutativity1.txt", "sumcommut2", "sumcommut1", path, true);
            WaveRule.CreateWaveRules("WR/2sumassoc1.txt", "WR/2sumassoc2.txt", "sumassoc1", "sumassoc2", path, false);
            WaveRule.CreateWaveRules("WR/2sumassoc2.txt", "WR/2sumassoc1.txt", "sumassoc2", "sumassoc1", path, false);
            WaveRule.CreateWaveRules("WR/3zero1.txt", "WR/3zero2.txt", "zero1", "zero2", path, true);
            WaveRule.CreateWaveRules("WR/3zero2.txt", "WR/3zero1.txt", "zero2", "zero1", path, true);
            WaveRule.CreateWaveRules("WR/4suminverse1.txt", "WR/4suminverse2.txt", "suminverse1", "suminverse2", path, true);
            WaveRule.CreateWaveRules("WR/4suminverse2.txt", "WR/4suminverse1.txt", "suminverse2", "suminverse1", path, true);
            WaveRule.CreateWaveRules("WR/5mulcommutativity1.txt", "WR/5mulcommutativity2.txt", "mulcommut1", "mulcommut2", path, true);
            WaveRule.CreateWaveRules("WR/5mulcommutativity2.txt", "WR/5mulcommutativity1.txt", "mulcommut2", "mulcommut1", path, true);
            WaveRule.CreateWaveRules("WR/6mulassoc1.txt", "WR/6mulassoc2.txt", "mulassoc1", "mulassoc2", path, false);
            WaveRule.CreateWaveRules("WR/6mulassoc2.txt", "WR/6mulassoc1.txt", "mulassoc2", "mulassoc1", path, false);
            WaveRule.CreateWaveRules("WR/7one1.txt", "WR/7one2.txt", "one1", "one2", path, true);
            WaveRule.CreateWaveRules("WR/7one2.txt", "WR/7one1.txt", "one2", "one1", path, true);
            WaveRule.CreateWaveRules("WR/8mulinverse1.txt", "WR/8mulinverse2.txt", "mulinverse1", "mulinverse2", path, true);
            WaveRule.CreateWaveRules("WR/8mulinverse1_anotherform.txt", "WR/8mulinverse2.txt", "mulinverse1_anotherform", "mulinverse2_anotherform", path, true);
            WaveRule.CreateWaveRules("WR/8mulinverse2.txt", "WR/8mulinverse1.txt", "mulinverse2", "mulinverse1", path, true);
            WaveRule.CreateWaveRules("WR/8mulinverse2.txt", "WR/8mulinverse1_anotherform.txt", "mulinverse2_anotherform", "mulinverse1_anotherform", path, true);

            WaveRule.CreateWaveRules("WR/9distrib1.txt", "WR/9distrib2.txt", "distrib1", "distrib2", path, false);
            WaveRule.CreateWaveRules("WR/9distrib2.txt", "WR/9distrib1.txt", "distrib2", "distrib1", path, false);
            WaveRule.CreateWaveRules("WR/10distribL1.txt", "WR/10distribL2.txt", "distribL1", "distribL2", path, false);
            WaveRule.CreateWaveRules("WR/10distribL2.txt", "WR/10distribL1.txt", "distribL2", "distribL1", path, false);

            WaveRule.CreateWaveRules("WR/11eq1.txt", "WR/11eqL2.txt", "eqL1", "eqL2", path, true);
            WaveRule.CreateWaveRules("WR/11eqL2.txt", "WR/11eq1.txt", "eqL2", "eqL1", path, true);
            WaveRule.CreateWaveRules("WR/11eq1.txt", "WR/11eqR2.txt", "eqR1", "eqR2", path, true);
            WaveRule.CreateWaveRules("WR/11eqR2.txt", "WR/11eq1.txt", "eqR2", "eqR1", path, true);

            WaveRule.CreateWaveRules("WR/12eqzero1.txt", "WR/12eqzero2.txt", "eqzero1", "eqzero2", path, true);
            WaveRule.CreateWaveRules("WR/12eqzero2.txt", "WR/12eqzero1.txt", "eqzero2", "eqzero1", path, true);

            WaveRule.CreateWaveRules("WR/13eqsum1.txt", "WR/13eqsumL2.txt", "eqsumL1", "eqsumL2", path, true);
            WaveRule.CreateWaveRules("WR/13eqsumL2.txt", "WR/13eqsum1.txt", "eqsumL2", "eqsumL1", path, true);
            WaveRule.CreateWaveRules("WR/13eqsum1.txt", "WR/13eqsumR2.txt", "eqsumR1", "eqsumR2", path, true);
            WaveRule.CreateWaveRules("WR/13eqsumR2.txt", "WR/13eqsum1.txt", "eqsumR2", "eqsumR1", path, true);

            WaveRule.CreateWaveRules("WR/14eqtimes1.txt", "WR/14eqtimesL2.txt", "eqtimesL1", "eqtimesL2", path, true);
            WaveRule.CreateWaveRules("WR/14eqtimesL2.txt", "WR/14eqtimes1.txt", "eqtimesL2", "eqtimesL1", path, true);
            WaveRule.CreateWaveRules("WR/14eqtimes1.txt", "WR/14eqtimesR2.txt", "eqtimesR1", "eqtimesR2", path, true);
            WaveRule.CreateWaveRules("WR/14eqtimesR2.txt", "WR/14eqtimes1.txt", "eqtimesR2", "eqtimesR1", path, true);

            WaveRule.CreateWaveRules("WR/15powertimes1.txt", "WR/15powertimes2.txt", "powertimes1", "powertimes2", path, false);
            WaveRule.CreateWaveRules("WR/15powertimes2.txt", "WR/15powertimes1.txt", "powertimes2", "powertimes1", path, false);
            WaveRule.CreateWaveRules("WR/16powerdivide1.txt", "WR/16powerdivide2.txt", "powerdivide1", "powerdivide2", path, false);
            WaveRule.CreateWaveRules("WR/16powerdivide2.txt", "WR/16powerdivide1.txt", "powerdivide2", "powerdivide1", path, false);

            WaveRule.CreateWaveRules("WR/17trig1.txt", "WR/17trig2.txt", "trig1", "trig2", path, true);
            WaveRule.CreateWaveRules("WR/17trig1_otherform.txt", "WR/17trig2.txt", "trig1_otherform", "trig2_otherform", path, true);
            WaveRule.CreateWaveRules("WR/17trig2.txt", "WR/17trig1_otherform.txt", "trig2_otherform", "trig1_otherform", path, true);
            WaveRule.CreateWaveRules("WR/17trig2.txt", "WR/17trig1.txt", "trig2", "trig1", path, true);

            WaveRule.CreateWaveRules("WR/18tan1.txt", "WR/18tan2.txt", "tan1", "tan2", path, true);
            WaveRule.CreateWaveRules("WR/18tan2.txt", "WR/18tan1.txt", "tan2", "tan1", path, true);
            WaveRule.CreateWaveRules("WR/19cotan1.txt", "WR/19cotan2.txt", "cotan1", "cotan2", path, true);
            WaveRule.CreateWaveRules("WR/19cotan2.txt", "WR/19cotan1.txt", "cotan2","cotan1", path, true);
            WaveRule.CreateWaveRules("WR/20sinsum1.txt", "WR/20sinsum2.txt", "sinsum1", "sinsum2", path, true);
            WaveRule.CreateWaveRules("WR/20sinsum2.txt", "WR/20sinsum1.txt", "sinsum2", "sinsum1", path, true);
            WaveRule.CreateWaveRules("WR/21sinminus1.txt", "WR/21sinminus2.txt", "sinminus1",  "sinminus2", path, true);
            WaveRule.CreateWaveRules("WR/21sinminus2.txt", "WR/21sinminus1.txt", "sinminus2",  "sinminus1", path, true);
            WaveRule.CreateWaveRules("WR/22cossum1.txt", "WR/22cossum2.txt", "cossum1", "cossum2", path, true);
            WaveRule.CreateWaveRules("WR/22cossum2.txt", "WR/22cossum1.txt", "cossum2", "cossum1", path, true);
            WaveRule.CreateWaveRules("WR/23cosminus1.txt", "WR/23cosminus2.txt", "cosminus1", "cosminus2", path, true);
            WaveRule.CreateWaveRules("WR/23cosminus2.txt", "WR/23cosminus1.txt", "cosminus2", "cosminus1", path, true);
            WaveRule.CreateWaveRules("WR/24sinoddity1.txt", "WR/24sinoddity2.txt", "sinoddity1", "sinoddity2", path, true);
            WaveRule.CreateWaveRules("WR/24sinoddity2.txt", "WR/24sinoddity1.txt", "sinoddity2", "sinoddity1", path, true);
            WaveRule.CreateWaveRules("WR/25cosoddity1.txt", "WR/25cosoddity2.txt", "cosoddity1", "cosoddity2", path, true);
            WaveRule.CreateWaveRules("WR/25cosoddity2.txt", "WR/25cosoddity1.txt", "cosoddity2", "cosoddity1", path, true);
            WaveRule.CreateWaveRules("WR/26tanoddity1.txt", "WR/26tanoddity2.txt", "tanoddity1", "tanoddity2", path, true);
            WaveRule.CreateWaveRules("WR/26tanoddity2.txt", "WR/26tanoddity1.txt", "tanoddity2", "tanoddity1", path, true);
            WaveRule.CreateWaveRules("WR/27cotanoddity1.txt", "WR/27cotanoddity2.txt", "cotanoddity1", "cotanoddity2", path, true);
            WaveRule.CreateWaveRules("WR/27cotanoddity2.txt", "WR/27cotanoddity1.txt", "cotanoddity2", "cotanoddity1", path, true);

            WaveRule.CreateWaveRules("WR/28log1.txt", "WR/28log2.txt", "log1", "log2", path, true);
            WaveRule.CreateWaveRules("WR/28log2.txt", "WR/28log1.txt", "log2", "log1", path, true);
            WaveRule.CreateWaveRules("WR/29logtimes1.txt", "WR/29logtimes2.txt", "logtimes1", "logtimes2", path, true);
            WaveRule.CreateWaveRules("WR/29logtimes2.txt", "WR/29logtimes1.txt", "logtimes2", "logtimes1", path, true);
            WaveRule.CreateWaveRules("WR/30logdivide1.txt", "WR/30logdivide2.txt", "logdivide1", "logdivide2", path, true);
            WaveRule.CreateWaveRules("WR/30logdivide2.txt", "WR/30logdivide1.txt", "logdivide2", "logdivide1", path, true);
            WaveRule.CreateWaveRules("WR/31logpower1.txt", "WR/31logpower2.txt", "logpower1",  "logpower2", path, true);
            WaveRule.CreateWaveRules("WR/31logpower2.txt", "WR/31logpower1.txt", "logpower2",  "logpower1", path, true);
            WaveRule.CreateWaveRules("WR/32ln1.txt", "WR/32ln2.txt", "ln1", "ln2",  path, true);
            WaveRule.CreateWaveRules("WR/32ln2.txt", "WR/32ln1.txt", "ln2", "ln1", path, true);
            WaveRule.CreateWaveRules("WR/33lntimes1.txt", "WR/33lntimes2.txt", "lntimes1", "lntimes2", path, true);
            WaveRule.CreateWaveRules("WR/33lntimes2.txt", "WR/33lntimes1.txt", "lntimes2", "lntimes1", path, true);
            WaveRule.CreateWaveRules("WR/34lndivide1.txt", "WR/34lndivide2.txt", "lndivide1", "lndivide2", path, true);
            WaveRule.CreateWaveRules("WR/34lndivide2.txt", "WR/34lndivide1.txt", "lndivide2", "lndivide1", path, true);
            WaveRule.CreateWaveRules("WR/35lnpower1.txt", "WR/35lnpower2.txt", "lnpower1", "lnpower2", path, true);
            WaveRule.CreateWaveRules("WR/35lnpower2.txt", "WR/35lnpower1.txt", "lnpower2", "lnpower1", path, true);
            WaveRule.CreateWaveRules("WR/36minusminus1.txt", "WR/36minusminus2.txt", "minusminus1", "minusminus2",  path, true);
            WaveRule.CreateWaveRules("WR/36minusminus2.txt", "WR/36minusminus1.txt", "minusminus2", "minusminus1", path, true);
            //LOGBASE
        }

        [HandleProcessCorruptedStateExceptions]
        private static void IndexGenerated()
        {
            XslCompiledTransform converter = new XslCompiledTransform();
            converter.Load(XmlReader.Create(System.IO.File.OpenRead("mmlctop.xsl")));
            string[] lines = File.ReadAllLines("result1k.txt");
            Searcher s = new Searcher();
            Directory.CreateDirectory("MWS");
            for (int i = 1; i < lines.Length; i += 3)
            {
                int num = ((i + 2) / 3);
                string newXhtml = "MWS/file_" + num + ".xhtml";
                File.Copy("xhtml.txt", newXhtml);
                string expr = lines[i];
                string prsnt = lines[i + 1];
                string output = "";
                try
                {
                    XmlReader reader = XmlReader.Create(new System.IO.StringReader(expr));
                    System.IO.StringWriter writer = new System.IO.StringWriter();
                    XmlWriter xmlwriter = XmlWriter.Create(writer);
                    converter.Transform(reader, new LimitedDepthXmlWriter(xmlwriter, 200));
                    output = writer.ToString();
                    reader.Close();
                    writer.Close();
                    output = output.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?><math xmlns=\"http://www.w3.org/1998/Math/MathML\" xmlns:mml=\"http://www.w3.org/1998/Math/MathML\">", string.Empty).Replace("</math>", string.Empty);
                }
                catch
                {
                    output = string.Empty;
                    File.AppendAllText("failedexprs.txt", num + "\r\n");
                }

                NewEntry ne = new NewEntry();
                ne.Secret = "A27A024F-E44A-4BF7-B169-B655B18A8124";
                ne.Content = expr;
                ne.Presentation = prsnt;
                ne.Description = "artificial data#"+num;
                ne.Location = "195.209.147.206:8080";
                string res = s.SubmitExpression(ne);
                Console.WriteLine("#" + num + " " + res);
                if (res.IndexOf("Success") < 0)
                    File.AppendAllText("failedexprs.txt", expr + "\n");
                else
                {
                    //id=\"idp" + num.ToString("000000") + "0000\"
                    string mathml = ne.Content.Replace("<math>", "<math xmlns=\"http://www.w3.org/1998/Math/MathML\" alttext=\"" + prsnt + "\"><semantics>" + output + "<annotation-xml encoding=\"MathML-Content\">").Replace("</math>", "</annotation-xml></semantics></math>");

                    XDocument xmlFile = XDocument.Parse(mathml);
                    var query = from c in xmlFile.Descendants() select c;

                    int counter = 1;
                    xmlFile.Root.SetAttributeValue("id", "idp" + num.ToString("000000") + "0000");
                    foreach (XElement elem in query)
                    {
                        elem.SetAttributeValue("id", "idp" + num.ToString("000000") + counter.ToString("0000"));
                        counter++;
                    }

                    mathml = xmlFile.ToString();
                    File.AppendAllText(newXhtml, "<div id=\"exprs_" + num + "\">\r\n<p>Expression block number " + num + ". $$" + prsnt + "$$</p>\r\n" + mathml + "\r\n<p>end of expression block</p>\r\n</div>\r\n");
                }
                File.AppendAllText(newXhtml, "</body>\r\n</html>");
            }
        }

        private static void MassTest()
        {
            int amount = 100;
            int DirectHit = 0;
            int Missed = 0;
            int failed = 0;
            double avg = 0.0;
            int avgResult = 0;
            Searcher r = new Searcher();
            for (int i = 0; i < amount; i++)
            {
                try
                {
                    Console.WriteLine("#" + i);
                    DateTime before = DateTime.Now;
                    Expression searchInput = r.getExpressionById(i);
                    if (searchInput == null)
                    {
                        Missed++;
                        continue;
                    }
                    SearchResultPage res = r.S(searchInput.CMathML);
                    Console.WriteLine("Received " + res.Results.Count + " results");
                    if (res.Results.Count == 1)
                        Console.WriteLine(res.Results[0].Content);
                    if (res.Results.Count == 0)
                        Missed++;
                    else
                        if (res.Results.First().Weight == 1.0)
                            DirectHit++;
                    avgResult += res.Results.Count;
                    avg += (DateTime.Now - before).TotalMilliseconds;
                }
                catch
                {
                    Console.WriteLine("ERROR");
                    failed++;
                }
            }
            Console.WriteLine("Avg result count: " + avgResult / (double)(amount - Missed - failed));
            Console.WriteLine("Avg: " + avg / amount);
            Console.WriteLine("Missed: " + Missed + " " + "Hit: " + DirectHit + " failed: " + failed);
        }

        private static void MassImproveTest()
        {
            Searcher s = new Searcher();
            List<bool> results = new List<bool>();
            for (int i = 1500; i < 1600; i++)
            {
                bool isSuccess = false;
                string query = s.getExpressionById(i).CMathML;
                foreach (SearchResult res in s.S(query).Results)
                {
                    Console.WriteLine("Result: " + res.ResultID);
                    SearchResult newResult = s.ImproveResult(res.ResultID);
                    if (newResult.Weight > res.Weight)
                        isSuccess = true;
                }
                results.Add(isSuccess);
            }
            for (int i = 0; i < results.Count; i++)
                Console.WriteLine("#" + i + " " + results[i]);
        }

        private static void ImproveTest()
        {
            Searcher s = new Searcher();
            bool isSuccess = false;
            string query = s.getExpressionById(1475).CMathML;
            foreach (SearchResult res in s.S(query).Results)
            {
                Console.WriteLine("Result: " + res.ResultID);
                SearchResult newResult = s.ImproveResult(res.ResultID);
                if (newResult.Weight > res.Weight)
                    isSuccess = true;
            }
            Console.WriteLine("Success: " + isSuccess);
        }

        private static void IndexWolfram(string path)
        {
            //94462
            Searcher s = new Searcher();
            StreamReader SR = new StreamReader(File.OpenRead(path));
            Regex regex = new Regex("<annotation-xml encoding='MathML-Content'>([^\n]*)</annotation-xml>");
            string expr = "";
            while (!SR.EndOfStream)
            {
                //if (counter < (85919 + 800))
                //{
                //    expr = SR.ReadLine();
                //    counter++;
                //    continue;
                //}
                expr = SR.ReadLine();
                try
                {
                    NewEntry ne = new NewEntry();
                    Match m = regex.Match(expr.Substring(expr.LastIndexOf("<annotation-xml encoding='MathML-Content'>")));
                    if (m.Success)
                        ne.Content = "<math>" + m.Groups[1].ToString() + "</math>";
                    else
                        throw new Exception("Regex not success");
                    ne.Location = expr.Split('\t')[0];
                    ne.Secret = "A27A024F-E44A-4BF7-B169-B655B18A8124";
                    ne.Presentation = expr.Split('\t')[1];
                    string res = s.SubmitExpression(ne);
                    Console.WriteLine(res);
                    if (res.IndexOf("Success") < 0)
                        File.AppendAllText("failedexprs.txt", expr + "\n");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            SR.Close();
        }


        private static void IndexTeXDump()
        {
            Regex idRegex = new Regex("[0-9][0-9][0-9][0-9].[0-9][0-9][0-9][0-9]");
            Searcher h = new Searcher();
            Dictionary<string, List<string>> added = new Dictionary<string, List<string>>();
            int counter = 0;
            int error = 0;
            Dictionary<string, int> dic = new Dictionary<string, int>();
            foreach (string s in File.ReadAllLines("math.txt"))
            {
                counter++;
                Console.Write("New string... ");
                NewEntry ne = new NewEntry();
                string[] arr = s.Split('\t');
                if (arr.Length != 3)
                {
                    Console.WriteLine(" parse error");
                    continue;
                }
                List<string> values;
                if (!added.TryGetValue(arr[0], out values))
                {
                    values = new List<string>();
                    added.Add(arr[0], values);
                }
                ne.Location = "http://arxiv.org/abs/" + idRegex.Match(arr[0]).Value;
                ne.Presentation = arr[1];
                ne.Content = arr[2];
                ne.Secret = "A27A024F-E44A-4BF7-B169-B655B18A8124";
                if (ne.Content.IndexOf("error") > -1 || values.IndexOf(ne.Content) > -1)
                {
                    Console.WriteLine(" duplicate/error");
                    error++;
                    continue;
                }
                else
                {
                    values.Add(ne.Content);
                    string res = h.SubmitExpression(ne);
                    int k;
                    if (dic.TryGetValue(res, out k))
                        dic[res] = k + 1;
                    else
                        dic.Add(res, 1);
                }

            }
            Console.WriteLine("Total: " + counter);
            foreach (KeyValuePair<string, int> pair in dic)
                Console.WriteLine(pair.Key + " " + pair.Value);
            Console.WriteLine("merror: " + error);
        }

        private static void IndexTeXDumpWithPosDescription()
        {
            Regex idRegex = new Regex("[0-9][0-9][0-9][0-9].[0-9][0-9][0-9][0-9]");
            Searcher h = new Searcher();
            Dictionary<string, List<string>> added = new Dictionary<string, List<string>>();
            List<string> addedPapers = new List<string>();
            Dictionary<string, string> titles = new Dictionary<string, string>();
            int WirisSuccess = 0;
            int emptypaperscounter = 0;
            int WirisFailedExpresions = 0;
            Dictionary<string, int> dic = new Dictionary<string, int>();
            List<string> xmlFiles = new List<string>(Directory.GetFiles("xml_IR"));
            int IRCount = xmlFiles.Count;
            xmlFiles.AddRange(Directory.GetFiles("xml"));
            List<string> math = new List<string>(File.ReadAllLines("math.txt"));
            int lastIndex = 0;
            int TotalExpressios = 0;
            int TrivialExpresssions = 0;
            int LaTeXFailedExpressions = 0;
            int dup = 0;
            for (int xmlFileIndex = 0; xmlFileIndex < xmlFiles.Count; xmlFileIndex++)
            {
                string filename = Path.GetFileName(xmlFiles[xmlFileIndex]);
                string arxivid = filename.Substring(0, filename.LastIndexOf('_'));
                Match m = idRegex.Match(arxivid);
                if (m.Success)
                    arxivid = m.Value;

                if (addedPapers.IndexOf(filename) > -1)
                    continue;
                else
                    addedPapers.Add(filename);
                XmlDocument document = new XmlDocument();
                document.Load(xmlFiles[xmlFileIndex]);
                string title;
                if (!titles.TryGetValue(arxivid, out title))
                {
                    if (document.GetElementsByTagName("title").Count > 0)
                    {
                        title = document.GetElementsByTagName("title")[0].InnerText;
                        titles.Add(arxivid, title);
                    }
                    else
                    {
                        title = String.Empty;
                    }
                }
                Console.WriteLine(xmlFileIndex + " out of " + xmlFiles.Count + " title " + title);
                XmlNodeList list = document.GetElementsByTagName("Math");
                List<string> expressions = new List<string>();
                int mathIndex = math.FindIndex(lastIndex, x => { return x.IndexOf(filename) > -1; });
                if (mathIndex > -1)
                    for (int i = mathIndex; i < math.Count; i++)
                        if (math[i].IndexOf(filename) > -1)
                        {
                            if (math[i].IndexOf("error") < 0)
                            {
                                WirisSuccess++;
                                expressions.Add(math[i]);
                            }
                            else
                                WirisFailedExpresions++;
                        }
                        else
                        {
                            lastIndex = i;
                            break;
                        }

                if (expressions.Count == 0)
                {
                    emptypaperscounter++;
                    continue;
                }
                int foundCount = 0;
                foreach (XmlNode node in list)
                {
                    TotalExpressios++;
                    node.FirstChild.FirstChild.RemoveChild(node.FirstChild.FirstChild.LastChild);
                    if (GetDepth(node.FirstChild.FirstChild) <= 3)
                    {
                        TrivialExpresssions++;
                        continue;
                    }
                    string mathml = node.FirstChild.FirstChild.InnerXml;
                    mathml = "<math>" + mathml.Replace("m:", "").Replace("xmlns:m=\"http://www.w3.org/1998/Math/MathML\"", "") + "</math>";
                    string asciimathml = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8, Encoding.GetEncoding("windows-1252"), Encoding.UTF8.GetBytes(mathml)));
                    if (mathml.IndexOf("merror") > -1)
                    {
                        LaTeXFailedExpressions++;
                        continue;
                    }
                    string tex = node.Attributes["tex"].Value;
                    string ex = null;
                    if (xmlFileIndex <= IRCount)
                        ex = expressions.Find(x => { return x.IndexOf(asciimathml) > -1; });
                    else
                        ex = expressions.Find(x => { return x.IndexOf(mathml) > -1; });
                    if (ex != null)
                    {
                        foundCount++;
                        Console.Write("New string... ");
                        NewEntry ne = new NewEntry();
                        string[] arr = ex.Split('\t');
                        if (arr.Length != 3)
                        {
                            Console.WriteLine(" parse error");
                            continue;
                        }
                        List<string> values;
                        if (!added.TryGetValue(arr[0], out values))
                        {
                            values = new List<string>();
                            added.Add(arr[0], values);
                        }
                        ne.Location = "http://arxiv.org/abs/" + arxivid;
                        ne.Presentation = tex;
                        ne.Content = arr[2];
                        ne.Secret = "A27A024F-E44A-4BF7-B169-B655B18A8124";
                        string descr = node.ParentNode.ParentNode.InnerText;
                        if (descr.Length > 300)
                            descr = node.ParentNode.InnerText;
                        if (title == String.Empty)
                            title = GetTitle(arxivid);
                        ne.Description = title + "\t" + descr;
                        if (values.IndexOf(ne.Content) > -1)
                        {
                            Console.WriteLine("duplicate");
                            dup++;
                            continue;
                        }
                        else
                        {
                            values.Add(ne.Content);
                            string res = h.SubmitExpression(ne);
                            int k;
                            if (dic.TryGetValue(res, out k))
                                dic[res] = k + 1;
                            else
                                dic.Add(res, 1);
                        }
                    }
                }
                if (foundCount != expressions.Count)
                {
                    Console.WriteLine("Found: " + foundCount + " of " + expressions.Count);
                    Console.ReadLine();
                }
            }
            h.FinishSubmit();
            Console.WriteLine();
            Console.WriteLine("Total titles: " + titles.Count);
            Console.WriteLine("Empty papers: " + emptypaperscounter);
            Console.WriteLine("Total expressions: " + TotalExpressios);
            Console.WriteLine("latex failed: " + LaTeXFailedExpressions);
            Console.WriteLine("Trivial expressions: " + TrivialExpresssions);
            Console.WriteLine("Total success conversions: " + WirisSuccess);
            foreach (KeyValuePair<string, int> pair in dic)
                Console.WriteLine(pair.Key + " " + pair.Value);
            Console.WriteLine("merror: " + WirisFailedExpresions);
        }
        static string GetTitle(string arxivid)
        {
            try
            {
                Console.WriteLine("http://arxiv.org/abs/" + arxivid);
                HttpWebRequest client = HttpWebRequest.CreateHttp("http://arxiv.org/abs/" + arxivid);
                //client.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                //client.Headers.Add("Accept-Encoding", "gzip, deflate");
                client.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
                //client.Headers.Add("Connection", "keep-alive");
                client.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:28.0) Gecko/20100101 Firefox/28.0";
                Regex titleregex = new Regex("<title>([^<>]*)</title>");
                WebResponse response = client.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string s = reader.ReadToEnd();
                reader.Close();
                response.Close();
                Match res = titleregex.Match(s);
                //Console.WriteLine(s.Substring(0, 100));
                System.Threading.Thread.Sleep(new Random().Next(5000));
                return res.Groups[0].Value;
            }
            catch (Exception)
            {
                return "NO_TITLE";
            }
        }

        static int GetDepth(XmlNode node)
        {
            int res = 0;
            if (node.HasChildNodes)
            {
                foreach (XmlNode n in node.ChildNodes)
                {
                    int r = GetDepth(n);
                    if (r > res)
                        res = r;
                }
            }
            return 1 + res;
        }

        private static void IndexThem()
        {
            Searcher s = new Searcher();
            //string dir = "C:\\Users\\adm\\repos\\TeXProcessor\\bin\\Debug\\cmmls2";
            string dir = "C:\\Users\\adm\\Desktop\\test\\sempomat\\sempomat\\mathSearchWeb\\TeXProcessor\\bin\\Debug\\cmmls2";
            string[] paths = Directory.GetFiles(dir, "*", SearchOption.AllDirectories);
            Console.WriteLine(paths.Count());
            List<string> log = new List<string>();
            int k = 0;
            int err = 0;
            foreach (string path in paths)
            {
                Console.Write("So far: " + k + " with error: " + err + " making request.... ");
                try
                {
                    NewEntry ne = new NewEntry();
                    ne.Content = File.ReadAllText(path);
                    if (ne.Content.IndexOf("cerror") > -1)
                        continue;
                    ne.Location = "http://arxiv.org/abs/" + path.Remove(0, dir.Length).Split('\\')[1];
                    ne.Secret = "A27A024F-E44A-4BF7-B169-B655B18A8124";
                    Console.WriteLine(s.SubmitExpression(ne));
                    k++;

                }
                catch (Exception ex)
                {
                    log.Add(path + " failed to load  failed with " + ex.Message);
                    Console.WriteLine(ex.Message);
                    err++;
                }
            }
        }
    }

    public class LimitedDepthXmlWriter : XmlWriter
    {
        private readonly XmlWriter _innerWriter;
        private readonly int _maxDepth;
        private int _depth;

        public LimitedDepthXmlWriter(XmlWriter innerWriter)
            : this(innerWriter, 100)
        {
        }

        public LimitedDepthXmlWriter(XmlWriter innerWriter, int maxDepth)
        {
            _maxDepth = maxDepth;
            _innerWriter = innerWriter;
        }

        public override void Close()
        {
            _innerWriter.Close();
        }

        public override void Flush()
        {
            _innerWriter.Flush();
        }

        public override string LookupPrefix(string ns)
        {
            return _innerWriter.LookupPrefix(ns);
        }

        public override void WriteBase64(byte[] buffer, int index, int count)
        {
            _innerWriter.WriteBase64(buffer, index, count);
        }

        public override void WriteCData(string text)
        {
            _innerWriter.WriteCData(text);
        }

        public override void WriteCharEntity(char ch)
        {
            _innerWriter.WriteCharEntity(ch);
        }

        public override void WriteChars(char[] buffer, int index, int count)
        {
            _innerWriter.WriteChars(buffer, index, count);
        }

        public override void WriteComment(string text)
        {
            _innerWriter.WriteComment(text);
        }

        public override void WriteDocType(string name, string pubid, string sysid, string subset)
        {
            _innerWriter.WriteDocType(name, pubid, sysid, subset);
        }

        public override void WriteEndAttribute()
        {
            _innerWriter.WriteEndAttribute();
        }

        public override void WriteEndDocument()
        {
            _innerWriter.WriteEndDocument();
        }

        public override void WriteEndElement()
        {
            _depth--;

            _innerWriter.WriteEndElement();
        }

        public override void WriteEntityRef(string name)
        {
            _innerWriter.WriteEntityRef(name);
        }

        public override void WriteFullEndElement()
        {
            _innerWriter.WriteFullEndElement();
        }

        public override void WriteProcessingInstruction(string name, string text)
        {
            _innerWriter.WriteProcessingInstruction(name, text);
        }

        public override void WriteRaw(string data)
        {
            _innerWriter.WriteRaw(data);
        }

        public override void WriteRaw(char[] buffer, int index, int count)
        {
            _innerWriter.WriteRaw(buffer, index, count);
        }

        public override void WriteStartAttribute(string prefix, string localName, string ns)
        {
            _innerWriter.WriteStartAttribute(prefix, localName, ns);
        }

        public override void WriteStartDocument(bool standalone)
        {
            _innerWriter.WriteStartDocument(standalone);
        }

        public override void WriteStartDocument()
        {
            _innerWriter.WriteStartDocument();
        }

        public override void WriteStartElement(string prefix, string localName, string ns)
        {
            if (_depth++ > _maxDepth) ThrowException();

            _innerWriter.WriteStartElement(prefix, localName, ns);
        }

        public override WriteState WriteState
        {
            get { return _innerWriter.WriteState; }
        }

        public override void WriteString(string text)
        {
            _innerWriter.WriteString(text);
        }

        public override void WriteSurrogateCharEntity(char lowChar, char highChar)
        {
            _innerWriter.WriteSurrogateCharEntity(lowChar, highChar);
        }

        public override void WriteWhitespace(string ws)
        {
            _innerWriter.WriteWhitespace(ws);
        }

        private void ThrowException()
        {
            throw new InvalidOperationException(string.Format("Result xml has more than {0} nested tags. It is possible that xslt transformation contains an endless recursive call.", _maxDepth));
        }
    }
}
