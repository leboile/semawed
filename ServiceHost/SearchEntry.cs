﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Xsl;
using Semawed;

namespace Service
{
    class SearchEntry
    {
        private static XslCompiledTransform converter = new XslCompiledTransform();
        private static bool XslInit = false;
        private static void LoadXSL()
        {
            if (XslInit)
                return;
            converter.Load(XmlReader.Create(System.IO.File.OpenRead("mmlctop.xsl")));
            XslInit = true;
        }

        private static string Transform(string input)
        {
            XmlReader reader = XmlReader.Create(new System.IO.StringReader(input));
            System.IO.StringWriter writer = new System.IO.StringWriter();
            converter.Transform(reader, XmlWriter.Create(writer));
            string output = writer.ToString();
            reader.Close();
            writer.Close();
            return output;
        }

        private bool improved = false;
        public bool Improved
        {
            get
            {
                return improved;
            }
        }
        private bool proofed = false;
        public bool Proofed
        {
            get
            {
                return proofed;
            }
        }
        private bool inclusionFound = false;
        public bool InclusionFound
        {
            get
            {
                return inclusionFound;
            }
        }
        private bool equal = false;
        private Expression ex;
        public Expression Entry
        {
            get
            {
                return ex;
            }
        }
        private string location;
        public string Location
        {
            get
            {
                return location;
            }
        }

        private string presentation;
        public string Presentation
        {
            get
            {
                return presentation;
            }
        }

        private string description;
        public string Description
        {
            get
            {
                return description;
            }
        }

        private Expression searchInput;
        ProofResult result = null;

        public string Proof
        {
            get
            {
                if (result != null && result.IsSuccess)
                    return "Substs: " + result.Substitutions.Count + " rules: " + (result.RulesAppliedToLeft.Count + result.RulesAppliedToRight.Count).ToString();//+"\n Result: "+result.LeftExpression+"\n\nEQ\n\n"+result.RightExpression+"\n\n\n";
                else
                    return "NOPROOF Res NULL";
                LoadXSL();
                if (result == null || !result.IsSuccess)
                    return "";
                StringBuilder SB = new StringBuilder();
                SB.AppendLine("First expression \n" + Transform(searchInput.ToCMathML()));
                SB.AppendLine("Second expression \n" + Transform(ex.ToCMathML()));
                SB.AppendLine("Assuming: ");
                foreach (KeyValuePair<string, string> pair in result.Substitutions)
                    SB.AppendLine(pair.Key + " as " + Transform(pair.Value));
                string[] split = { "-->", "###" };
                if (result.RulesAppliedToLeft.Count > 0)
                {
                    SB.AppendLine("Applying rules to search input: ");
                    foreach (string rule in result.RulesAppliedToLeft)
                    {
                        string[] arr = rule.Split(split, StringSplitOptions.None);
                        SB.AppendLine(arr[0]+"\n"+Transform(arr[1]) + " --> " + Transform(arr[2]));
                    }
                }

                if (result.RulesAppliedToRight.Count > 0)
                {
                    SB.AppendLine("Applying rules to current expression: ");
                    foreach (string rule in result.RulesAppliedToRight)
                    {
                        string[] arr = (rule.Split(split, StringSplitOptions.None));
                        SB.AppendLine(arr[0]+"\n"+Transform(arr[1]) + " --> " + Transform(arr[2]));
                    }
                }

                SB.AppendLine("Results:");
                SB.AppendLine();
                SB.AppendLine(Transform(result.LeftExpression));
                SB.AppendLine();
                SB.AppendLine("is similar by " + result.Similarity + " to ");
                SB.AppendLine();
                SB.AppendLine(Transform(result.RightExpression));
                SB.AppendLine();
                SB.AppendLine();
                //SB.AppendLine("##################");
                //SB.AppendLine("CONTENT MATHML");
                //if (result.RulesAppliedToLeft.Count > 0)
                //{
                //    SB.AppendLine("Applying rules to search input: ");
                //    foreach (string rule in result.RulesAppliedToLeft)
                //    {
                //        SB.AppendLine(rule);
                //    }
                //}

                //if (result.RulesAppliedToRight.Count > 0)
                //{
                //    SB.AppendLine("Applying rules to current expression: ");
                //    foreach (string rule in result.RulesAppliedToRight)
                //    {
                //        SB.AppendLine(rule);
                //    }
                //}

                //SB.AppendLine("Results:");
                //SB.AppendLine();
                //SB.AppendLine(result.LeftExpression);
                //SB.AppendLine();
                //SB.AppendLine("is similar by " + result.Similarity + " to ");
                //SB.AppendLine();
                //SB.AppendLine(result.RightExpression);
                
                return SB.ToString();
            }
        }

        public SearchEntry(Expression input, Expression e, string l, string p, string d)
        {
            searchInput = input;
            ex = e;
            location = l;
            presentation = p;
            description = d;
        }

        public SearchResult ToSearchResult()
        {
            SearchResult res = new SearchResult();
            res.Content = ex.CMathML;
            if (equal)
                res.Weight = 1.0;
            else
                res.Weight = result.Similarity;
            res.Proof = Proof;
            res.Location = Location;
            res.Presentation = presentation;
            res.Description = description;
            return res;
        }

        public bool CheckEqual()
        {
            if (searchInput.Equals(ex, false))
            {
                proofed = true;
                equal = true;
            }
          
            return equal;
        }

        public bool TryProof(out SearchResult searchResult)
        {
            try
            {
                result = Rippling.PerformProof(ex, searchInput, 0.5, ProofMode.Fast, StopConditions.UnificationFinishOrFirstResultOrLimitReached, new TimeSpan(0, 0, 0, 0, 200));
            }
            catch (Exception ex)
            {
                //throw;
                Console.WriteLine("ERROR OCCURED: " + ex.Message);
                Rippling.SystemLog.Add("BgWorker: " + ex.Message);
                searchResult = null;
                proofed = false;
                return false;
            }
            searchResult = this.ToSearchResult();
            proofed = true;
            return true;
        }

        public bool ImproveProof(out SearchResult searchResult)
        {
            if (!proofed)
                throw new InvalidOperationException("Cannont improve proof. Entry is not proofed.");

            if (improved)
            {
                Console.WriteLine("Improved: was improved before");
                searchResult = null;
                return false;
            }

            try
            {
                ProofResult improvedResult = Rippling.PerformProof(ex, searchInput, result.Similarity + (1 - result.Similarity) / 2.0, ProofMode.Slow, StopConditions.LimitReached, new TimeSpan(0, 0, 0, 5, 0));
                if (result.Similarity < improvedResult.Similarity)
                {
                    Console.WriteLine("Improved: was " + result.Similarity + " got " + improvedResult.Similarity);
                    result = improvedResult;
                }
                else
                    Console.WriteLine("Not improved: was " + result.Similarity + " got " + improvedResult.Similarity);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR OCCURED: " + ex.Message);
                Rippling.SystemLog.Add("BgWorker: " + ex.Message);
                searchResult = null;
                improved = false;
                return false;
            }

            searchResult = this.ToSearchResult();
            improved = true;
            return true;
        }

        public override bool Equals(object obj)
        {
            return object.ReferenceEquals(this, obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    internal class TillVariablesEqualityCmp : IEqualityComparer<Element>
    {
        public bool Equals(Element x, Element y)
        {
            if (x == null && y == null)
                return true;
            else
                if (x == null || y == null)
                    return false;
            if (x.TypeOfElement == ElementType.Variable && y.TypeOfElement == ElementType.Variable)
                return true;
            else
                return x.Equals(y);
        }

        public int GetHashCode(Element obj)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");
            return obj.GetHashCode();
        }
    }
}
