﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ServiceHost;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using Semawed;
using System.Collections.Concurrent;

namespace Service
{
    class Cache
    {
        private ConcurrentDictionary<string, CacheEntry> cache = new ConcurrentDictionary<string, CacheEntry>();
        private DateTime initTimeStamp = DateTime.Now;
        private int maxEntries;
        private TimeSpan TTL;
        private uint entryCounter = 1;
        public Cache(int MaxEntries, TimeSpan timeToLive)
        {
            maxEntries = MaxEntries;
            TTL = timeToLive;
        }

        public void ClearCache()
        {
            cache = new ConcurrentDictionary<string, CacheEntry>();//it might be the good way to ensure that no CacheEntry on which BGWorker is running would be missed
        }

        public bool TryGetCacheEntry(string s, out CacheEntry entry)
        {
            if (cache.TryGetValue(s, out entry))//cachehit
                return true;
            else
                return false;
        }

        public CacheEntry EnCache(string s, List<SearchEntry> notProofed, List<SearchEntry> readyForNow)
        {
            if (notProofed == null)
                throw new ArgumentNullException("notProofed");
            if (readyForNow == null)
                throw new ArgumentNullException("readyForNow");
            lock (cache)
            {
                CacheEntry res;
                if (cache.TryGetValue(s, out res))//cachehit
                    return res;
                CacheEntry entry = new CacheEntry(notProofed, readyForNow, 0, entryCounter);
                cache.AddOrUpdate(s, entry, (string key, CacheEntry e) => { res = e; return e; });
                entryCounter++;
                return entry;
            }
        }

        public SearchResult ImproveResult(uint resultId)
        {
            uint cacheId = resultId >> 16;
            CacheEntry entry = cache.Values.First(x=>x.ID==cacheId);
            if (entry!=null)
                return entry.ImproveResult(resultId);
            else
                throw new ArgumentException("CacheEntry with specified resultId not found", "resultId");
        }

        private void cleanUp()
        {
            DateTime now = DateTime.Now;
            var res = from pair in cache where (now - pair.Value.TimeStamp) >= TTL select pair.Key;
            CacheEntry whythisismanadatory;
            foreach (string key in res)
                cache.TryRemove(key, out whythisismanadatory);
        }
    }
}
