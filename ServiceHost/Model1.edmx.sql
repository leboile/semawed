
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 11/27/2014 02:40:35
-- Generated from EDMX file: C:\Users\dvg\Desktop\Semawed\ServiceHost\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [ExpressionsDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[ExprsTable]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ExprsTable];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'ExprsTable'
CREATE TABLE [dbo].[ExprsTable] (
    [ID] int  NOT NULL,
    [Location] nvarchar(max)  NOT NULL,
    [Serialized] varbinary(max)  NOT NULL,
    [varCount] int  NOT NULL,
    [Depth] int  NOT NULL,
    [cCount] int  NOT NULL,
    [Presentation] nvarchar(max)  NOT NULL,
    [cHash] nvarchar(max)  NOT NULL,
    [Total] int  NOT NULL,
    [Description] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ID] in table 'ExprsTable'
ALTER TABLE [dbo].[ExprsTable]
ADD CONSTRAINT [PK_ExprsTable]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------