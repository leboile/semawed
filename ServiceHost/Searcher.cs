﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ServiceHost;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using Semawed;
using System.Collections.Concurrent;
namespace Service
{
    public class Searcher : ISearcher
    {
        private static Cache cache = new Cache(1024, new TimeSpan(24, 0, 0));
        private static SqlConnection connection = new SqlConnection("Data Source=(LocalDB)\\v11.0;AttachDbFilename=|DataDirectory|\\ExpressionsDB.mdf;Integrated Security=True");
        private static bool init = false;
        private static SqlDataAdapter refreshAdapter = new SqlDataAdapter(new SqlCommand("select * from Expressions", connection));
        private static ExpressionsDBEntities dbContext = new ExpressionsDBEntities();
        public SearchResultPage S(string q)
        {
            if (!init)
                return SearchResultPage.GetErrorPage("Service not initiaized");
            //Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\ExpressionsDB.mdf;Integrated Security=True 
            SearchResultPage res = new SearchResultPage();
            TimeSpan dur = TimeSpan.Zero;
            DateTime before = DateTime.Now;
            try
            {
                Expression searchInput = new Expression(q);
                Console.WriteLine("new search query: " + searchInput.Root.ToString());
                int varCount = searchInput.VarCount;
                int cCount = searchInput.Constants.Count;
                string cHash = searchInput.ConstantsHash;
                double total = searchInput.InitialInformation.Total;
                if (searchInput.InitialInformation.Depth == 0)
                    return res;
                double depth = searchInput.InitialInformation.Depth;
                double Percent = (Math.Ceiling(depth * 0.3) + Math.Ceiling(total * 0.3)) / 2;
                if (Percent < 3)
                    Percent = 3;
                if (varCount < 3)
                    Percent = 10;

                //Percent = 10;
                var preSearchResult = (from o in dbContext.ExprsTable where Math.Abs(o.varCount - varCount - cCount + o.cCount) < 2 && (Math.Abs(depth - o.Depth + total - o.Total) < Percent) && (cHash.Length == o.cHash.Length) select o).ToList(); //where o.varCount == varCount && o.cCount == cCount && o.cHash == cHash
                Console.WriteLine("Pre search result count: " + preSearchResult.Count());
                //System.Linq.IQueryable<SearchEntry> searchResult;
                var tags = getTeXArray(searchInput);
                System.Collections.Generic.IEnumerable<SearchEntry> searchResult;
                //if (preSearchResult.Count() > 50)
                //{
                //    searchResult = from o in preSearchResult where o.varCount == varCount && o.cHash == cHash let z = Expression.LoadFromByteArray(o.Serialized) select new SearchEntry(searchInput, z, o.Location, o.Presentation, o.Description);
                //    if (!searchResult.Any() || searchResult.Count() > 50)
                //    //    searchResult = from o in preSearchResult where Math.Abs(depth - o.Depth) < PercentOfDepth && Math.Abs(total - o.Total) < PercentOfTotal let z = Expression.LoadFromByteArray(o.Serialized) select new SearchEntry(searchInput, z, o.Location, o.Presentation);
                //    //else
                //    {
                //        int k = 0;
                //        searchResult = from o in preSearchResult let c = k where (k = c + 1) < 50 let z = Expression.LoadFromByteArray(o.Serialized) select new SearchEntry(searchInput, z, o.Location, o.Presentation, o.Description);
                //    }
                //}
                //else
                searchResult = from o in preSearchResult  let z = Expression.LoadFromByteArray(o.Serialized) select new SearchEntry(searchInput, z, o.Location, o.Presentation, o.Description);
                //where (checkTeXInclusion(o.Presentation, tags) >= 0.5) 
                //might really be useful
                List<SearchEntry> searchList = searchResult.ToList();
                int countSL = searchList.Count;
                List<SearchEntry> readyForNow = new List<SearchEntry>();// (from z in searchResult where z.CheckEqual() select z).ToList();
                foreach (SearchEntry e in searchList)
                    if (e.CheckEqual())
                        readyForNow.Add(e);
                foreach (SearchEntry e in readyForNow)
                    searchList.Remove(e);
                Console.WriteLine("Possible results: " + searchList.Count);
                Console.WriteLine("Full match: " + readyForNow.Count());

                CacheEntry entry = cache.EnCache(q, searchList, readyForNow);
                int resultsNeeded = searchList.Count > 10 ? 10 : searchList.Count - readyForNow.Count;
                entry.WaitForResultsOrUntilReady(resultsNeeded);
                res.Results.AddRange(entry.ReadyResults.Take(resultsNeeded));
                dur = DateTime.Now - before;
                Console.WriteLine("Ready in " + dur.TotalMilliseconds.ToString());
            }
            catch (Exception ex)
            {
                res = SearchResultPage.GetErrorPage(ex.Message);
                Rippling.SystemLog.Add(ex.Message);
            }
            return res;
        }

        public SearchResultPage Search(string q, int from, int to)
        {
            if (!init)
                return SearchResultPage.GetErrorPage("Service not initiaized");
            if (from > to)
                return SearchResultPage.GetErrorPage("Invalid arguments from and to");
            if (to - from < 10)
                to = from + 10;
            if (to <= 10 || from == 0)
                return S(q);

            Console.WriteLine("new query");
            Console.WriteLine("From: " + from + " to: " + to);
            DateTime before = DateTime.Now;
            SearchResultPage res = new SearchResultPage();
            CacheEntry entry;

            try
            {
                if (cache.TryGetCacheEntry(q, out entry))
                {
                    entry.WaitUntilReady();
                    if (from >= entry.ReadyResults.Count)
                        return res;
                    int availableResultCount = entry.ReadyResults.Count - from > 10 ? to - from : entry.ReadyResults.Count - from;
                    Console.WriteLine("but obtaining: " + from + " to: " + (from + availableResultCount));
                    res.Results.AddRange(entry.ReadyResults.GetRange(from, availableResultCount));
                }
                else
                {
                    S(q);
                    if (cache.TryGetCacheEntry(q, out entry))
                    {
                        entry.WaitUntilReady();
                        if (from >= entry.ReadyResults.Count)
                            return res;
                        int availableResultCount = entry.ReadyResults.Count - from > 10 ? to - from : entry.ReadyResults.Count - from;
                        Console.WriteLine("but obtaining: " + from + " to: " + (from + availableResultCount));
                        res.Results.AddRange(entry.ReadyResults.GetRange(from, availableResultCount));
                    }
                    else
                    {
                        Console.WriteLine("Something went wrong! CacheEntry not found!");
                        return SearchResultPage.GetErrorPage("Internal error");
                    }

                }
                TimeSpan dur = TimeSpan.Zero;
            }
            catch (Exception ex)
            {
                res = SearchResultPage.GetErrorPage(ex.Message);
                Rippling.SystemLog.Add(ex.Message);
            }
            return res;
        }

        public SearchResult ImproveResult(uint resultId)
        {
            Console.WriteLine("Improve result calling...");
            try
            {
                if (resultId < (1 << 16))
                    throw new ArgumentException();
                Console.WriteLine("Improving result for id: " + resultId);
                return cache.ImproveResult(resultId);
            }
            catch (ArgumentException)
            {
                return SearchResultPage.GetErrorPage("Wrong resultId").Results[0];
            }
        }

        public void Init()
        {
            dbContext.Database.CreateIfNotExists();
            dbContext.Database.Initialize(false);
            //int amount = dbContext.Database.ExecuteSqlCommand("SELECT * FROM ExprsTable", new object[1]);
            init = true;
            Console.WriteLine(Rippling.LoadRules("rules") + " rules loaded");
            Console.WriteLine("Loaded");
        }

        public Expression getExpressionById(int id)
        {
            var res = dbContext.ExprsTable.Find(id);
            if (res == null)
                return null;
            return Expression.LoadFromByteArray(res.Serialized);
        }

        private static int submittedCount = 0;
        private static int ID = -1;
        public void FinishSubmit()
        {
            dbContext.SaveChanges();
            submittedCount = 0;
            dbContext.Dispose();
            dbContext = new ExpressionsDBEntities();
            dbContext.Configuration.AutoDetectChangesEnabled = false;
        }

        public string SubmitExpression(NewEntry entry)
        {
            if (entry == null)
                return "fail";
            if (entry.Secret != "A27A024F-E44A-4BF7-B169-B655B18A8124")
                return "Wrong secret";
            try
            {
                if (ID == -1)
                    ID = dbContext.ExprsTable.Count();
                Expression e = new Expression(entry.Content, ID.ToString() + "_");
                //var r = from o in dbContext.ExprsTable where o.Location == entry.Location select o;
                byte[] binary = Expression.ToByteArray(e);
                //var similar = from o in r where o.Serialized == binary select o;
                //if (similar.Any())
                //    return "Dublicate";
                //if (e.InitialInformation.Depth <= 3)
                //    return "Depth <3";
                //if (e.CMathML.IndexOf("merror")>-1 || e.CMathML.IndexOf("
                var row = new ExprsTable();
                row.ID = ID;
                row.Location = entry.Location;
                row.Serialized = Expression.ToByteArray(e);
                row.cCount = e.Constants.Count;
                row.Depth = e.InitialInformation.Depth;
                row.varCount = e.VarCount;
                row.Presentation = entry.Presentation;
                row.Total = e.InitialInformation.Total;
                row.cHash = e.ConstantsHash;
                row.Description = entry.Description;
                dbContext.ExprsTable.Add(row);
                submittedCount++;
                if (submittedCount >= 100)
                {
                    dbContext.SaveChanges();
                    submittedCount = 0;
                    dbContext.Dispose();
                    dbContext = new ExpressionsDBEntities();
                    dbContext.Configuration.AutoDetectChangesEnabled = false;
                }
                ID++;
                return "Success";
            }
            catch (Exception ex)
            {
                return "Failed: " + ex.Message;
            }
        }


        public string ClearCache(string passwd)
        {
            if (passwd == "61D43594-3623-46FF-A9AB-07011A0FDD71")
            {
                cache.ClearCache();
                return "Success.";
            }
            else
                return "Wrong Password";
        }

        ~Searcher()
        {
            if (connection != null)
                connection.Close();
        }

        private List<string> getTeXArray(Expression searchInput)
        {
            List<string> result = new List<string>();
            HashSet<Element> elements = searchInput.Root.GetAllElements();
            foreach (Element el in elements)
            {
                string tex;
                if (el.TryGetTeXRepresentation(out tex))
                    result.Add(tex);
            }
            foreach (string r in result)
                Console.WriteLine("TeX tag found: " + r);
            return result;
        }

        private double checkTeXInclusion(string texPresentation, List<string> texArray)
        {
            double weight = 0.0;
            double k = 1.0 / (texArray.Count + 1);
            int lengthSum = 0;
            foreach (string tag in texArray)
            {
                if (texPresentation.IndexOf(tag) > -1)
                    weight += k * (1 - weight);
                lengthSum += tag.Length;
            }
            double r = texPresentation.Length / (double)(texArray.Count * lengthSum);
            //Console.WriteLine("TagWeight: " + weight +" K: "+k+" r: "+r);
            return weight*r;
        }
    }
}
