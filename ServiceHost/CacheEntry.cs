﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Threading;
using Semawed;

namespace Service
{
    class CacheEntry
    {
        private ConcurrentDictionary<uint, SearchEntry> IDTable = new ConcurrentDictionary<uint,SearchEntry>();
        private bool isReady = false;
        public bool IsReady
        {
            get
            {
                return isReady;
            }
        }
        private Task worker;
        private List<SearchEntry> notReadyResults;
        private List<SearchResult> readyResults = new List<SearchResult>();
        public List<SearchResult> ReadyResults
        {
            get
            {
                return readyResults.OrderByDescending(x => x.Weight).ToList();
            }
        }

        private uint id;//CacheEntry Id
        public uint ID
        {
            get
            {
                return id>>16;
            }
        }

        private uint amountOfresultsReady;//used to count results id's
        public uint AmountOfResultsReady
        {
            get
            {
                return amountOfresultsReady;
            }
        }

        private int totalAmount;
        public int TotalAmountOfResults
        {
            get
            {
                return totalAmount;
            }
        }

        private DateTime timestamp;
        public DateTime TimeStamp
        {
            get
            {
                return timestamp;
            }
        }

        private int priority;
        public int Priority
        {
            get
            {
                return priority;
            }
        }

        private int amountOfCompleteProofs = 0;
        ManualResetEventSlim waiter = new ManualResetEventSlim(false);

        /// <summary>
        /// There is 9999 maximum of results for cacheentry to handle 
        /// </summary>
        /// <param name="notProofed"></param>
        /// <param name="proofed"></param>
        /// <param name="p"></param>
        /// <param name="entryId"></param>
        public CacheEntry(List<SearchEntry> notProofed, List<SearchEntry> proofed, int p, uint entryId)
        {
            if (notProofed == null)
                throw new ArgumentNullException("notProofed");
            if (proofed == null)
                throw new ArgumentNullException("proofed");
            if (notProofed.Count + proofed.Count > 9999)
                throw new ArgumentException("There is too much results");
            if (uint.MaxValue >> 16 < entryId)
                throw new ArgumentException("entryId is too big", "entryId");
            id = entryId << 16;
            timestamp = DateTime.Now;
            amountOfresultsReady = 0;
            priority = p;
            notReadyResults = notProofed;
            foreach (SearchEntry e in proofed)
            {
                SearchResult res = e.ToSearchResult();
                readyResults.Add(res);
                res.ResultID = id + amountOfresultsReady;
                IDTable.AddOrUpdate(res.ResultID, e, (x, y) => { return e; });
                amountOfresultsReady++;
            }

            totalAmount = notReadyResults.Count + Convert.ToInt32(amountOfresultsReady);
            worker = new Task(delegate()
            {
                foreach (SearchEntry e in notReadyResults)
                {
                    SearchResult res;
                    bool proofResult = e.TryProof(out res);
                    if (proofResult)
                    {
                        readyResults.Add(res);
                        res.ResultID = id + amountOfresultsReady;
                        IDTable.AddOrUpdate(res.ResultID, e, (x, y) => { return e; });
                        amountOfresultsReady++;
                        if (res.Weight == 1.0)
                            amountOfCompleteProofs++;
                        waiter.Set();
                    }
                }
                isReady = true;
            });
            worker.Start();
        }

        public SearchResult ImproveResult(uint resultId)
        {
            SearchEntry val=null;
            SearchResult res = null;
            lock (readyResults)
            {
                bool improved = false;
                try
                {
                    if (IDTable.TryGetValue(resultId, out val) && !val.Improved)
                        improved = val.ImproveProof(out res);
                    else
                        if (val == null)
                            throw new ArgumentException("Wrong resultID " + resultId, "resultId");
                }
                catch (Exception ex)
                {
                    Rippling.SystemLog.Add("Error in service, while trying to improve proof: "+ex.Message);
                    improved = false;
                }

                for (int i = 0; i < readyResults.Count; i++)
                    if (readyResults[i].ResultID == resultId)
                    {
                        if (res != null && improved)
                        {
                            res.ResultID = resultId;
                            readyResults[i] = res;
                            return res;
                        }
                        else
                            return readyResults[i];
                    }
            }
            // no result with the specified resultId is found and yet it is present in the IDTable
            throw new NotImplementedException();
        }

        public void WaitUntilReady()
        {
            worker.Wait();
        }

        public void WaitForResultsOrUntilReady(int amount)
        {
            while (amountOfCompleteProofs < amount && !isReady)
            {
                waiter.Reset();
                waiter.Wait(new TimeSpan(0, 0, 0, 1));//TimeSpan for possible race condition prevention.
                //Race condition could make waiter never been set resultin in infinite wait
            }
        }
    }
}
