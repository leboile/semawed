﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="main.aspx.cs" Inherits="FrontEnd.WebForm1" ValidateRequest="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Математический поисковик</title>
    <%--<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.4.1/base-min.css"/>--%>
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.4.1/pure-min.css" />
    <style>
        A.dot {
            text-decoration: none; /* Убираем подчеркивание */
            border-bottom: 1px dashed #000080; /* Добавляем свою линию */
        }

            A.dot:hover {
                color: #f00000; /* Цвет ссылки при наведении на нее курсора */
            }

        .stop-scrolling {
            height: 100%;
            overflow: hidden;
        }

        .ui-widget.success-dialog {
            font-family: Verdana,Arial,sans-serif;
            font-size: .8em;
        }

        .ui-widget-content.success-dialog {
            background: #F9F9F9;
            border: 1px solid #90d93f;
            color: #222222;
        }

        .ui-dialog.success-dialog {
            left: 0;
            outline: 0 none;
            padding: 0 !important;
            position: absolute;
            top: 0;
        }

            .ui-dialog.success-dialog .ui-dialog-content {
                background: none repeat scroll 0 0 transparent;
                border: 0 none;
                overflow: auto;
                position: relative;
                padding: 0 !important;
                margin: 0;
            }

            .ui-dialog.success-dialog .ui-widget-header {
                background: #b0de78;
                border: 0;
                color: #fff;
                font-weight: normal;
            }

            .ui-dialog.success-dialog .ui-dialog-titlebar {
                padding: 0.1em .5em;
                position: relative;
                font-size: 1em;
            }

        .ui-widget-overlay {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: #aaaaaa;
            opacity: 0.3;
        }
    </style>
</head>

<body>
    <form id="form1" class="pure-form" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="True" EnablePartialRendering="true">
        </asp:ScriptManager>
        <asp:HiddenField ID="HiddenFieldSessionID" runat="server" />
        <asp:HiddenField ID="HiddenFieldAllowPostback" Value="true" runat="server" />
        <asp:HiddenField ID="HiddenFieldInput" runat="server" />
        <div class="pure-menu pure-menu-open pure-menu-horizontal" style="text-align: left">
            <ul>
                <li class="pure-menu-selected" id="menuItemMathML"><a href="#" onclick="ShowMathMLInput();">Ввести MathML</a></li>
                <%-- <li id="menuItemTeX"><a href="#" onclick="ShowTeXInput();">Ввести LaTeX</a></li>--%>
                <li id="menuItemAbout"><a href="#" onclick="ShowAbout();">О проекте</a></li>
                <li id="menuItemEn"><a href="main_en.aspx">English</a></li>
            </ul>
        </div>
        <div id="aboutContainer"  style="display: none">
            <asp:TextBox ID="TextBox1" runat="server" Height="156px" ReadOnly="True" TextMode="MultiLine" Width="633px" Font-Bold="True" Font-Size="Large" BorderStyle="None">Данный сайт посвящен проекту математического поиска. Вы можете ввести интересующую вас формулу в специальное поле ввода, а система проведет поиск по разделам Information Retrieval и Symbolic Computation проекта arXiv.org. Особенностью данного поиска является попытка доказать равенство введенной вами формулы и каждой формулы из полученного результата.</asp:TextBox>
        </div>
        <div id="mathmlInputContainer" style="display: none">
            <div id="editorContainer">
            </div>
            <br />
            <%--<asp:HyperLink ID="HyperLink3" class="dot" ForeColor="Blue" runat="server" onclick="ShowTeXInput();">Ввести LaTeX</asp:HyperLink>--%>
        </div>
        <div id="texInputContainer" style="display: none">
            TeX:
            <asp:TextBox ID="TeXInput" runat="server" Width="524px"></asp:TextBox>
            <br />
            <%--<asp:HyperLink ID="HyperLink2" class="dot" ForeColor="Blue" runat="server" onclick="ShowMathMLInput();">Ввести MathML</asp:HyperLink>--%>
        </div>

        <div id="modalLoader" style="text-align: left; display: none;">
            <asp:Image ID="loaderImg" ImageUrl="~/images/loader_big.gif" runat="server" Height="64px" Width="64px" />
        </div>

        <div id="resultsDiv" style="overflow: auto;">
            <div style="text-align: center">
                <asp:Button ID="searchBtn" class="pure-button pure-button-primary" runat="server" Text="Найти!" OnClientClick=" return GetInput();" OnClick="searchBtn_Click" Width="355px" Height="43px" />
                <br />
                <br />
            </div>
            <asp:UpdatePanel ID="resUpdate" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="searchResultPanel" runat="server">
                    </asp:Panel>
                    <%--<asp:UpdateProgress runat="server" AssociatedUpdatePanelID="resUpdate" DisplayAfter="1">
                        <ProgressTemplate>
                            <div id="loaderDiv" style="text-align: center">
                                <asp:Image ID="loaderGid" ImageUrl="~/images/loader.gif" runat="server" Height="24px" Width="24px" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>--%>
                    <div style="text-align: center">
                        <asp:Button ID="nextRes" class="pure-button pure-button-primary" runat="server" Text="Еще..." OnClick="nextRes_Click" Width="633px" Visible="False" />
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="searchBtn" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="nextRes" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </form>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script src="http://andywer.github.io/jquery-dim-background/jquery.dim-background.min.js"></script>
    <script type="text/javascript"
        src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
    </script>
    <script src="http://www.wiris.net/demo/editor/editor"></script>

    <script type="text/javascript">
        //function fillInput() {
        //    document.getElementById("inputForm").value = "<math><apply><plus/><apply><power/><ci>x</ci><ci>n</ci></apply><apply><power/><ci>y</ci><ci>n</ci></apply></apply></math>";
        //    MathJax.Hub.Queue(['Typeset', MathJax.Hub]);
        //}
        function OnSuccess(response) {
            //alert(response);
        }
        function OnError(error) {
            alert(error);
        }

        function improve(arg1) {
            ShowModalLoader();
            var sessionId = document.getElementById('<%=HiddenFieldSessionID.ClientID%>').value;
            PageMethods.ImproveResult(arg1, sessionId, OnSuccess, OnError);
            __doPostBack('<%=resUpdate.ClientID%>', 'populate');
        }

        function showProof(arg1) {
            MathJax.Hub.Queue(['Typeset', MathJax.Hub]);
            $("#proofDiv" + arg1).dialog(
           {
               dialogClass: 'success-dialog',
               width: 600,
               height: 600,
               modal: true,
               open: function (type, data) {
                   document.getElementById('<%=HiddenFieldAllowPostback.ClientID%>').value = 'false';
               },

               close: function (type, data) {
                   document.getElementById('<%=HiddenFieldAllowPostback.ClientID%>').value = 'true';
               }
           });
           }

           $(window).scroll(function () {
               if (($(window).scrollTop() == $(document).height() - $(window).height()) && document.getElementById('HiddenFieldAllowPostback').value == 'true') {
                   var resultsDone = document.getElementById("HiddenFieldResultsDone");
                   var nextBtn = document.getElementById("nextRes");
                   if (nextBtn != null) {
                       var pMathML = document.getElementById("<%=HiddenFieldInput.ClientID%>").value.toString();
                       if (pMathML.indexOf("math") != -1)
                           editor.setMathML(pMathML);
                       ShowModalLoaderNoDim();
                       nextBtn.click();
                       nextBtn.disabled = true;
                   }
               }
           });


           function ShowModalLoaderNoDim() {
               $("#modalLoader").dialog(
               {
                   resizable: false,
                   height: 'auto',
                   width: 150,
                   modal: true,
                   closeText: '',
                   bgiframe: true,
                   draggable: false,
                   closeOnEscape: false,
                   dialogClass: "noclose",
                   open: function (type, data) {
                       //$(this).parent().appendTo($("form:first"));
                       // $('body').css('overflow', 'auto'); //IE scrollbar fix for long checklist templates
                   }
               });
           }

           function ShowModalLoader() {
               $("#modalLoader").dialog(
               {
                   resizable: false,
                   height: 'auto',
                   width: 150,
                   modal: true,
                   closeText: '',
                   bgiframe: true,
                   draggable: false,
                   closeOnEscape: false,
                   dialogClass: "noclose",
                   open: function (type, data) {
                       //$(this).parent().appendTo($("form:first"));
                       //$('body').css('overflow', 'auto'); //IE scrollbar fix for long checklist templates
                       $(this).dimBackground({
                           darkness: 0.4  // 0: no dimming, 1: completely black
                       });
                   }
               });
               $("button.ui-dialog-titlebar-close").hide();
           }

           function CloseModalLoader() {
               $("#modalLoader").undim();
               $("#modalLoader").dialog("close");
           }

           function ShowTeXInput() {
               $(".pure-menu-selected").toggleClass("pure-menu-selected");
               $("#menuItemTeX").toggleClass("pure-menu-selected");
               //editor.close();
               $("#mathmlInputContainer").hide();
               $("#aboutContainer").hide();
               $("#texInputContainer").show();
               $("#resultsDiv").show();
           }

           function ShowAbout() {
               $(".pure-menu-selected").toggleClass("pure-menu-selected");
               $("#menuItemAbout").toggleClass("pure-menu-selected");
               //editor.close();
               $("#mathmlInputContainer").hide();
               $("#texInputContainer").hide();
               $("#aboutContainer").show();
               $("#resultsDiv").hide();
           }

           function ShowMathMLInput() {
               $(".pure-menu-selected").toggleClass("pure-menu-selected");
               $("#menuItemMathML").toggleClass("pure-menu-selected");
               $("#texInputContainer").hide();
               $("#aboutContainer").hide();
               $("#mathmlInputContainer").show();
               $("#resultsDiv").show();
               LoadWiris();
           }

           var editor;
           function LoadWiris() {
               if (typeof editor === 'undefined') {
                   editor = com.wiris.jsEditor.JsEditor.newInstance({ 'checkSyntax': 'true', 'language': 'en' });//'checkSyntax': 'true', 
                   editor.insertInto(document.getElementById('editorContainer'));
               }
           }

           function GetInput() {
               ShowModalLoader();
               //var url = "www.wiris.net/demo/editor/mathml2content?mml=" + editor.getMathML();
               var pMathML = editor.getMathML().toString();
               if (editor.isFormulaEmpty() || pMathML.indexOf('merror') != -1) {
                   alert("Введите корректную формулу");
                   CloseModalLoader();
                   return false;
               }
               else {
                   document.getElementById("<%=HiddenFieldInput.ClientID%>").value = pMathML;
                   return true;
               }
               //$.get(url, function (data) {
               //   if (typeof res == "string") {
               //        document.getElementById("<%=HiddenFieldInput.ClientID%>").value = data;
               //    }
               //    else {
               //         document.getElementById("<%=HiddenFieldInput.ClientID%>").value = "NO STRING";
               //      }
               //  });
           }

        function pageLoad(sender, args) {
            //var cb = MathJax.Callback([CloseModalLoader]);
            MathJax.Hub.Queue(['Typeset', MathJax.Hub]);
            //MathJax.Hub.Queue(["CallBack", MathJax, CloseModalLoader]);
            ShowMathMLInput();
            var pMathML = document.getElementById("<%=HiddenFieldInput.ClientID%>").value.toString();
            if (pMathML.indexOf("math") != -1)
                editor.setMathML(pMathML);
            CloseModalLoader();
        }
    </script>
</body>
</html>
