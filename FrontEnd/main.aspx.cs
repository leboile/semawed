﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Xsl;
using System.Xml;
using Service;

namespace FrontEnd
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        ISearcher channel;
        public class SearchResultOutput
        {
            public SearchResult result;
            public bool isImproved;
            public SearchResultOutput(SearchResult r, bool improved)
            {
                result = r;
                isImproved = improved;
            }
        }

        private static HashSet<System.Web.SessionState.HttpSessionState> sessions = new HashSet<System.Web.SessionState.HttpSessionState>();
        private static XslCompiledTransform converter = new XslCompiledTransform();


        public string SearchQueryPMathML
        {
            get { return Session["searchQueryPMathML"] as string; }
            set { Session["searchQueryPMathML"] = value; }
        }

        public string SearchQuery
        {
            get { return Session["searchQuery"] as string; }
            set { Session["searchQuery"] = value; }
        }

        public List<SearchResultOutput> SearchResults
        {
            get { return Session["searchResults"] as List<SearchResultOutput>; }
            set { Session["searchResults"] = value; }
        }

        public bool ResultsDone
        {
            get
            {
                return (bool)Session["resultsDone"];
            }
            set
            {
                Session["resultsDone"] = value;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            sessions.Add(Session);
            if (SearchQueryPMathML == null)
                SearchQueryPMathML = String.Empty;
            if (SearchQuery == null)
                SearchQuery = String.Empty;
            if (SearchResults == null)
                SearchResults = new List<SearchResultOutput>();
            if (Session["resultsDone"] == null)
                ResultsDone = false;
            HiddenFieldSessionID.Value = Session.SessionID;
            HiddenFieldInput.Value = SearchQueryPMathML;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                string arg = Request["__EVENTARGUMENT"];
                if (arg == "populate")
                    PopulateSearchResults();
            }
            else
            {
                //iframeDiv.Controls.Add(new LiteralControl("<iframe runat=\"server\" name=\"cnxIframe\" id=\"cnxIframe\" class=\"iframe\" width=\"100%\" height=\"100%\" frameborder=\"0\" src=\"http://cnx.org/math-editor/popup#editor\"></iframe>"));
                //ClientScript.RegisterStartupScript(this.GetType(), "MathJax", "<script type=\"text/javascript\" src=\"http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-MML-AM_HTMLorMML\"></script>");
                //ClientScript.RegisterOnSubmitStatement(this.GetType(),"ReMath","MathJax.Hub.Queue([\"Typeset\", MathJax.Hub]);");
                converter.Load(Server.MapPath("mmlctop.xsl"));
            }
        }

        protected void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                nextRes.Visible = false;
                System.Net.WebClient client = new System.Net.WebClient();
                UriBuilder builder = new UriBuilder();
                builder.Host = "www.wiris.net";
                builder.Path = "demo/editor/mathml2content";
                builder.Query = "mml=" + HiddenFieldInput.Value;
                string inputQuery = client.DownloadString(builder.Uri).Trim();
                // string inputQuery = "<math><apply><plus/><apply><power/><ci>x</ci><ci>n</ci></apply><apply><power/><ci>y</ci><ci>n</ci></apply></apply></math>";
                System.Diagnostics.Debug.Write("IQ: " + inputQuery);
                if (inputQuery != "" && inputQuery.IndexOf("merror") < 0)
                {
                    SearchQueryPMathML = HiddenFieldInput.Value;
                    SearchQuery = inputQuery;
                    SearchResults.Clear();
                    ResultsDone = false;
                    WebHttpBinding binding = new WebHttpBinding();
                    binding.MaxBufferSize = Int32.MaxValue;
                    binding.MaxReceivedMessageSize = Int32.MaxValue;
                    using (ChannelFactory<ISearcher> cf = new ChannelFactory<ISearcher>(binding, "http://localhost:8200"))
                    {
                        cf.Endpoint.Behaviors.Add(new WebHttpBehavior());
                        channel = cf.CreateChannel();
                        SearchResultPage result = channel.S(SearchQuery);
                        if (result.Results.Count == 0)
                            searchResultPanel.Controls.Add(new LiteralControl("Ничего не найдено"));
                        else
                        {
                            foreach (SearchResult r in result.Results)
                                SearchResults.Add(new SearchResultOutput(r, false));
                            PopulateSearchResults();
                            if (result.Results.Count > 9)
                                nextRes.Visible = true;
                        }
                    }
                }
                else
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "s", "window.alert('Некорректный ввод');", true);
            }
            catch (Exception)
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "s", "window.alert('Error occured');", true);
            }
        }

        protected void nextRes_Click(object sender, EventArgs e)
        {
            try
            {
                int currentAmount = SearchResults.Count;
                if (ResultsDone)
                {
                    nextRes.Visible = false;
                    PopulateSearchResults();
                    return;
                }

                try
                {
                    WebHttpBinding binding = new WebHttpBinding();
                    binding.MaxBufferSize = Int32.MaxValue;
                    binding.MaxReceivedMessageSize = Int32.MaxValue;
                    using (ChannelFactory<ISearcher> cf = new ChannelFactory<ISearcher>(binding, "http://localhost:8200"))
                    {
                        cf.Endpoint.Behaviors.Add(new WebHttpBehavior());
                        channel = cf.CreateChannel();
                        SearchResultPage result = channel.Search(SearchQuery, currentAmount + 1, currentAmount + 11);
                        if (result.Results.Count > 0 && result.From >= 0 && result.To >= 0)
                            foreach (SearchResult r in result.Results)
                                SearchResults.Add(new SearchResultOutput(r, false));
                        else
                            ResultsDone = true;
                    }
                }
                finally
                {
                    PopulateSearchResults();
                }
            }
            catch (Exception)
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "s", "window.alert('Error occured');", true);
            }
        }

        //public void RaisePostBackEvent(string arg)
        //{
        //    if (arg.IndexOf("inputForm") >= 0)
        //    {
        //        inputForm.Focus();
        //        inputForm.Text = arg.Remove(0, 9);
        //        inputForm_TextChanged(inputForm, null);
        //    }
        //}

        //<annotation encoding="application/x-tex">TexMath</annotation>
        private static Regex getTeX = new Regex("<annotation encoding=\"application/x-tex\">([^<]*)</annotation>");
        private void PopulateSearchResults()
        {
            searchResultPanel.Controls.Add(new LiteralControl("<div style=\"text-align:center\"><table width=\"100%\" class=\"pure-table pure-table-bordered\">"));
            for (int i = 0; i < SearchResults.Count; i++)
            {
                try
                {
                    SearchResult r = SearchResults[i].result;
                    bool improved = SearchResults[i].isImproved;
                    string title = r.Description.Split('\t')[0].Replace("\n", "<br/>");
                    if (title.Length > 100)
                        title = title.Substring(0, 100) + "...";
                    string description = r.Description.Remove(0, title.Length + 1);
                    int index = description.IndexOf(r.Presentation);
                    if (index < 0)
                    {
                        if (description.Length > 100)
                        {
                            int spaceindex = description.IndexOf(' ', 100);
                            if (spaceindex < 0)
                                spaceindex = 100;
                            description = description.Substring(0, spaceindex);
                        }
                        description = "$$" + r.Presentation + "$$" + "<br/>найдено поблизости следующего отрывка<br/><br/>" + "..." + description;
                    }
                    else
                    {
                        if (description.Length > 100)
                            description = description.Substring(index / 2, 1.5 * index > description.Length ? description.Length : index);
                        description = "$$" + r.Presentation + "$$" + "<br/>найдено поблизости следующего отрывка<br/><br/>" + "..." + description.Replace(r.Presentation, "$$" + r.Presentation + "$$");
                    }
                    description = description + "...";
                    searchResultPanel.Controls.Add(new LiteralControl("<tr>"));
                    //searchResultPanel.Controls.Add(new LiteralControl("<td>№" + (i + 1) + "</td>"));
                    searchResultPanel.Controls.Add(new LiteralControl("<td>№" + (i + 1) + "<br/><a href=\"" + r.Location + "\" target=\"_blank\">" + title + "</a></td>"));
                    //searchResultPanel.Controls.Add(new LiteralControl("<td>" +description+"</td"));
                    searchResultPanel.Controls.Add(new LiteralControl("<td><div style=\"text-align:left;vertical-align:top\">" + description + "</div>"));
                    searchResultPanel.Controls.Add(new LiteralControl("<div style=\"text-align:center;vertical-align:bottom\"><a class=\"dot\" onclick=\"showProof(" + r.ResultID.ToString() + ");\">Показать вывод</a></div></td>"));
                    searchResultPanel.Controls.Add(new LiteralControl("<td><div style=\"text-align:center\">" + (r.Weight * 100).ToString("F1") + "%</div>"));
                    Button improveBtn = new Button();
                    improveBtn.UseSubmitBehavior = true;
                    // improveBtn.PostBackUrl = "#";
                    improveBtn.OnClientClick = "ShowModalLoader(); __doPostBack('<%=resUpdate.ClientID%>', 'populate');";
                    //improveBtn.CssClass = "dot";
                    improveBtn.Text = "Уточнить";
                    improveBtn.ID = r.ResultID.ToString();
                    improveBtn.Click += improveBtn_Click;
                    if (!improved)
                        //searchResultPanel.Controls.Add(new LiteralControl("<div style=\"vertical-align:bottom\"><a class=\"dot\" runat =\"server\" onclick=\"improve(" + r.ResultID.ToString() + ");\">Уточнить</a></div></td>"));//результат id:" + r.ResultID.ToString() + "
                        searchResultPanel.Controls.Add(improveBtn);
                    else
                        searchResultPanel.Controls.Add(new LiteralControl("Уточнено</td>"));
                    LiteralControl proofBox = new LiteralControl();
                    // proofBox.TextMode = TextBoxMode.MultiLine;
                    proofBox.Text = r.Proof;
                    System.Text.StringBuilder b = new System.Text.StringBuilder();
                    HtmlTextWriter h = new HtmlTextWriter(new System.IO.StringWriter(b));
                    proofBox.RenderControl(h);
                    searchResultPanel.Controls.Add(new LiteralControl("<div id=\"proofDiv" + r.ResultID.ToString() + "\"style=\"display: none;\">" + b.ToString().Replace("\n", "<br/>") + "</div>"));
                    searchResultPanel.Controls.Add(new LiteralControl("</tr>"));
                }
                catch (Exception)
                {
                    //donothing
                }
            }

            searchResultPanel.Controls.Add(new LiteralControl("</table></div>"));
        }

        protected void improveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Debug.Write("improvebtn_click");
                ImproveResultLocalSession(((Button)sender).ID);
                ClientScript.RegisterClientScriptBlock(this.GetType(), "s", "__doPostBack('<%=resUpdate.ClientID%>', 'populate');", true);
            }
            catch (Exception)
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "s", "window.alert('Error occured');", true);
            }
        }

        public void ImproveResultLocalSession(string resultId)
        {
            System.Diagnostics.Debug.Write("Improve: " + resultId);
            try
            {
                uint id = Convert.ToUInt32(resultId);
                using (ChannelFactory<ISearcher> cf = new ChannelFactory<ISearcher>(new WebHttpBinding(), "http://localhost:8200"))
                {
                    cf.Endpoint.Behaviors.Add(new WebHttpBehavior());
                    ISearcher channel = cf.CreateChannel();
                    SearchResult r = channel.ImproveResult(id);
                    List<SearchResultOutput> SearchResults = Session["searchResults"] as List<SearchResultOutput>;
                    if (r.Weight != -1)
                        for (int i = 0; i < SearchResults.Count; i++)
                            if (SearchResults[i].result.ResultID == id)
                            {
                                (SearchResults[i]).result = r;
                                (SearchResults[i]).isImproved = true;
                                break;
                            }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        //[System.Web.Services.WebMethod]
        //public static void ImproveResult(string resultId, string sessionId)
        //{
        //    System.Diagnostics.Debug.Write("Improve: " + resultId);
        //    try
        //    {
        //        uint id = Convert.ToUInt32(resultId);
        //        using (ChannelFactory<ISearcher> cf = new ChannelFactory<ISearcher>(new WebHttpBinding(), "http://localhost:8200"))
        //        {
        //            cf.Endpoint.Behaviors.Add(new WebHttpBehavior());
        //            ISearcher channel = cf.CreateChannel();
        //            SearchResult r = channel.ImproveResult(id);
        //            System.Web.SessionState.HttpSessionState session = sessions.First(x => x.SessionID == sessionId);
        //            List<SearchResultOutput> SearchResults = session["searchResults"] as List<SearchResultOutput>;
        //            if (r.Weight != -1)
        //                for (int i = 0; i < SearchResults.Count; i++)
        //                    if (SearchResults[i].result.ResultID == id)
        //                    {
        //                        (SearchResults[i]).result = r;
        //                        (SearchResults[i]).isImproved = true;
        //                        break;
        //                    }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        //protected void inputForm_TextChanged(object sender, EventArgs e)
        //{
        //    if (inputForm.Text.Trim().Length > 0)
        //    {
        //        try
        //        {
        //            string input = inputForm.Text.Trim().Replace("xmlns=\"http://www.w3.org/1998/Math/MathML\"", "");
        //            XmlReader reader = XmlReader.Create(new System.IO.StringReader(input));
        //            System.IO.StringWriter writer = new System.IO.StringWriter();
        //            converter.Transform(reader, XmlWriter.Create(writer));
        //            previewInput.Text = writer.ToString();
        //            previewText.Text = "Предпросмотр: ";
        //            inputForm.Focus();
        //        }
        //        catch (Exception)
        //        {
        //            //logsomething
        //            previewInput.Text = "";
        //            previewText.Text = "Не могу разобрать поисковой запрос";
        //        }
        //    }
        //    else
        //    {
        //        previewText.Text = "Предпросмотр: ";
        //        previewInput.Text = "";
        //    }
        //}
    }
}