﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface ISearcher
    {
        [OperationContract]
        [WebGet]
        SearchResultPage S(string q);

        [OperationContract]
        [WebGet(UriTemplate="Search?q={q}&from={from}&to={to}")]
        SearchResultPage Search(string q, int from, int to);
        [OperationContract]
        [WebGet(UriTemplate = "ImproveResult?resultId={resultId}")]
        SearchResult ImproveResult(uint resultId);

        [OperationContract]
        [WebInvoke]
        string SubmitExpression(NewEntry entry);

        [OperationContract]
        [WebInvoke]
        void Init();

        [OperationContract]
        [WebInvoke]
        string ClearCache(string secret);
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "Service.ContractType".
    [DataContract]
    public class SearchQuery
    {
        string searchInput;
        [DataMember]
        public string q
        {
            get
            {
                return searchInput;
            }
            set
            {
                searchInput = value;
            }
        }

        int f = 0;
        int t = 0;
        [DataMember]
        public int from
        {
            get
            {
                return f;
            }
            set
            {
                f = value;
            }
        }

        [DataMember]
        public int to
        {
            get
            {
                return t;
            }
            set
            {
                t = value;
            }
        }
    }

    [DataContract]
    public class SearchResultPage
    {
        [DataMember]
        public List<SearchResult> Results = new List<SearchResult>();

        int from = 0;
        int to = 0;

        [DataMember]
        public int From
        {
            get
            {
                return from;
            }
            set
            {
                from = value;
            }
        }

        [DataMember]
        public int To
        {
            get
            {
                return to;
            }
            set
            {
                to = value;
            }
        }

        public static SearchResultPage GetErrorPage(string message)
        {
            SearchResultPage errPage = new SearchResultPage();
            errPage.From =-1;
            errPage.To = -1;
            SearchResult errResult = new SearchResult();
            errResult.Content=message;
            errResult.Weight = -1;
            errPage.Results.Add(errResult);
            return errPage;
        }
    }

    [DataContract]
    public class SearchResult
    {
        double weight = 0.0;
        string location = "";
        string content = "";
        string presentation = "";
        string proof = "";
        string description = "";
        uint ID = 0;

        [DataMember]
        public double Weight
        {
            get { return weight; }
            set { weight = value; }
        }

        [DataMember]
        public string Content
        {
            get { return content; }
            set { content = value; }
        }

        [DataMember]
        public string Presentation
        {
            get { return presentation; }
            set { presentation = value; }
        }

        [DataMember]
        public string Location
        {
            get { return location; }
            set { location = value; }
        }

        [DataMember]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        [DataMember]
        public uint ResultID
        {
            get { return ID; }
            set { ID = value; }
        }

        [DataMember]
        public string Proof
        {
            get { return proof; }
            set { proof = value; }
        }
    }

    [DataContract]
    public class NewEntry
    {
        string location = "";
        string content = "";
        string secret = "";
        string presentation = "";
        string description = "";
        [DataMember]
        public string Content
        {
            get { return content; }
            set { content = value; }
        }

        [DataMember]
        public string Location
        {
            get { return location; }
            set { location = value; }
        }

        [DataMember]
        public string Presentation
        {
            get { return presentation; }
            set { presentation = value; }
        }

        [DataMember]
        public string Secret
        {
            get { return secret; }
            set { secret = value; }
        }

        [DataMember]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
    }
}
