﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semawed
{
    internal class SkeletonNode
    {
        private BinaryTree node;
        public BinaryTree Node
        {
            get
            {
                return node;
            }
        }
        private SkeletonNode left;
        public SkeletonNode Left
        {
            get
            {
                return left;
            }
        }

        private SkeletonNode right;
        public SkeletonNode Right
        {
            get
            {
                return right;
            }
        }

        private BinaryTree correspondingNode;
        public BinaryTree CorrespondingNode
        {
            get
            {
                return correspondingNode;
            }
        }

        public SkeletonNode(BinaryTree skeletonNode, BinaryTree originalNode)
        {
            node = skeletonNode;
            correspondingNode = originalNode;
        }

        public SkeletonNode GuaranteedAdd(BinaryTree originalNode)//returns added node
        {
            if (this.Node.Data.TypeOfElement != ElementType.Function && this.Node.Data.TypeOfElement != ElementType.ServiceObject)
                throw new InvalidOperationException("Node cannot have children");
            if (originalNode == null)
                throw new ArgumentNullException("originalNode", "Node is null or has a wrong type");
            if (node.Left == null && left != null || node.Left != null && left == null)
                throw new InvalidOperationException("Something went wrong. There is disagreement between nodes. Left");
            if (node.Right == null && right != null || node.Right != null && right == null)
                throw new InvalidOperationException("Something went wrong. There is disagreement between nodes. Right");
            if (left == null)
            {
                BinaryTree newNode = new BinaryTree(originalNode.Data);
                node.Left = newNode;
                left = new SkeletonNode(node.Left, originalNode);
                return left;
            }
            else
                if (right == null)
                {
                    BinaryTree newNode = new BinaryTree(originalNode.Data);
                    node.Right = newNode;
                    right = new SkeletonNode(node.Right, originalNode);
                    return right;
                }
                else
                {
                    SkeletonNode rightBackup = right;
                    node.Right = new BinaryTree(node.Data);//node:BinaryTree
                    node.Right.IsVirtualNode = true;
                    right = new SkeletonNode(node.Right, originalNode);//right:SkeletonNode
                    node.Right.Left = rightBackup.Node;
                    right.left = rightBackup;
                    BinaryTree newNode = new BinaryTree(originalNode.Data);
                    node.Right.Right = newNode;
                    right.right = new SkeletonNode(node.Right.Right, originalNode);
                    return this.right.right;
                }
        }

        public override bool Equals(object obj)
        {
            SkeletonNode skNode = obj as SkeletonNode;
            if (skNode == null)
                return false;
            if (node == skNode.node && correspondingNode == skNode.correspondingNode)
                return true;
            else
                return false;
        }

        public static bool operator ==(SkeletonNode skeletonNode1, SkeletonNode skeletonNode2)
        {
            if ((object)skeletonNode1 == null && (object)skeletonNode2 == null)
                return true;
            if ((object)skeletonNode1 == null || (object)skeletonNode2 == null)
                return false;
            return skeletonNode1.Equals(skeletonNode2);
        }

        public static bool operator !=(SkeletonNode skeletonNode1, SkeletonNode skeletonNode2)
        {
            return !(skeletonNode1 == skeletonNode2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Returns planar list of Non-leaf skeleton nodes.
        /// </summary>
        /// <returns></returns>
        public IReadOnlyList<SkeletonNode> ToListNoLeaves()
        {
            List<SkeletonNode> result = new List<SkeletonNode>();
            toList(result);
            return result;
        }

        private void toList(List<SkeletonNode> res)
        {
            if (Node.Data.TypeOfElement == ElementType.Function || Node.Data.TypeOfElement == ElementType.ServiceObject)
                res.Add(this);
            if (Left != null)
                Left.toList(res);
            if (Right != null)
                Right.toList(res);
        }
    }

    /// <summary>
    /// Class storing skeleton nodes of the binarytree in a convinient for equals check manner
    /// </summary>
    public class Skeleton
    {
        List<SkeletonNode> skeleton;

        public IReadOnlyList<BinaryTree> ToList()
        {
            List<BinaryTree> result = new List<BinaryTree>();
            foreach (SkeletonNode skNode in skeleton)
                result.AddRange(skNode.ToListNoLeaves().Select(x => x.Node));
            return result;
        }

        public Skeleton(BinaryTree tree)
        {
            skeleton = new List<SkeletonNode>();
            buildSkeleton(tree, skeleton, null);
            //cleanUp();
            //if (skeleton.Count == 0)
            //    throw new NotSupportedException("There is no skeleton");
        }

        private void buildSkeleton(BinaryTree node, List<SkeletonNode> skel, SkeletonNode parent)
        {
            if (node == null)
                return;
            SkeletonNode newParent = parent;
            if (node.Data.Markup == MarkupType.Skeleton)
            {
                if (parent == null)
                {
                    newParent = new SkeletonNode(new BinaryTree(node.Data), node);
                    skel.Add(newParent);
                }
                else
                {
                    //if (parent.Left != null && parent.Right != null)
                    //{
                    //    BinaryTree anotherParent = new BinaryTree(node.Data);
                    //    List<SkeletonNode> newSkel = new List<SkeletonNode>();
                    //    foreach (SkeletonNode skNode in skel)
                    //        newSkel.Add(skNode.DeepCopy());
                    //    skeleton.Add(newSkel);
                    //    newSkel.Add(new SkeletonNode(anotherParent, node));
                    //    buildSkeleton(node.Left, newSkel, anotherParent);
                    //    buildSkeleton(node.Right, newSkel, anotherParent);
                    //}
                    newParent = parent.GuaranteedAdd(node);
                }
            }
            //else
            //{
            //    //dummy skeleton node
            //    if (node.Left == null && node.Right == null && (node.Sibling==null || node.Sibling.Data.Markup==MarkupType.WaveFront))
            //        if (parent == null)
            //        {
            //            newParent = new SkeletonNode(new BinaryTree(new ServiceObject("*", MarkupType.Skeleton)), null);
            //            skel.Add(newParent);
            //        }
            //        else
            //            newParent = parent.GuaranteedAdd(new BinaryTree(new ServiceObject("*", MarkupType.Skeleton)));
            //}
            buildSkeleton(node.Left, skel, newParent);
            buildSkeleton(node.Right, skel, newParent);
        }

        /// <summary>
        /// Causes invalid skeletons. 
        /// </summary>
        //private void cleanUp()
        //{
        //    throw new NotImplementedException();
        //    List<SkeletonNode> cleanUpList = new List<SkeletonNode>();
        //    List<SkeletonNode> listOfPossibleParents = new List<SkeletonNode>();
        //    var res = from s in skeleton select s.ToList();
        //    foreach (List<SkeletonNode> s in res)
        //        listOfPossibleParents.AddRange(s);
        //    listOfPossibleParents = listOfPossibleParents.OrderBy(x => x.CorrespondingNode.Depth).ToList();
        //    foreach (SkeletonNode skNode in skeleton)
        //    {
        //        int depth = skNode.CorrespondingNode.Depth;
        //        int k = depth-1;
        //        while (k >= 0)
        //        {
        //            var parents = from s in listOfPossibleParents where s.CorrespondingNode.Depth == k select s;
        //            if (parents.Any())
        //            {
        //                //some decision must be made about choosing possible parent node
        //                parents.First().GuaranteedAdd(skNode.CorrespondingNode);
        //                cleanUpList.Add(skNode);
        //                foreach (SkeletonNode s in skNode.ToList())
        //                    listOfPossibleParents.Remove(s);
        //                break;
        //            }
        //            else
        //                k--;
        //        }
        //    }
        //    foreach (SkeletonNode skNode in cleanUpList)
        //        skeleton.Remove(skNode);
        //}

        //transitivity implementation would be useful
        public override bool Equals(object obj)
        {
            Skeleton sk = obj as Skeleton;
            if (sk == null)
                return false;
            List<SkeletonNode> skeletonSet1 = skeleton;
            List<SkeletonNode> skeletonSet2 = sk.skeleton;
            if (skeletonSet1.Count != skeletonSet2.Count)
                return false;
            bool[] isTaken = new bool[skeletonSet2.Count];
            for (int i = 0; i < skeletonSet1.Count; i++)
            {
                bool isMatching = false;
                for (int j = 0; j < skeletonSet2.Count; j++)
                    if (!isTaken[j] && skeletonSet1[i].Node == skeletonSet2[j].Node)
                    {
                        isTaken[j] = true;
                        isMatching = true;
                        break;
                    }
                if (!isMatching)
                    return false;
            }
            //for (int i = 0; i < skeletonSet2.Count; i++)
            //    if (!isTaken[i])
            //        return false;
            return true;
        }

        //public override bool Equals(object obj)
        //{
        //    Skeleton sk = obj as Skeleton;
        //    if (sk == null)
        //        return false;
        //    foreach (List<SkeletonNode> skeletonSet1 in skeleton)
        //    {
        //        foreach (List<SkeletonNode> skeletonSet2 in sk.skeleton)
        //        {
        //            bool breakFlag = false;
        //            if (skeletonSet1.Count != skeletonSet2.Count)
        //                break;
        //            bool[] isTaken = new bool[skeletonSet2.Count];
        //            for (int i = 0; i < skeletonSet1.Count; i++)
        //            {
        //                bool isMatching = false;
        //                for (int j = 0; j < skeletonSet2.Count; j++)
        //                    if (!isTaken[j] && skeletonSet1[i].Node == skeletonSet2[j].Node)
        //                    {
        //                        isTaken[j] = true;
        //                        isMatching = true;
        //                        break;
        //                    }
        //                if (!isMatching)
        //                {
        //                    breakFlag = true;
        //                    break;
        //                }
        //            }
        //            if (breakFlag)
        //                break;
        //            else
        //            {
        //                //for (int i = 0; i < skeletonSet2.Count; i++)
        //                //    if (!isTaken[i])
        //                //        return false;
        //                return true;
        //            }   
        //        }
        //    }
        //    return false;   
        //}

        public static bool operator ==(Skeleton skeleton1, Skeleton skeleton2)
        {
            if ((object)skeleton1 == null && (object)skeleton2 == null)
                return true;
            else
                if ((object)skeleton1 == null || (object)skeleton2 == null)
                    return false;
            return skeleton1.Equals(skeleton2);
        }

        public static bool operator !=(Skeleton skeleton1, Skeleton skeleton2)
        {
            return !(skeleton1 == skeleton2);
        }

        public override string ToString()
        {
            StringBuilder SB = new StringBuilder();
            foreach (SkeletonNode t in skeleton)
            {
                SB.AppendLine(t.Node.ToString());
                SB.AppendLine("-----------");
            }
            return SB.ToString();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
