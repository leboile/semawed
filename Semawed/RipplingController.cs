﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Semawed
{
    internal class RipplingController
    {
        private static int ruleLimit = 8;
        private static Action<object> ripplingFunc = new Action<object>(Rippling.RipplingStep1);
        private double maxWeight = Double.MinValue;
        private List<Task> runningTasks = new List<Task>();
        private int eventCounter = 0;
        private Object thisLock = new Object();
        ManualResetEventSlim unificationFinished = new ManualResetEventSlim(false);
        ManualResetEventSlim waitForTaskArrayCopy = new ManualResetEventSlim(true);
        public double CurrentMaxWeight
        {
            get
            {
                return maxWeight;
            }
        }
        private double limit;
        public double Limit
        {
            get
            {
                return limit;
            }
        }

        private bool isFinished = false;
        public bool IsFinished
        {
            get
            {
                return isFinished;
            }
        }

        private RipplingEntry maxEntry = null;
        public RipplingEntry MaxEntry
        {
            get
            {
                return maxEntry;
            }
        }

        public void Finish(bool waitForComplete)
        {
            //here might be stopping of all threads
            if (isFinished)
                return;
            if (!waitForComplete)
            {
                if (maxWeight == double.MinValue)
                    try
                    {
                        if (runningTasks.Count > 0)
                            runningTasks[0].Wait();
                    }
                    catch (AggregateException ae)
                    {
                        Rippling.SystemLog.Add(ae.Message);
                    }
            }
            else
            {
                try
                {
                    Task.WaitAll(runningTasks.ToArray());
                    Console.WriteLine("Waited " + runningTasks.Count + " tasks");
                }
                catch (AggregateException ae)
                {
                    Rippling.SystemLog.Add(ae.Message);
                }
            }
            isFinished = true;
        }

        public void Finish(bool waitForComplete, UnificationController unifController, TimeSpan timeLimit)
        {
            //here might be stopping of all threads
            if (isFinished)
                return;
            DateTime start = DateTime.Now;
            if (!unifController.IsUnificationFinished)
                unificationFinished.Wait(timeLimit);
            unifController.FinishUnificationProcess();
            TimeSpan timeLeft = timeLimit - (DateTime.Now - start);
            if (timeLeft < TimeSpan.Zero)
                timeLeft = TimeSpan.Zero;
            if (!waitForComplete)
            {
                if (maxWeight == double.MinValue)
                    try
                    {
                        if (runningTasks.Count > 0)
                            runningTasks[0].Wait(timeLeft);
                    }
                    catch (AggregateException ae)
                    {
                        Rippling.SystemLog.Add(ae.Message);
                    }
            }
            else
            {
                try
                {
                    waitForTaskArrayCopy.Reset();
                    List<Task> newTaskList = new List<Task>(runningTasks);
                    waitForTaskArrayCopy.Set();
                    Task.WaitAll(newTaskList.ToArray(), timeLeft);
                    //Console.WriteLine("Waited " + runningTasks.Count + " tasks");
                }
                catch (AggregateException ae)
                {
                    Rippling.SystemLog.Add(ae.Message);
                }
            }
            isFinished = true;
            foreach (Task t in runningTasks)
                if (t.Status == TaskStatus.RanToCompletion || t.Status == TaskStatus.Canceled || t.Status == TaskStatus.Faulted)
                    t.Dispose();
        }

        public RipplingController(double stopLimit)
        {
            limit = stopLimit;
        }

        public void NewUnificationEntryEventHandler(object sender, UEEventArgs e)
        {
            eventCounter++;
            if (e == null)
                throw new ArgumentNullException("e");
            if (isFinished)
                return;
            RipplingEntry entry = new RipplingEntry(e.Entry, this);
            //threading goes there
            try
            {
                Task newTask = new Task(ripplingFunc, entry);
                waitForTaskArrayCopy.Wait();
                if (!isFinished)
                {
                    runningTasks.Add(newTask);
                    newTask.Start();
                    newTask.ContinueWith((t) =>
                    {
                        Rippling.SystemLog.Add(t.Exception.Message);
                    }, TaskContinuationOptions.OnlyOnFaulted);
                }
            }
            catch (Exception ex)
            {
                Rippling.SystemLog.Add("Task failed with " + ex.Message);
                throw;
            }
        }

        //not used yet, but might be useful with time limited multithreading
        public void UnificationFinishedEventHandler(object sender, EventArgs e)
        {
            if (e == null)
                throw new ArgumentNullException("e");
            unificationFinished.Set();
        }

        /// <summary>
        /// Calculates result and tells whether calling rippling task should proceed proof
        /// </summary>
        /// <param name="entry"></param>
        /// <returns>Whether Rippling should proceed</returns>
        public bool AddResult(RipplingEntry entry)
        {
            lock (thisLock)
            {
                int k = entry.AppliedRulesToGiven.Count + entry.AppliedRulesToGoal.Count;
                bool structuralEq = BinaryTree.CheckStructure(entry.GivenRoot, entry.GoalRoot, new GenericComparer(), true);
                if (structuralEq && k != 0)
                {
                    double weight = 1.0;
                    foreach (var pair in entry.RelatedUnificationEntry.Substitutions)
                    {
                        if (pair.Value.Data.TypeOfElement == ElementType.Variable && pair.Value.Data.Name.IndexOf("newvar") < 0)
                            weight = weight * 1.1;
                        else
                        {
                            foreach (var p in pair.Value.GetAllLeafNodes())
                                weight = weight - 0.05 * (p.Data.Name.IndexOf("newvar") + 1);
                            //weight = weight - 0.1 * pair.Value.GetTreeInfo().Depth;
                            weight = weight * 9.0 / (8.0 * ((pair.Value.GetTreeInfo().Depth + pair.Value.GetTreeInfo().Total)));

                        }
                    }
                    isFinished = true;
                    if (weight < 0)
                        weight = 0.01;
                    if (weight > 1.0)
                        weight = 1.0;
                    if (weight > maxWeight)
                    {
                        maxWeight = weight;
                        maxEntry = entry.DeepCopy();
                    }
                    return false;
                }
                else
                {
                    double weight = Rippling.FuzzyJaccardSimilarity(entry.GivenRoot, entry.GoalRoot, 0.5, true);
                    var elements = entry.GivenRoot.GetAllElements();
                    elements.SymmetricExceptWith(entry.GoalRoot.GetAllElements());
                    elements.RemoveWhere(x => x.TypeOfElement == ElementType.Variable);
                    //weight = weight - 0.1 * elements.Count;
                    weight = weight / (1.2*elements.Count + 1);
                    if (entry.RelatedUnificationEntry.UnificatedToEqual || (k == 0 && weight > maxWeight))
                    {
                        foreach (var pair in entry.RelatedUnificationEntry.Substitutions)
                        {
                            if (pair.Value.Data.TypeOfElement == ElementType.Variable && pair.Value.Data.Name.IndexOf("newvar") < 0)
                                weight = weight * 1.1;
                            else
                            {
                                foreach (var p in pair.Value.GetAllLeafNodes())
                                    weight = weight - weight*0.01 * (p.Data.Name.IndexOf("newvar") + 1);
                                weight = weight*9.0/(8.0*((pair.Value.GetTreeInfo().Depth + pair.Value.GetTreeInfo().Total)));
                                //
                            }
                        }
                    }
                    if (weight < 0)
                        weight = 0.01;
                    if (weight > 1.0)
                        weight = 1.0;
                    if (weight > maxWeight)
                    {
                        maxWeight = weight;
                        maxEntry = entry.DeepCopy();
                    }
                }

                if (maxWeight > limit)
                {
                    isFinished = true;
                    return false;
                }

                if (entry.AppliedRulesToGiven.Count + entry.AppliedRulesToGoal.Count > ruleLimit)
                    return false;
                else
                    return !isFinished;
            }
        }
    }
}
