﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Runtime.Serialization.Formatters.Binary;

namespace Semawed
{
    [Serializable]
    public class Expression
    {
        private static int totalExprCount = 0;
        private int varCount = 0;
        public int VarCount
        {
            get
            {
                return varCount;
            }
        }
        private List<string> vars = new List<string>(); //names of variables
        public IReadOnlyCollection<string> Variables
        {
            get
            {
                return vars;
            }
        }

        private int varChecksum = 0;
        public int VarChecksum
        {
            get
            {
                return varChecksum;
            }
        }
        private string varPrefix;
        public string Prefix
        {
            get
            {
                return varPrefix;
            }
        }

        private BinaryTreeInfo initInfo;
        public BinaryTreeInfo InitialInformation
        {
            get
            {
                return initInfo;
            }
        }
        private string cHash;
        public string ConstantsHash
        {
            get
            {
                return cHash;
            }
        }

        private string origMathML;
        public string CMathML
        {
            get
            {
                return origMathML;
            }
        }

        private List<Constant> constants = new List<Constant>();
        public IReadOnlyList<Constant> Constants
        {
            get
            {
                return constants;
            }
        }

        private BinaryTree treeRoot;
        public BinaryTree Root
        {
            get
            {
                return treeRoot;
            }
        }

        bool isWaveRule;
        private Expression()
        {
            varPrefix = "";
            varCount = 0;
            treeRoot = null;
            totalExprCount++;
        }

        //public Expression(XmlNode root, bool isWR)
        //{
        //    if (root == null)
        //        throw new ArgumentNullException("root");
        //    varPrefix = "";
        //    origMathML = root.InnerXml;
        //    treeRoot = new BinaryTree(new ServiceObject(root.Name));
        //    isWaveRule = isWR;
        //    buildTree(treeRoot, root);
        //    treeRoot = treeRoot.Left;
        //    treeRoot.Parent = null;
        //    totalExprCount++;
        //    initInfo = treeRoot.GetTreeInfo();
        //    cHash = getConstantsHash();
        //    //treeRoot.TreeChangedEvent += TreeChangedEventHandler;
        //}

        public Expression(string content)
        {
            varPrefix = "";
            origMathML = content;
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(content);
            XmlNode root = doc.DocumentElement;
            treeRoot = new BinaryTree(new ServiceObject(root.Name));
            isWaveRule = false;
            buildTree(treeRoot, root, false);
            if (treeRoot.Left != null)
            {
                treeRoot = treeRoot.Left;
                treeRoot.Parent = null;
            }
            totalExprCount++;
            initInfo = treeRoot.GetTreeInfo();
            cHash = getConstantsHash();
            //treeRoot.TreeChangedEvent += TreeChangedEventHandler;
        }

        public Expression(string content, string prefix)
        {
            varPrefix = prefix;
            origMathML = content;
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(content);
            XmlNode root = doc.DocumentElement;
            treeRoot = new BinaryTree(new ServiceObject(root.Name));
            isWaveRule = false;
            buildTree(treeRoot, root, false);
            if (treeRoot.Left != null)
            {
                treeRoot = treeRoot.Left;
                treeRoot.Parent = null;
            }
            totalExprCount++;
            initInfo = treeRoot.GetTreeInfo();
            cHash = getConstantsHash();
        }

        public Expression(string content, bool isWR)
        {
            varPrefix = "";
            origMathML = content;
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(content);
            XmlNode root = doc.DocumentElement;
            treeRoot = new BinaryTree(new ServiceObject(root.Name));
            isWaveRule = isWR;
            buildTree(treeRoot, root, false);
            treeRoot = treeRoot.Left;
            treeRoot.Parent = null;
            totalExprCount++;
            initInfo = treeRoot.GetTreeInfo();
            cHash = getConstantsHash();
        }

        public Expression(BinaryTree tree)
        {
            varPrefix = "";
            origMathML = "UNDEFINED";
            treeRoot = tree;
            retrieveInfoFromBTree(treeRoot);
            isWaveRule = false;
            HashSet<Variable> v = treeRoot.GetAllVariables();
            foreach (Variable var in v)
                vars.Add(var.Name);
            varCount = vars.Count;
            //treeRoot.TreeChangedEvent += TreeChangedEventHandler;
            totalExprCount++;
            initInfo = treeRoot.GetTreeInfo();
            cHash = getConstantsHash();
        }

        private void retrieveInfoFromBTree(BinaryTree tree)
        {
            if (tree == null)
                return;
            switch (tree.Data.TypeOfElement)
            {
                case ElementType.Variable:
                    varChecksum += tree.Depth + 1;
                    break;
                case ElementType.Constant:
                    constants.Add((Constant)tree.Data);
                    break;
            }
            retrieveInfoFromBTree(tree.Left);
            retrieveInfoFromBTree(tree.Right);
        }

        void buildTree(BinaryTree t, XmlNode node, bool apply)
        {
            int i = 0;
            foreach (XmlNode child in node.ChildNodes)
            {
                if (child.NodeType == XmlNodeType.Element)
                {
                    if (apply && i < 1)
                    {
                        i++;
                        continue;
                    }
                    BinaryTree addedNode = null;
                    switch (child.Name.ToLower())
                    {
                        case "apply":
                            if (child.FirstChild.Name == "ci" || child.FirstChild.Name == "csymbol")
                                addedNode = t.GuaranteedAdd(new Function(child.FirstChild.InnerText));
                            else
                                addedNode = t.GuaranteedAdd(new Function(child.FirstChild.Name));
                            buildTree(addedNode, child, true);
                            break;
                        case "ci":
                            int ind = vars.IndexOf(varPrefix + child.InnerText);
                            varChecksum += t.Depth + 1;

                            if (ind < 0)
                            {
                                if (isWaveRule)
                                    addedNode = t.GuaranteedAdd(new Variable(varPrefix + "WAVERULEVAR" + child.InnerText));
                                else
                                    addedNode = t.GuaranteedAdd(new Variable(varPrefix + child.InnerText + varCount));
                                varCount++;
                                vars.Add(varPrefix + child.InnerText);
                            }
                            else
                            {
                                if (isWaveRule)
                                    addedNode = t.GuaranteedAdd(new Variable(varPrefix + "WAVERULEVAR" + child.InnerText));
                                else
                                    addedNode = t.GuaranteedAdd(new Variable(varPrefix + child.InnerText + ind));
                            }
                            break;
                        case "cn":
                            Constant newConstant = new Constant(child.InnerText);
                            addedNode = t.GuaranteedAdd(newConstant);
                            constants.Add(newConstant);
                            break;
                        case "semantics":
                        case "math":
                        default:
                            if (child.HasChildNodes)
                                buildTree(t, child, false);
                            break;
                    }
                    if (node != null && Element.IsQualifier(node.Name) && addedNode != null)
                        addedNode.qualifier = node.Name;
                }
            }
        }

        //void buildTree(BinaryTree t, XmlNode node)
        //{
        //    switch (node.NodeType)
        //    {
        //        case XmlNodeType.Element:
        //            switch (node.Name.ToLower())
        //            {
        //                case "apply":
        //                    BinaryTree res = null;
        //                    if (node.FirstChild.Name == "csymbol")
        //                        res = t.GuaranteedAdd(new Function("csymbol_" + node.FirstChild.InnerText));
        //                    else
        //                        res = t.GuaranteedAdd(new Function(node.FirstChild.Name));
        //                    buildTree(res, node.FirstChild);
        //                    break;
        //                case "ci":
        //                    int ind = vars.IndexOf(varPrefix + node.InnerText);
        //                    varChecksum += t.Depth + 1;
        //                    if (ind < 0)
        //                    {
        //                        if (isWaveRule)
        //                        {
        //                            t.Add(new Variable(varPrefix + "WAVERULEVAR" + node.InnerText));
        //                        }
        //                        else
        //                            t.Add(new Variable(varPrefix + node.InnerText + varCount));
        //                        varCount++;
        //                        vars.Add(varPrefix + node.InnerText);
        //                    }
        //                    else
        //                    {
        //                        if (isWaveRule)
        //                            t.Add(new Variable(varPrefix + "WAVERULEVAR" + node.InnerText));
        //                        else
        //                            t.Add(new Variable(varPrefix + node.InnerText + ind));
        //                    }
        //                    break;
        //                case "cn":
        //                    Constant newConstant = new Constant(node.InnerText);
        //                    t.Add(newConstant);
        //                    constants.Add(newConstant);
        //                    break;
        //                case "Math":
        //                case "semantics":
        //                case "math":
        //                    if (node.HasChildNodes)
        //                        buildTree(t, node.FirstChild);
        //                    break;
        //                default:
        //                    if (node.HasChildNodes)
        //                        buildTree(t, node.FirstChild);
        //                    break;
        //            }
        //            break;
        //        case XmlNodeType.Text:
        //        case XmlNodeType.XmlDeclaration:
        //        case XmlNodeType.ProcessingInstruction:
        //        case XmlNodeType.Comment:
        //        case XmlNodeType.EndElement:
        //            if (node.HasChildNodes)
        //                buildTree(treeRoot, node.FirstChild);
        //            break;
        //    }

        //    if (node.NextSibling != null)
        //    {
        //        if (t.Left == null)
        //            buildTree(t, node.NextSibling);
        //        else
        //        {
        //            if (node.ParentNode.ChildNodes.Count <= 3 || node.NextSibling.NextSibling == null)//3 because MathML standard demand <apply><operator/> ..args...</apply> so there is always atleast one childnode
        //                buildTree(t, node.NextSibling);
        //            else
        //            {
        //                if (t.Right == null)
        //                {
        //                    t.Add(t.Data);
        //                    buildTree(t.Right, node.NextSibling);
        //                }
        //                else
        //                    throw new InvalidOperationException("Building tree error");
        //            }
        //        }
        //    }
        //}

        public static double CompareElementSets(HashSet<Element> set1, HashSet<Element> set2)
        {
            if (set1 == null)
                throw new ArgumentNullException("set1");
            if (set2 == null)
                throw new ArgumentNullException("set2");
            HashSet<Element> cmp = new HashSet<Element>(set1);
            cmp.IntersectWith(set2);
            double count = set1.Count + set2.Count;
            //return (2*cmp.Count / ((double)count));
            return ((4 * cmp.Count * cmp.Count) / ((double)count * (double)count));
        }

        public static string GetString(Expression expression)
        {
            return BinaryTree.SerializeToString(expression.treeRoot);
        }

        public static Expression LoadFromString(string s)
        {
            return new Expression(BinaryTree.DeserializeFromString(s));
        }


        public static byte[] GetBinary(Expression expression)
        {
            Stream s = null;
            byte[] result = null;
            try
            {
                s = new MemoryStream();
                BinaryFormatter serializer = new BinaryFormatter();
                serializer.Serialize(s, expression);
                result = new byte[s.Length];
                s.Read(result, 0, (int)s.Length);
            }
            finally
            {
                if (s != null)
                    s.Close();
            }

            return result;
        }

        public static Expression LoadFromByteArray(byte[] array)
        {
            MemoryStream stream = null;
            Expression res = null;
            try
            {
                stream = new MemoryStream(array);
                BinaryFormatter deserializer = new BinaryFormatter();
                res = (Expression)deserializer.Deserialize(stream);
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            if (res == null)
                throw new InvalidOperationException("Loading failed");
            return res;
        }

        public static byte[] ToByteArray(Expression expression)
        {
            MemoryStream stream = null;
            byte[] res = null;
            try
            {
                stream = new MemoryStream();
                BinaryFormatter serializer = new BinaryFormatter();
                serializer.Serialize(stream, expression);
                res = stream.ToArray();
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            return res;
        }

        private string getConstantsHash()
        {
            string res = "";
            constants.Sort(new Comparison<Constant>((Constant x, Constant y) =>
            {
                for (int i = 0; i < x.Name.Length; i++)
                {
                    if (i == y.Name.Length)
                        return -1;
                    if (x.Name[i] > y.Name[i])
                        return 1;
                    else
                        if (x.Name[i] < y.Name[i])
                            return -1;
                }
                return 1;
            }));
            foreach (Constant c in constants)
                res += c.Name;
            return res;
        }

        private void printToString(BinaryTree t, int offset, StringBuilder SB)
        {
            if (t == null)
                return;
            for (int i = 0; i < offset; i++)
                SB.Append(" ");
            if (t.Data.TypeOfElement == ElementType.Variable)
                SB.AppendLine(t.Data.ToString().Remove(0, varPrefix.Length));
            else
                SB.AppendLine(t.Data.ToString());
            printToString(t.Left, offset + 1, SB);
            printToString(t.Right, offset + 1, SB);
        }

        public override bool Equals(object obj)
        {
            Expression arg = obj as Expression;
            if (arg == null)
                return false;
            NoPrefixEqualityCmp cmp = new NoPrefixEqualityCmp(varPrefix, arg.varPrefix);
            return BinaryTree.CheckStructure(Root, arg.Root, cmp);
        }

        public bool Equals(object obj, bool accuracyUpToVariables)
        {
            Expression arg = obj as Expression;
            if (arg == null)
                return false;
            if (accuracyUpToVariables)
            {
                TillVariablesEqualityCmp cmp = new TillVariablesEqualityCmp();
                return BinaryTree.CheckStructure(Root, arg.Root, cmp);
            }
            else
                return this.Equals(obj);
        }

        public string ToCMathML()
        {
            return Root.ToCMathML(varPrefix);
        }

        public override string ToString()
        {
            StringBuilder SB = new StringBuilder();
            SB.AppendLine(Root.Data.ToString());
            printToString(Root.Left, 1, SB);
            printToString(Root.Right, 1, SB);
            return SB.ToString();
        }

        public override int GetHashCode()
        {
            return Root.GetHashCode();
        }
    }

    internal class TillVariablesEqualityCmp : IEqualityComparer<Element>
    {
        public bool Equals(Element x, Element y)
        {
            if (x == null && y == null)
                return true;
            else
                if (x == null || y == null)
                    return false;
            if (x.TypeOfElement == ElementType.Variable && y.TypeOfElement == ElementType.Variable)
                return true;
            else
                return x.Equals(y);
        }

        public int GetHashCode(Element obj)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");
            return obj.GetHashCode();
        }
    }

    internal class NoPrefixEqualityCmp : IEqualityComparer<Element>
    {
        private int prefix1;
        private int prefix2;

        public NoPrefixEqualityCmp(string p1, string p2)
        {
            prefix1 = p1.Length;
            prefix2 = p2.Length;
        }

        public bool Equals(Element x, Element y)
        {
            if (x == null && y == null)
                return true;
            else
                if (x == null || y == null)
                    return false;
            if (x.TypeOfElement == ElementType.Variable && prefix1 > 0)
                x = new Variable(x.Name.Remove(0, prefix1), x.Markup);
            if (y.TypeOfElement == ElementType.Variable && prefix2 > 0)
                y = new Variable(y.Name.Remove(0, prefix2), y.Markup);
            return x.Equals(y);
        }

        public int GetHashCode(Element obj)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");
            return obj.GetHashCode();
        }
    }
}
