﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semawed
{
    public class UnificationEntry
    {
        private int similaritiesCount;//e.g. DECOMPOSE and DELETE counter
        public int SimilarCount
        {
            get
            {
                return similaritiesCount;
            }
        }

        private int substitutionsCount;//e.g. ELIMINATE and IMITATE counter
        public int SubstitutionCount
        {
            get
            {
                return substitutionsCount;
            }
        }

        private Dictionary<Element, BinaryTree> substTable = new Dictionary<Element, BinaryTree>();
        public Dictionary<Element, BinaryTree> Substitutions
        {
            get
            {
                return substTable;
            }
        }

        private int cost;//HIDE counter
        public int WRCost
        {
            get
            {
                return cost;
            }
        }

        internal void IncreaseSimilarCount()
        {
            similaritiesCount++;
        }

        internal void IncreaseSubstCount(Element toReplace, BinaryTree replacement)
        {
            substitutionsCount++;
            substTable.Add(toReplace.Copy(), replacement.DeepCopy());
        }

        internal void IncreaseCost()
        {
            cost++;
        }

        private bool unificationComplete;
        public bool UnificationComplete
        {
            get
            {
                return unificationComplete;
            }
        }

        private BinaryTree givenRoot;
        public BinaryTree GivenRoot
        {
            get
            {
                return givenRoot;
            }
        }

        private BinaryTree goalRoot;
        public BinaryTree GoalRoot
        {
            get
            {
                return goalRoot;
            }
        }

        private BinaryTree goalNode;
        internal BinaryTree GoalNode
        {
            get
            {
                return goalNode;
            }
            set
            {
                goalNode = value;
            }
        }

        private BinaryTree givenNode;
        internal BinaryTree GivenNode
        {
            get
            {
                return givenNode;
            }
            set
            {
                givenNode = value;
            }
        }

        Skeleton givenSkeleton;
        Skeleton goalSkeleton;
        public Skeleton GivenSkeleton
        {
            get
            {
                if (!unificationComplete)
                    throw new InvalidOperationException("Unification is not completed yet");
                if (givenSkeleton == null)
                    givenSkeleton = new Skeleton(GivenRoot);
                return givenSkeleton;
            }
        }

        public Skeleton GoalSkeleton
        {
            get
            {
                if (!unificationComplete)
                    throw new InvalidOperationException("Unification is not completed yet");
                if (goalSkeleton == null)
                    goalSkeleton = new Skeleton(GoalRoot);
                return goalSkeleton;
            }
        }

        bool equalOnUnification = false;
        public bool UnificatedToEqual
        {
            get
            {
                return equalOnUnification;
            }
        }

        public bool CompleteUnification()
        {
            if (unificationComplete)
                return true;
            Unification.SetMarkup(givenRoot, MarkupType.Skeleton, MarkupType.Undefined);
            Unification.SetMarkup(goalRoot, MarkupType.Skeleton, MarkupType.Undefined);
            givenSkeleton = new Skeleton(GivenRoot);
            goalSkeleton = new Skeleton(GoalRoot);
            try
            {
                if (givenSkeleton == goalSkeleton)
                {
                    unificationComplete = true;
                    if (givenRoot == goalRoot)
                        equalOnUnification = true;
                    return true;
                }
                else
                {
                    Unification.SetMarkup(givenRoot, MarkupType.Undefined, MarkupType.Skeleton);
                    Unification.SetMarkup(goalRoot, MarkupType.Undefined, MarkupType.Skeleton);
                    return false;
                }
            }
            catch (InvalidOperationException ex)
            {
               // Unification.SetMarkup(givenRoot, MarkupType.Undefined, MarkupType.Skeleton);
                //Unification.SetMarkup(goalRoot, MarkupType.Undefined, MarkupType.Skeleton);
                //log
                Rippling.SystemLog.Add("IOE in CompleteUnification call "+ex.Message);
                Rippling.SystemLog.Add(givenRoot.ToString());
                Rippling.SystemLog.Add(goalRoot.ToString());
                return false;
            }
        }

        internal UnificationEntry MoveByPath(bool[][] path)
        {
            givenNode = givenRoot.GetNodeUsingPath(path[0]);
            goalNode = GoalRoot.GetNodeUsingPath(path[1]);
            return this;
        }

        internal UnificationEntry(UnificationEntry entry)
        {
            if (entry == null)
                throw new ArgumentNullException("entry", "Entry cannot be null");

            if (entry.goalNode == null || entry.givenNode == null)
                throw new InvalidOperationException("Invalid state of the entry.");
            goalNode = entry.goalNode.DeepCopyWholeTree(out goalRoot);
            givenNode = entry.givenNode.DeepCopyWholeTree(out givenRoot);
            foreach (var pair in entry.substTable)
                substTable.Add(pair.Key, pair.Value);
            cost = entry.WRCost;
            substitutionsCount = entry.SubstitutionCount;
            similaritiesCount = entry.SimilarCount;
            unificationComplete = entry.unificationComplete;
        }

        public UnificationEntry(BinaryTree tree, BinaryTree target)
        {
            if (tree == null)
                throw new ArgumentNullException("tree", "BinaryTree node cannot be null");
            if (target == null)
                throw new ArgumentNullException("target", "BinaryTree node cannot be null");

            similaritiesCount = 0;
            substitutionsCount = 0;
            cost = 0;
            unificationComplete = false;
            givenRoot = tree;
            goalRoot = target;
            givenNode = givenRoot;
            goalNode = goalRoot;
        }

        public override string ToString()
        {
            StringBuilder SB = new StringBuilder();
            SB.AppendLine("Given: ");
            SB.AppendLine(GivenRoot.ToString());
            SB.AppendLine("Goal: ");
            SB.AppendLine(GoalRoot.ToString());
            return SB.ToString();
        }

    }
}
