﻿using System;
using System.Text;
using System.IO;
namespace Semawed
{
    public class Log
    {
        private StringBuilder messages;
        private string pathToJournal;
        public Log(string name)
        {
            messages = new StringBuilder();
            if (!Directory.Exists("logs"))
                Directory.CreateDirectory("logs");
            pathToJournal = Path.Combine("logs", name);
            Add("Journal init");
            Flush();
        }

        public void Add(string text)
        {
            lock (messages)
            {
                messages.AppendLine("[" + DateTime.Now.ToLongTimeString() + "]" + text);
                if (messages.Length > 1024)
                    Flush();
            }
        }

        public void Add(StringBuilder text)
        {
            lock (messages)
            {
                messages.AppendLine("[" + DateTime.Now.ToLongTimeString() + "]" + text);
                if (messages.Length > 1024)
                    Flush();
            }
        }

        public void Flush()
        {
            lock (messages)
            {
                File.AppendAllText(pathToJournal, messages.ToString());
                messages.Clear();
            }
        }

        public void FlushOnscreen()
        {
            lock (messages)
            {
                Console.WriteLine(messages.ToString());
                messages.Clear();
            }
        }

        ~Log()
        {
            Flush();
        }
    }
}
