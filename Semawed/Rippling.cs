﻿using System;
using System.Runtime.CompilerServices;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

[assembly: InternalsVisibleTo("UnitTest")]
namespace Semawed
{
    public class ProofResult
    {
        bool success;
        /// <summary>
        /// Indicates whether proof was able to provide some value 
        /// </summary>
        public bool IsSuccess
        {
            get
            {
                return success;
            }
        }
        double similarityValue;
        public double Similarity
        {
            get
            {
                return similarityValue;
            }
        }

        Dictionary<string, string> substTable;
        public IReadOnlyDictionary<string, string> Substitutions
        {
            get
            {
                return substTable;
            }
        }

        List<string> rulesAppliedLeft;
        public IReadOnlyList<string> RulesAppliedToLeft
        {
            get
            {
                return rulesAppliedLeft;
            }
        }

        List<string> rulesAppliedRight;
        public IReadOnlyList<string> RulesAppliedToRight
        {
            get
            {
                return rulesAppliedRight;
            }
        }

        string left;
        public string LeftExpression
        {
            get
            {
                return left;
            }
        }

        string right;
        public string RightExpression
        {
            get
            {
                return right;
            }
        }

        internal ProofResult(Expression ex1, Expression ex2, double weight)
        {
            if (weight == double.MinValue)
            {
                success = false;
                left = null;
                right = null;
                similarityValue = double.MinValue;
                rulesAppliedLeft = null;
                rulesAppliedRight = null;
                substTable = null;
                return;
            }
            if (ex1 == null)
                throw new ArgumentNullException("ex1");
            if (ex2 == null)
                throw new ArgumentNullException("ex2");
            success = true;
            string prefix1 = ex1.Prefix;
            string prefix2 = ex2.Prefix;
            if (String.IsNullOrEmpty(prefix1))
                prefix1 = " ";
            if (String.IsNullOrEmpty(prefix2))
                prefix2 = " ";

            left = ex1.Root.ToCMathML().Replace(prefix1, String.Empty).Replace(prefix2, String.Empty);
            right = ex2.Root.ToCMathML().Replace(prefix1, String.Empty).Replace(prefix2, String.Empty);
            similarityValue = weight;
            rulesAppliedLeft = new List<string>();
            rulesAppliedRight = new List<string>();
            substTable = new Dictionary<string, string>();
        }

        internal ProofResult(RipplingEntry entry, double weight, string prefix1, string prefix2)
        {
            if (weight == double.MinValue)
            {
                success = false;
                left = null;
                right = null;
                similarityValue = double.MinValue;
                rulesAppliedLeft = null;
                rulesAppliedRight = null;
                substTable = null;
                return;
            }
            if (entry == null)
                throw new ArgumentNullException("entry");
            success = true;
            if (String.IsNullOrEmpty(prefix1))
                prefix1 = " ";
            if (String.IsNullOrEmpty(prefix2))
                prefix2 = " ";

            left = entry.GivenRoot.ToCMathML().Replace(prefix1, String.Empty).Replace(prefix2, String.Empty);
            right = entry.GoalRoot.ToCMathML().Replace(prefix1, String.Empty).Replace(prefix2, String.Empty);
            similarityValue = weight;
            rulesAppliedLeft = new List<string>(entry.AppliedRulesToGiven);
            rulesAppliedRight = new List<string>(entry.AppliedRulesToGoal);
            substTable = new Dictionary<string, string>();
            foreach (KeyValuePair<Element, BinaryTree> pair in entry.RelatedUnificationEntry.Substitutions)
                substTable.Add(pair.Key.Name, pair.Value.ToCMathML().Replace(prefix1, String.Empty).Replace(prefix2, String.Empty));
            for (int i = 0; i < rulesAppliedLeft.Count; i++)
                rulesAppliedLeft[i] = rulesAppliedLeft[i].Replace(prefix1, String.Empty).Replace(prefix2, String.Empty);
            for (int i = 0; i < rulesAppliedRight.Count; i++)
                rulesAppliedRight[i] = rulesAppliedRight[i].Replace(prefix1, String.Empty).Replace(prefix2, String.Empty);
        }
    }


    class GenericComparer : IEqualityComparer<Element>
    {
        public bool Equals(Element x, Element y)
        {
            if (x == null && y == null)
                return true;
            else
                if (x == null || y == null)
                    return false;

            if (x.TypeOfElement == ElementType.Variable && y.TypeOfElement == ElementType.Variable)
                return true;
            else
            {
                if (x.Equals(y))
                    return true;
                else
                    return false;
            }
        }

        public int GetHashCode(Element obj)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");
            return obj.GetHashCode();
        }
    }

    class NaiveSimilarity : IEqualityComparer<Element>
    {
        int totalCount = 0;
        int matchCount = 0;
        int varInstancesCount = 0;
        public double Weight
        {
            get
            {
                return (double)matchCount / (double)(totalCount + varInstancesCount * 0.1);
            }
        }

        public bool Equals(Element x, Element y)
        {
            if (x == null && y == null)
                return true;
            else
                if (x == null || y == null)
                {
                    totalCount++;
                    return false;
                }

            if (x.TypeOfElement == ElementType.Variable && y.TypeOfElement == ElementType.Variable)
            {
                varInstancesCount++;
                return true;
            }
            else
            {
                if (x.Equals(y))
                {
                    totalCount += 2;
                    matchCount += 2;
                    return true;
                }
                else
                {
                    totalCount++;
                    return false;
                }
            }
        }

        public int GetHashCode(Element obj)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");
            return obj.GetHashCode();
        }
    }

    public enum StopConditions
    {
        LimitReached,
        UnificationFinishOrFirstResultOrLimitReached,
    }

    public enum ProofMode
    {
        Fast,
        Slow,
        Full
    }

    public static class Rippling
    {
        private static Log systemLog = new Log("Log_" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute + ".txt");
        public static Log SystemLog
        {
            get
            {
                return systemLog;
            }
        }
        private static List<WaveRule> rules;
        public static IReadOnlyList<WaveRule> Rules
        {
            get
            {
                return rules;
            }
        }

        public static int LoadRules(string rulesDirectory)
        {
            rules = new List<WaveRule>();
            int count = 0;
            foreach (string path in Directory.GetFiles(rulesDirectory))
            {
                WaveRule WR = new WaveRule(null, null, null, null, null);
                WaveRule.Load(path, ref WR);
                if (WR.LeftPart == null || WR.RightPart == null)
                    throw new FileLoadException(path);
                rules.Add(WR);
                count++;
            }
            return count;
        }

        public static double FuzzyJaccardSimilarity(BinaryTree tree1, BinaryTree tree2, double alpha, bool ordered)
        {
            List<BinaryTree> labels1 = new List<BinaryTree>(tree1.GetAllLeafNodes());
            List<BinaryTree> labels2 = new List<BinaryTree>(tree2.GetAllLeafNodes());
            List<KeyValuePair<BinaryTree, BinaryTree>> relatedLabels = new List<KeyValuePair<BinaryTree, BinaryTree>>();
            List<BinaryTree> labels2Taken = new List<BinaryTree>();
            foreach (BinaryTree t1 in labels1)
            {
                bool ist1Taken = false;
                foreach (BinaryTree t2 in labels2)
                {
                    if (t1.Data == t2.Data && labels2Taken.FindIndex(new Predicate<BinaryTree>(x => BinaryTree.ReferenceEquals(x, t2))) < 0)
                    {
                        relatedLabels.Add(new KeyValuePair<BinaryTree, BinaryTree>(t1, t2));
                        labels2Taken.Add(t2);
                        ist1Taken = true;
                        break;
                    }
                }
                if (!ist1Taken)
                    relatedLabels.Add(new KeyValuePair<BinaryTree, BinaryTree>(t1, null));
            }

            foreach (BinaryTree t2 in labels2)
                if (labels2Taken.FindIndex(new Predicate<BinaryTree>(x => BinaryTree.ReferenceEquals(x, t2))) < 0)
                    relatedLabels.Add(new KeyValuePair<BinaryTree, BinaryTree>(null, t2));

            //foreach (KeyValuePair<BinaryTree, BinaryTree> pair in relatedLabels)
            //{
            //    Console.Write("Key: ");
            //    if (pair.Key != null)
            //        Console.WriteLine(pair.Key.Data);
            //    else
            //        Console.WriteLine("NULL");

            //    Console.Write("Value: ");
            //    if (pair.Value != null)
            //        Console.WriteLine(pair.Value.Data);
            //    else
            //        Console.WriteLine("NULL");

            //}
            //Console.WriteLine("End of rel");

            Dictionary<BinaryTree, double> weightMatrix1 = new Dictionary<BinaryTree, double>();
            Dictionary<BinaryTree, double> weightMatrix2 = new Dictionary<BinaryTree, double>();
            foreach (BinaryTree t in tree1.GetAllNodes())
                weightMatrix1.Add(t, 0.0);
            foreach (BinaryTree t in tree2.GetAllNodes())
                weightMatrix2.Add(t, 0.0);

            int depth1 = fillMatrix(tree1, weightMatrix1);
            if (depth1 != tree1.GetTreeInfo().Depth - 1)
                throw new NotImplementedException("tree1 depth mismatch");
            int depth2 = fillMatrix(tree2, weightMatrix2);
            if (depth2 != tree2.GetTreeInfo().Depth - 1)
                throw new NotImplementedException("tree2 depth mismatch");

            double minValues = 0.0;
            double maxValues = 0.0;
            for (int i = 0; i < relatedLabels.Count; i++)
            {
                KeyValuePair<BinaryTree, BinaryTree> pair1 = relatedLabels[i];
                for (int j = 0; j < relatedLabels.Count; j++)
                {
                    KeyValuePair<BinaryTree, BinaryTree> pair2 = relatedLabels[j];
                    BinaryTree ancestor1 = getLeastCommonAncestor(pair1.Key, pair2.Key);
                    BinaryTree ancestor2 = getLeastCommonAncestor(pair1.Value, pair2.Value);
                    double k = 1.0;
                    if (ancestor1 != null && ancestor2 != null && ancestor1.Data != ancestor2.Data)
                    {
                        k = alpha;
                    }

                    double v1 = 0.0;
                    if (ancestor1 != null)
                    {
                        v1 = weightMatrix1[ancestor1];
                        if (ordered)
                            if ((ancestor1.Left == null && pair1.Key == null || ancestor1.Left != null && ancestor1.Left.GetAllLeafNodes().Contains(pair1.Key)) && (ancestor1.Right == null && pair2.Key == null || ancestor1.Right != null && ancestor1.Right.GetAllLeafNodes().Contains(pair2.Key)))
                                v1 = 1.0;
                    }

                    double v2 = 0.0;
                    if (ancestor2 != null)
                    {
                        v2 = weightMatrix2[ancestor2];
                        if (ordered)
                            if ((ancestor2.Left == null && pair1.Value == null || ancestor2.Left != null && ancestor2.Left.GetAllLeafNodes().Contains(pair1.Value)) && (ancestor2.Right == null && pair2.Value == null || ancestor2.Right != null && ancestor2.Right.GetAllLeafNodes().Contains(pair2.Value)))
                                v2 = 1.0;
                    }

                    if (v1 > v2)
                    {
                        maxValues += (v1 / k);
                        minValues += (v2 * k);
                    }
                    else
                    {
                        maxValues += (v2 / k);
                        minValues += (v1 * k);
                    }
                }
            }
            //Console.WriteLine("MivVal: " + minValues);
            //Console.WriteLine("MaxVal: " + maxValues);
            return minValues / maxValues;
        }

        internal static BinaryTree getLeastCommonAncestor(BinaryTree node1, BinaryTree node2)
        {
            if (object.ReferenceEquals(node1, node2))
                return node1;
            if (node1 == null || node2 == null)
                return null;
            HashSet<BinaryTree> ancestors1 = new HashSet<BinaryTree>();
            while (node1.Parent != null)
            {
                ancestors1.Add(node1.Parent);
                node1 = node1.Parent;
            }

            HashSet<BinaryTree> ancestors2 = new HashSet<BinaryTree>();
            while (node2.Parent != null)
            {
                ancestors2.Add(node2.Parent);
                node2 = node2.Parent;
            }

            ancestors1.IntersectWith(ancestors2);
            int max = ancestors1.Max(x => x.Depth);
            var result = ancestors1.Where(x => x.Depth == max);
            if (result.Count() > 1)
                throw new NotImplementedException("Something wrong with common ancestor");
            return result.First();
        }

        private static int fillMatrix(BinaryTree node, Dictionary<BinaryTree, double> weightMatrix)
        {
            if (node == null)
                return 0;
            int currentDepth = node.Depth;
            if (node.Left == null && node.Right == null)
            {
                weightMatrix[node] = 1.0;
                return node.Depth;
            }
            int maxDepth = Math.Max(fillMatrix(node.Left, weightMatrix), fillMatrix(node.Right, weightMatrix));
            weightMatrix[node] = currentDepth / (double)maxDepth;
            return maxDepth;
        }


        private static double performInclusionPreCheck(Expression expression1, Expression expression2)
        {
            int totalHits = 0;
            double weight = 0.0;
            if (expression1.InitialInformation.Depth >= expression2.InitialInformation.Depth)
            {
                foreach (var node in expression1.Root.GetAllNodes().Where(x => x.Depth == (expression1.InitialInformation.Depth - expression2.InitialInformation.Depth)))
                {
                    if (BinaryTree.CheckStructureWithCommutation(node, expression2.Root, new TillVariablesEqualityCmp()))
                    {
                        totalHits++;
                    }
                }
                weight = (expression2.InitialInformation.Total / (double)expression1.InitialInformation.Total);
            }
            else
                if (expression1.InitialInformation.Depth < expression2.InitialInformation.Depth)
                {
                    foreach (var node in expression2.Root.GetAllNodes().Where(x => x.Depth == (expression2.InitialInformation.Depth - expression1.InitialInformation.Depth)))
                    {
                        if (BinaryTree.CheckStructureWithCommutation(node, expression1.Root, new TillVariablesEqualityCmp()))
                        {
                            totalHits++;
                        }
                    }
                    weight = (expression1.InitialInformation.Total / (double)expression2.InitialInformation.Total);
                }
            //weight = weight * 1.0 / 3.0;
            if (totalHits > 0)
            {
                if (weight > 1)
                    Console.WriteLine("WEIGHT>1");
                for (int i = 0; i < totalHits; i++)
                    weight =weight+((1 - weight) * 0.05);
            }
            else
                weight = 0.0;
            return weight;
        }

        public static ProofResult PerformProof(Expression expression1, Expression expression2, double limit, ProofMode mode, StopConditions conditions)
        {
            if (expression1 == null)
                throw new ArgumentNullException("expression1");
            if (expression2 == null)
                throw new ArgumentNullException("expression2");
            if (Rippling.rules == null)
                throw new InvalidOperationException("No rules were loaded.");

            double preCheckWeight = performInclusionPreCheck(expression1, expression2);
            if (preCheckWeight > 0.0)
                return new ProofResult(expression1, expression2, preCheckWeight);

            RipplingController controller = new RipplingController(limit);
            switch (mode)
            {
                case ProofMode.Fast:
                    Unification.StartConveyor(expression1, expression2, controller, UnificationOption.CostAndSubstitutionMinimal, true, false);
                    break;
                case ProofMode.Slow:
                    Unification.StartConveyor(expression1, expression2, controller, UnificationOption.NoRestrictions, true, false);
                    break;
                case ProofMode.Full:
                    Unification.StartConveyor(expression1, expression2, controller, UnificationOption.NoRestrictions, false, false);
                    break;
            }
            //below unification is already finished
            switch (conditions)
            {
                case StopConditions.LimitReached:
                    controller.Finish(true);
                    break;
                case StopConditions.UnificationFinishOrFirstResultOrLimitReached:
                    controller.Finish(false);
                    break;
                default:
                    throw new NotImplementedException();
            }
            return new ProofResult(controller.MaxEntry, controller.CurrentMaxWeight, expression1.Prefix, expression2.Prefix);
        }


        public static ProofResult PerformProof(Expression expression1, Expression expression2, double limit, ProofMode mode, StopConditions conditions, TimeSpan timeLimit)
        {
            if (expression1 == null)
                throw new ArgumentNullException("expression1");
            if (expression2 == null)
                throw new ArgumentNullException("expression2");
            if (Rippling.rules == null)
                throw new InvalidOperationException("No rules were loaded.");

            double preCheckWeight = performInclusionPreCheck(expression1, expression2);
            if (preCheckWeight > 0.0)
                return new ProofResult(expression1, expression2, preCheckWeight);

            RipplingController controller = new RipplingController(limit);
            UnificationController unifController = null;
            switch (mode)
            {
                case ProofMode.Fast:
                    unifController = Unification.StartConveyor(expression1, expression2, controller, UnificationOption.CostAndSubstitutionMinimal, true, true);
                    break;
                case ProofMode.Slow:
                    unifController = Unification.StartConveyor(expression1, expression2, controller, UnificationOption.NoRestrictions, true, true);
                    break;
                case ProofMode.Full:
                    unifController = Unification.StartConveyor(expression1, expression2, controller, UnificationOption.NoRestrictions, false, true);
                    break;
            }
            //below unification is NOT already finished
            switch (conditions)
            {
                case StopConditions.LimitReached:
                    controller.Finish(true, unifController, timeLimit);
                    break;
                case StopConditions.UnificationFinishOrFirstResultOrLimitReached:
                    controller.Finish(false, unifController, timeLimit);
                    break;
                default:
                    throw new NotImplementedException();
            }
            return new ProofResult(controller.MaxEntry, controller.CurrentMaxWeight, expression1.Prefix, expression2.Prefix);
        }

        internal static bool TestRipplingStep(RipplingEntry entry)
        {
            if (entry == null)
                throw new ArgumentNullException("entry");
            RipplingEntry backupEntry = entry.DeepCopy();
            try
            {
                if (BinaryTree.CheckStructure(entry.GivenRoot, entry.GoalRoot, new NaiveSimilarity()))
                {
                    Console.WriteLine("Proof with " + (entry.AppliedRulesToGiven.Count + entry.AppliedRulesToGoal.Count) + " rules: ");
                    Console.WriteLine(entry.GivenRoot.ToString());
                    Console.WriteLine("--");
                    Console.WriteLine(entry.GoalRoot.ToString());
                    return true;
                }
                if (backupEntry.CommonMeasure == null || backupEntry.CommonMeasure.IsEnd)
                    return false;
            }
            catch (Exception)
            {
                Console.WriteLine("Catch: ");
                Console.WriteLine(backupEntry.GivenRoot.ToString());
                Console.WriteLine("And ");
                Console.WriteLine(backupEntry.GoalRoot.ToString());
                Console.WriteLine(entry.GivenSkeleton.ToString());
                Console.WriteLine(entry.GoalSkeleton.ToString());
                throw;
            }

            foreach (WaveRule rule in rules)
            {
                if (entry.ApplyRuleToGiven(rule))
                {
                    if (TestRipplingStep2(entry))
                        return true;
                    else
                        entry = backupEntry.DeepCopy();
                }

                if (entry.ApplyRuleToGoal(rule))
                {
                    if (TestRipplingStep2(entry))
                        return true;
                    else
                        entry = backupEntry.DeepCopy();
                }
            }
            return false;
        }

        private static bool TestRipplingStep2(RipplingEntry entry)
        {
            if (!entry.IsSkeletonDepthPreserving)
                return false;
            IReadOnlyCollection<RipplingEntry> possibleEntries = entry.GetSkeletonEnlargements();
            foreach (RipplingEntry e in possibleEntries)
                if (!(Measure.GetShrunk(entry, e) > entry.CommonMeasure) && TestRipplingStep(e))
                    return true;
            return false;
        }


        internal static void RipplingStep1(object obj)
        {
            RipplingEntry entry = obj as RipplingEntry;
            if (entry == null)
                throw new ArgumentException("entry");

            if (!entry.Controller.AddResult(entry))
                return;

            RipplingEntry backupEntry = entry.DeepCopy();
            foreach (WaveRule rule in rules)
            {
                if (entry.Controller.IsFinished)
                    return;
                if (entry.ApplyRuleToGiven(rule))
                {
                    RipplingStep2(entry);
                    if (!entry.Controller.IsFinished)
                        entry = backupEntry.DeepCopy();
                    else
                        return;
                }

                if (entry.ApplyRuleToGoal(rule))
                {
                    RipplingStep2(entry);
                    if (!entry.Controller.IsFinished)
                        entry = backupEntry.DeepCopy();
                    else
                        return;
                }
            }
        }

        [MethodImplAttribute(MethodImplOptions.AggressiveInlining)]
        private static void RipplingStep2(RipplingEntry entry)
        {
            if (!entry.IsSkeletonDepthPreserving)
                return;
            IReadOnlyCollection<RipplingEntry> possibleEntries = entry.GetSkeletonEnlargements();
            Measure m = entry.CommonMeasure;
            foreach (RipplingEntry e in possibleEntries)
                if (!(Measure.GetShrunk(entry, e) > m) && !e.Controller.IsFinished)
                    RipplingStep1(e);
        }


    }
}
