﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semawed
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1815:OverrideEqualsAndOperatorEqualsOnValueTypes"), Serializable]
    public class BinaryTreeInfo
    {
        private int skCount;
        private int wfCount;
        private int undCount;
        private int depth;
        public int SkeletonCount
        {
            get
            {
                return skCount;
            }
        }
        public int WFCount
        {
            get
            {
                return wfCount;
            }
        }
        public int UndefinedCount
        {
            get
            {
                return undCount;
            }
        }

        public int Depth
        {
            get
            {
                return depth;
            }
        }

        public int Total
        {
            get
            {
                return SkeletonCount + WFCount + UndefinedCount;
            }
        }

        private void DFS(BinaryTree target)
        {
            if (target == null)
                return;
            if (target.Data.Markup == MarkupType.Skeleton)
                skCount++;
            else
                if (target.Data.Markup == MarkupType.WaveFront)
                    wfCount++;
                else
                    undCount++;
            if (target.Depth > depth)
                depth = target.Depth;//adding this actual node
            DFS(target.Left);
            DFS(target.Right);
        }

        public BinaryTreeInfo(BinaryTree target)
        {
            if (target == null)
                throw new ArgumentNullException("target");
            skCount = 0;
            wfCount = 0;
            undCount = 0;
            depth = 0;
            DFS(target);
            depth++;
        }
    }

    [Serializable()]
    public class BinaryTree
    {
        private Element data;
        public Element Data
        {
            get
            {
                return data;
            }
        }
        public string qualifier { get; set; }

        private BinaryTree left;
        private BinaryTree right;
        private BinaryTree parent;
        private int depth;
        private bool virtualNodeForGuaranteedAdd = false;
        public BinaryTree Left
        {
            get
            {
                if (left != null)
                    left.Parent = this;
                return left;
            }
            set
            {
                left = value;
                if (left != null)
                {
                    left.Parent = this;
                }
            }
        }

        public BinaryTree Right
        {
            get
            {
                if (right != null)
                    right.Parent = this;
                return right;
            }
            set
            {
                right = value;
                if (right != null)
                {
                    right.Parent = this;
                }
            }
        }

        public BinaryTree Root
        {
            get
            {
                BinaryTree p = this;
                while (p.Parent != null)
                    p = p.Parent;
                return p;
            }
        }

        public BinaryTree Parent
        {
            get
            {
                return parent;
            }
            set
            {
                parent = value;
                depth = -1;
            }
        }

        public BinaryTree Sibling
        {
            get
            {
                if (Parent != null)
                    return IsLeftNode ? Parent.Right : Parent.Left;
                else
                    return null;
            }
        }

        /// <summary>
        /// Zero-based depth of the current node
        /// </summary>
        public int Depth
        {
            get
            {
                if (depth == -1)
                {
                    int res = 0;
                    BinaryTree t = this.Parent;
                    if (t != null)
                        res = t.Depth + 1;
                    return res;
                }
                else
                    return depth;
            }
        }

        /// <summary>
        /// Zero-based depth of the element in expression i.e. like if tree was n-ary. This propery does not cache it's value unlike Depth
        /// </summary>
        public int ActualDepth
        {
            get
            {
                int res = 0;
                BinaryTree last = this;
                BinaryTree t = this.Parent;
                while (t != null)
                {
                    if (!(last.IsVirtualNode && last.Data == t.Data))
                        res += 1;
                    last = t;
                    t = t.Parent;
                }

                return res;
            }
        }

        public bool IsLeftNode
        {
            get
            {
                if (Parent != null)
                    return Object.ReferenceEquals(Parent.Left, this);
                else
                    return false;
            }
        }

        public bool IsRightNode
        {
            get
            {
                if (Parent != null)
                    return Object.ReferenceEquals(Parent.Right, this);
                else
                    return false;
            }
        }

        /// <summary>
        /// Indicating whether the node was added for GuaranteedAdd() i.e. being an binarytree representation artefact
        /// </summary>
        internal bool IsVirtualNode
        {
            get
            {
                return virtualNodeForGuaranteedAdd;
            }
            set
            {
                virtualNodeForGuaranteedAdd = value;
            }
        }

        public BinaryTree(Element element)
        {
            if (element == null)
                throw new ArgumentNullException("element");
            data = element.Copy();
            left = null;
            right = null;
            parent = null;
            depth = -1;
            qualifier = "";
        }

        #region Overriden
        public override bool Equals(object obj)
        {
            BinaryTree t = obj as BinaryTree;
            if (t == null)
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            return checkStructure(this, t);
        }

        public static bool operator ==(BinaryTree obj1, BinaryTree obj2)
        {
            if ((object)obj1 == null && (object)obj2 == null)
                return true;
            else
                if ((object)obj1 != null && (object)obj2 != null)
                    return obj1.Equals(obj2);
                else
                    return false;
        }

        public static bool operator !=(BinaryTree obj1, BinaryTree obj2)
        {
            return !(obj1 == obj2);
        }

        private static bool checkStructure(BinaryTree t1, BinaryTree t2)
        {
            if (t1 == null && t2 == null)
                return true;
            if ((t1 == null || t2 == null) || (t1.Data != t2.Data))
                return false;
            return (checkStructure(t1.Left, t2.Left) && checkStructure(t1.Right, t2.Right));
        }

        public static bool CheckStructure(BinaryTree tree1, BinaryTree tree2, IEqualityComparer<Element> comparer)
        {
            if (comparer == null)
                throw new ArgumentNullException("comparer");
            if (tree1 == null && tree2 == null)
                return true;
            if ((tree1 == null || tree2 == null) || (!comparer.Equals(tree1.Data, tree2.Data)))
                return false;
            return (CheckStructure(tree1.Left, tree2.Left, comparer) && CheckStructure(tree1.Right, tree2.Right, comparer));
        }

        public static bool CheckStructureWithCommutation(BinaryTree tree1, BinaryTree tree2, IEqualityComparer<Element> comparer)
        {
            if (comparer == null)
                throw new ArgumentNullException("comparer");
            if (tree1 == null && tree2 == null)
                return true;
            if ((tree1 == null || tree2 == null) || (!comparer.Equals(tree1.Data, tree2.Data)))
                return false;
            if (tree1.Data.TypeOfElement == ElementType.Function && Function.IsCommutative(tree1.data))
                return (CheckStructure(tree1.Left, tree2.Left, comparer) && CheckStructure(tree1.Right, tree2.Right, comparer)) || (CheckStructure(tree1.Right, tree2.Left, comparer) && CheckStructure(tree1.Left, tree2.Right, comparer));
            else
                return (CheckStructure(tree1.Left, tree2.Left, comparer) && CheckStructure(tree1.Right, tree2.Right, comparer));
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "1")]
        public static bool CheckStructure(BinaryTree tree1, BinaryTree tree2, IEqualityComparer<Element> comparer, bool fullTraverse)
        {
            if (comparer == null)
                throw new ArgumentNullException("comparer");
            if (tree1 == null && tree2 == null)
                return true;
            if (!comparer.Equals(tree1 != null ? tree1.Data : null, tree2 != null ? tree2.Data : null))
            {
                if (fullTraverse)
                {
                    CheckStructure(tree1 != null ? tree1.Left : tree1, tree2 != null ? tree2.Left : tree2, comparer, fullTraverse);
                    CheckStructure(tree1 != null ? tree1.Right : tree1, tree2 != null ? tree2.Right : tree2, comparer, fullTraverse);
                }
                return false;
            }
            return (CheckStructure(tree1.Left, tree2.Left, comparer, fullTraverse) && CheckStructure(tree1.Right, tree2.Right, comparer, fullTraverse));
        }

        private void printToString(int offset, StringBuilder SB)
        {
            for (int i = 0; i < offset; i++)
                SB.Append(" ");
            if (virtualNodeForGuaranteedAdd)
                SB.AppendLine(Data.ToString() + " VIRTUAL");
            else
                SB.AppendLine(Data.ToString());
            if (Left != null)
                Left.printToString(offset + 1, SB);
            if (Right != null)
                Right.printToString(offset + 1, SB);
        }

        public override string ToString()
        {
            StringBuilder SB = new StringBuilder();
            SB.AppendLine(Data.ToString());
            if (Left != null)
                Left.printToString(1, SB);
            if (Right != null)
                Right.printToString(1, SB);
            return SB.ToString();
        }


        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public string ToPlainString()
        {
            string result = Data.Name;
            if (left != null && right != null)
                result = "(" + left.ToPlainString() + " " + result + " " + right.ToPlainString() + ")";
            else
                if (left != null && right == null)
                    result = "(" + result + " " + left.ToPlainString() + ")";
                else
                    if (left == null && right != null)
                        result = "(" + result + " " + right.ToPlainString() + ")";
            return result;
        }

        private string printCMathML(string prefix)
        {
            string res = "";
            if (!virtualNodeForGuaranteedAdd)
            {
                if (Left == null && Right == null)
                {
                    if (Data.TypeOfElement == ElementType.Variable && prefix != "")
                        res = Data.ToCMathMLNode().Replace(prefix, "");
                    else
                        res = Data.ToCMathMLNode();
                }
                else
                {
                    if (Left != null)
                        res += left.printCMathML(prefix);
                    if (Right != null)
                        res += right.printCMathML(prefix);
                    res = "<apply>" + Data.ToCMathMLNode() + res + "</apply>";
                }
            }
            else
            {
                if (Left != null)
                    res += left.printCMathML(prefix);
                if (Right != null)
                    res += right.printCMathML(prefix);
            }
            if (qualifier != "")
                res = "<" + qualifier + ">" + res + "</" + qualifier + ">";
            return res;
        }

        public string ToCMathML(string prefix)
        {
            return "<math>" + printCMathML(prefix) + "</math>";
        }

        public string ToCMathML()
        {
            return "<math>" + printCMathML("") + "</math>";
        }

        public static string SerializeToString(BinaryTree tree)
        {
            StringBuilder SB = new StringBuilder();
            SB.Append("%");
            stringSerialization(tree, SB);
            return SB.ToString();
        }

        public static BinaryTree DeserializeFromString(string serializedString)
        {
            char[] separators = { '%' };
            string[] split = serializedString.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            BinaryTree node = null;
            BinaryTree parent = null;
            int emptychild = 0;
            foreach (string s in split)
            {
                if (s != ".")
                {
                    Console.WriteLine(s);
                    Element e = Element.DeserializeFromString(s);
                    if (node == null)
                    {
                        node = new BinaryTree(e);
                        parent = node;
                    }
                    else
                    {
                        if (emptychild == 0)
                            node = node.GuaranteedAdd(e);
                        else
                            if (emptychild == 1)
                            {
                                node.Right = new BinaryTree(e);
                                node = node.Right;
                            }
                    }
                }
                else
                {
                    emptychild++;
                    if (emptychild == 2)
                    {
                        emptychild = 0;
                        if (node != null)
                        {
                            node = node.Parent;
                            while (node != null && node.Right != null)
                                node = node.Parent;
                        }
                    }
                }
            }
            return parent;
        }

        private static void stringSerialization(BinaryTree t, StringBuilder SB)
        {
            if (t != null)
            {
                SB.Append(Element.SerializeToString(t.Data) + "%");
                stringSerialization(t.Left, SB);
                stringSerialization(t.Right, SB);
            }
            else
                SB.Append(".%");
        }
        #endregion

        public int Add(Element element)
        {
            if (this.Data.TypeOfElement != ElementType.Function && this.Data.TypeOfElement != ElementType.ServiceObject)
                throw new InvalidOperationException("Node cannot have children");
            if (element == null)
                throw new ArgumentNullException("element");

            if (left == null)
            {
                Left = new BinaryTree(element);
                return -1;
            }
            else
                if (right == null)
                {
                    Right = new BinaryTree(element);
                    return 1;
                }
            return 0;
        }

        public BinaryTree GuaranteedAdd(Element element)//returns added node
        {
            if (this.Data.TypeOfElement != ElementType.Function && this.Data.TypeOfElement != ElementType.ServiceObject)
                throw new InvalidOperationException("Node cannot have children");
            if (element == null)
                throw new ArgumentNullException("element");

            if (left == null)
            {
                Left = new BinaryTree(element);
                return Left;
            }
            else
                if (right == null)
                {
                    Right = new BinaryTree(element);
                    return Right;
                }
                else
                {
                    BinaryTree rightBackup = right;
                    Right = new BinaryTree(Data);
                    Right.IsVirtualNode = true;
                    Right.Left = rightBackup;
                    Right.Right = new BinaryTree(element);
                    return this.right.right;
                }
        }

        #region Get... Methods
        public bool[] GetPathFromRoot()//true for right node
        {
            bool[] path = new bool[Depth];
            int i = Depth - 1;
            BinaryTree p = this;
            if (Parent == null)
                return path;
            do
            {
                path[i] = p.IsRightNode;
                p = p.Parent;
                i--;
            } while (p.Parent != null);
            return path;
        }

        public BinaryTree GetNodeUsingPath(ICollection<bool> path)
        {
            if (path == null)
                return null;
            BinaryTree node = this;
            try
            {
                foreach (bool step in path)
                {
                    if (step)
                        node = node.Right;
                    else
                        node = node.Left;
                }
            }
            catch (NullReferenceException)
            {
                throw new ArgumentException("Invalid path for the current tree", "path");
            }
            return node;
        }

        public HashSet<Element> GetAllElements()
        {
            HashSet<Element> result = new HashSet<Element>();
            result.Add(this.Data.Copy());
            if (Left != null)
                result.UnionWith(Left.GetAllElements());
            if (Right != null)
                result.UnionWith(Right.GetAllElements());
            return result;
        }

        public IReadOnlyList<BinaryTree> GetAllLeafNodes()
        {
            List<BinaryTree> result = new List<BinaryTree>();
            getLeafNodes(result);
            return result;
        }

        private void getLeafNodes(List<BinaryTree> result)
        {
            if (this.left == null && this.right == null)
                result.Add(this);
            else
            {
                if (Left != null)
                    Left.getLeafNodes(result);
                if (Right != null)
                    Right.getLeafNodes(result);
            }
        }

        public HashSet<Variable> GetAllVariables()
        {
            HashSet<Variable> result = new HashSet<Variable>();
            if (this.Data.TypeOfElement == ElementType.Variable)
                result.Add((Variable)this.Data.Copy());
            if (Left != null)
                result.UnionWith(Left.GetAllVariables());
            if (Right != null)
                result.UnionWith(Right.GetAllVariables());
            return result;
        }

        public IReadOnlyList<BinaryTree> GetAllNodes()
        {
            List<BinaryTree> result = new List<BinaryTree>();
            getAllNodes(result);
            return result;
        }

        private void getAllNodes(List<BinaryTree> result)
        {
            result.Add(this);
            if (Left != null)
                Left.getAllNodes(result);
            if (Right != null)
                Right.getAllNodes(result);

        }

        public BinaryTreeInfo GetTreeInfo()
        {
            BinaryTreeInfo res = new BinaryTreeInfo(this);
            return res;
        }
        #endregion

        public BinaryTree DeepCopy()
        {
            BinaryTree newTree = new BinaryTree(data.Copy());
            newTree.parent = parent;
            if (Left != null)
                newTree.Left = Left.DeepCopy();
            if (Right != null)
                newTree.Right = Right.DeepCopy();
            return newTree;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "0#")]
        public BinaryTree DeepCopyWholeTree(out BinaryTree rootCopy)//returns corresponding node of the copy
        {
            bool[] path = GetPathFromRoot();//false - for left, true - for right
            BinaryTree p = this;
            if (p.Parent == null)
            {
                rootCopy = p.DeepCopy();
                return rootCopy;
            }
            rootCopy = p.Root.DeepCopy();
            return rootCopy.GetNodeUsingPath(path);
        }

        public BinaryTree Find(Element obj)
        {
            BinaryTree res = null;
            if (Data == obj)
                res = this;
            else
            {
                if (Left != null)
                    res = Left.Find(obj);
                if (Right != null && res == null)
                    res = Right.Find(obj);
            }
            return res;
        }

        public IReadOnlyList<BinaryTree> FindAll(Element obj)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");
            List<BinaryTree> result = new List<BinaryTree>();
            findAll(obj, result);
            return result;
        }

        private void findAll(Element obj, List<BinaryTree> result)
        {
            if (Data == obj)
                result.Add(this);
            if (Left != null)
                Left.findAll(obj, result);
            if (Right != null)
                Right.findAll(obj, result);
        }

        public IReadOnlyList<BinaryTree> FindAll(Element obj, Predicate<BinaryTree> match)//binary tree for possible tree-specific conditions
        {
            if (obj == null)
                throw new ArgumentNullException("obj");
            if (match == null)
                throw new ArgumentNullException("match");
            List<BinaryTree> result = new List<BinaryTree>();
            findAll(obj, result, match);
            return result;
        }

        private void findAll(Element obj, List<BinaryTree> result, Predicate<BinaryTree> match)//binary tree for possible tree-specific conditions
        {
            if (Data == obj && match.Invoke(this))
                result.Add(this);
            if (Left != null)
                Left.findAll(obj, result, match);
            if (Right != null)
                Right.findAll(obj, result, match);
        }


        internal void ReplaceCurrentNodeWith(BinaryTree newNode)
        {
            if (newNode == null)
                return;
            this.data = newNode.Data.Copy();
            if (this.Left != null)
                this.Left.Parent = null;
            if (this.Right != null)
                this.Right.Parent = null;
            this.Left = null;
            this.Right = null;
            this.Left = newNode.Left;
            this.Right = newNode.Right;
        }

        internal void ReplaceSpecificNodes(Element target, BinaryTree newNode, bool keepMarkup)
        {
            if (target == null)
                throw new ArgumentNullException("target");
            if (target.TypeOfElement != ElementType.Variable)
                throw new NotImplementedException("Target element is not variable");
            if (newNode == null)
                return;
            IReadOnlyList<BinaryTree> result = FindAll(target);
            foreach (BinaryTree t in result)
            {
                BinaryTree substTree = newNode.DeepCopy();
                if (keepMarkup)
                    Unification.SetMarkup(substTree, t.Data.Markup);
                t.ReplaceCurrentNodeWith(substTree);
            }
        }
    }
}
