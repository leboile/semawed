﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

namespace Semawed
{
    public static class Unification
    {
        private static int varCount = 0;
        public static void SetMarkup(BinaryTree target, MarkupType desiredMarkup)
        {
            if (target == null)
                return;
            target.Data.Markup = desiredMarkup;
            SetMarkup(target.Left, desiredMarkup);
            SetMarkup(target.Right, desiredMarkup);
        }

        public static void SetMarkup(BinaryTree target, MarkupType desiredMarkup, MarkupType changeOnlyThisMarkup)
        {
            if (target == null)
                return;
            if (target.Data.Markup == changeOnlyThisMarkup)
                target.Data.Markup = desiredMarkup;
            SetMarkup(target.Left, desiredMarkup, changeOnlyThisMarkup);
            SetMarkup(target.Right, desiredMarkup, changeOnlyThisMarkup);
        }

        internal static UnificationController StartConveyor(Expression expression1, Expression expression2, RipplingController controller, UnificationOption option, bool forcedd, bool runAsync)
        {
            if (expression1 == null)
                throw new ArgumentNullException("expression1");
            if (expression2 == null)
                throw new ArgumentNullException("expression2");
            //cleaning up from all previous annotations
            SetMarkup(expression1.Root, MarkupType.Undefined, MarkupType.Skeleton);
            SetMarkup(expression1.Root, MarkupType.Undefined, MarkupType.WaveFront);
            SetMarkup(expression2.Root, MarkupType.Undefined, MarkupType.Skeleton);
            SetMarkup(expression2.Root, MarkupType.Undefined, MarkupType.WaveFront);
            UnificationEntry initialEntry = new UnificationEntry(expression1.Root.DeepCopy(), expression2.Root.DeepCopy());
            int max = (int)(expression1.InitialInformation.Depth > expression2.InitialInformation.Depth ? Math.Pow(2, expression1.InitialInformation.Depth) : Math.Pow(2, expression2.InitialInformation.Depth));
            if (max > 128)
                max = 128;
            UnificationController misc = new UnificationController(option, forcedd, max, true);
            misc.AddUnification(initialEntry);
            misc.NewUnificationEntryEvent += controller.NewUnificationEntryEventHandler;
            misc.UnificationFinishedEvent += controller.UnificationFinishedEventHandler;
            Task unifTask = new Task(delegate()
            {
                try
                {
                    DUNIFYPROCESS(initialEntry, ref misc, null);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("exception in DUNIFYPROCESS task " + ex.Message);
                }
                misc.FinishUnificationProcess();
            });
            if (runAsync)
                unifTask.Start();
            else
                unifTask.RunSynchronously();
            return misc;
        }


        public static IReadOnlyList<UnificationEntry> Annotate(Expression expression1, Expression expression2, UnificationOption minOption)
        {
            if (expression1 == null)
                throw new ArgumentNullException("expression1");
            if (expression2 == null)
                throw new ArgumentNullException("expression2");
            //cleaning up from all previous annotations
            SetMarkup(expression1.Root, MarkupType.Undefined, MarkupType.Skeleton);
            SetMarkup(expression1.Root, MarkupType.Undefined, MarkupType.WaveFront);
            SetMarkup(expression2.Root, MarkupType.Undefined, MarkupType.Skeleton);
            SetMarkup(expression2.Root, MarkupType.Undefined, MarkupType.WaveFront);
            UnificationEntry initialEntry = new UnificationEntry(expression1.Root.DeepCopy(), expression2.Root.DeepCopy());
            int max = (int)(expression1.InitialInformation.Depth > expression2.InitialInformation.Depth ? Math.Pow(2, expression1.InitialInformation.Depth) : Math.Pow(2, expression2.InitialInformation.Depth));
            if (max > 128)
                max = 128;
            UnificationController misc = new UnificationController(minOption, false, max, false);
            misc.AddUnification(initialEntry);
            DUNIFYPROCESS(initialEntry, ref misc, null);
            misc.FinishUnificationProcess();
            List<UnificationEntry> results = misc.GetResults();
            for (int i = 0; i < results.Count; i++)
            {
                if (!results[i].CompleteUnification())
                {
                    results.RemoveAt(i);
                    i--;
                    Rippling.SystemLog.Add("DUNIFY produced non-complete unification!");
                    continue;
                }
                if (minOption != UnificationOption.NoRestrictions)//cleaningup
                {
                    if (minOption != UnificationOption.SubstitutionMinimal && results[i].WRCost > misc.MinimalUnificationCost)
                    {
                        results.RemoveAt(i);
                        i--;
                    }
                    else
                        if (minOption != UnificationOption.CostMinimal && results[i].SubstitutionCount > misc.MinimalUnificationSubstCount)
                        {
                            results.RemoveAt(i);
                            i--;
                        }
                }
            }
            return results;
        }

        [MethodImplAttribute(MethodImplOptions.AggressiveInlining)]
        private static void logDUNIFYFail(string description, UnificationEntry entry)
        {
            Rippling.SystemLog.Add("Dunify fail!");
            Rippling.SystemLog.Add(description);
            Rippling.SystemLog.Add("givenRoot: ");
            Rippling.SystemLog.Add(entry.GivenRoot.ToString());
            Rippling.SystemLog.Add("given: ");
            Rippling.SystemLog.Add(entry.GivenNode.ToString());
            Rippling.SystemLog.Add("goalRoot: ");
            Rippling.SystemLog.Add(entry.GoalRoot.ToString());
            Rippling.SystemLog.Add("goal: ");
            Rippling.SystemLog.Add(entry.GoalNode.ToString());
        }

        [MethodImplAttribute(MethodImplOptions.AggressiveInlining)]
        private static void DUNIFYPATHS(UnificationEntry entry, UnificationEntry backupEntry, ref UnificationController misc, List<bool[][]> pathsToUnify)
        {
            List<bool[][]> newPaths = new List<bool[][]>(pathsToUnify);
            foreach (bool[][] path in pathsToUnify)
            {
                newPaths.Remove(path);
                try
                {
                    DUNIFYPROCESS(entry.MoveByPath(path), ref misc, new List<bool[][]>(newPaths));
                }
                catch (NotImplementedException)
                {
                    logDUNIFYFail("DUNIFYPATHS messed up " + true, backupEntry);
                    throw;
                }
            }
        }

        [MethodImplAttribute(MethodImplOptions.AggressiveInlining)]
        private static void DUNIFYSIBLINGS(UnificationEntry newEntry, UnificationEntry backupEntry, ref UnificationController misc, List<bool[][]> pathsToUnify, bool makeCopy, string Descr)
        {
            List<bool[][]> pathsIncreased = new List<bool[][]>(pathsToUnify != null ? pathsToUnify : new List<bool[][]>(1));
            bool[][] newpath = new bool[2][];
            newpath[0] = newEntry.GivenNode.Right == null ? null : newEntry.GivenNode.Right.GetPathFromRoot();
            newpath[1] = newEntry.GoalNode.Right == null ? null : newEntry.GoalNode.Right.GetPathFromRoot();
            pathsIncreased.Add(newpath);
            BinaryTree newGiven = newEntry.GivenNode;
            BinaryTree newGoal = newEntry.GoalNode;
            try
            {
                newEntry.GoalNode = newGoal.Left;
                newEntry.GivenNode = newGiven.Left;
                DUNIFYPROCESS(newEntry, ref misc, pathsIncreased);
            }
            catch (NotImplementedException)
            {
                logDUNIFYFail(Descr + " ON LEFT messed up " + makeCopy, backupEntry);
                throw;
            }

            try
            {
                newEntry.GoalNode = newGoal.Right;
                newEntry.GivenNode = newGiven.Right;
                DUNIFYPROCESS(newEntry, ref misc, pathsToUnify);
            }
            catch (NotImplementedException)
            {
                logDUNIFYFail(Descr + " ON Right messed up " + makeCopy, backupEntry);
                throw;
            }
        }


        [MethodImplAttribute(MethodImplOptions.AggressiveInlining)]
        private static UnificationEntry DUNIFYENTRY(UnificationEntry entry, UnificationEntry backupEntry, UnificationController misc, List<UnificationEntry> newEntries, bool makeCopy)
        {
            UnificationEntry newEntry;
            if (makeCopy)
            {
                newEntry = new UnificationEntry(backupEntry);
                misc.AddUnification(newEntry);
                newEntries.Add(newEntry);
            }
            else
                newEntry = entry;
            return newEntry;
        }

        [MethodImplAttribute(MethodImplOptions.AggressiveInlining)]
        private static void DUNIFYELIMINATE(UnificationEntry newEntry, UnificationEntry backupEntry, bool isLeft)
        {
            if (isLeft)
            {
                newEntry.GivenRoot.ReplaceSpecificNodes(backupEntry.GivenNode.Data, newEntry.GoalNode, true);
                newEntry.GoalRoot.ReplaceSpecificNodes(backupEntry.GivenNode.Data, newEntry.GoalNode, true);
                newEntry.IncreaseSubstCount(backupEntry.GivenNode.Data, newEntry.GoalNode);
            }
            else
            {
                newEntry.GivenRoot.ReplaceSpecificNodes(backupEntry.GoalNode.Data, newEntry.GivenNode, true);
                newEntry.GoalRoot.ReplaceSpecificNodes(backupEntry.GoalNode.Data, newEntry.GivenNode, true);
                newEntry.IncreaseSubstCount(backupEntry.GoalNode.Data, newEntry.GivenNode);
            }

        }

        [MethodImplAttribute(MethodImplOptions.AggressiveInlining)]
        private static void DUNIFYIMITATE(UnificationEntry newEntry, UnificationEntry backupEntry, bool isLeft)
        {
            if (isLeft)
            {
                BinaryTree treeSubst = new BinaryTree(newEntry.GoalNode.Data);
                if (newEntry.GoalNode.Left != null)
                    treeSubst.Add(new Variable("newvar" + (++varCount).ToString()));
                if (newEntry.GoalNode.Right != null)
                    treeSubst.Add(new Variable("newvar" + (++varCount).ToString()));
                newEntry.GivenRoot.ReplaceSpecificNodes(backupEntry.GivenNode.Data, treeSubst, true);
                newEntry.GoalRoot.ReplaceSpecificNodes(backupEntry.GivenNode.Data, treeSubst, true);
                newEntry.IncreaseSubstCount(backupEntry.GivenNode.Data, treeSubst);
            }
            else
            {
                BinaryTree targetSubst = new BinaryTree(newEntry.GivenNode.Data);
                if (newEntry.GivenNode.Left != null)
                    targetSubst.Add(new Variable("newvar" + (++varCount).ToString()));
                if (newEntry.GivenNode.Right != null)
                    targetSubst.Add(new Variable("newvar" + (++varCount).ToString()));
                newEntry.GivenRoot.ReplaceSpecificNodes(backupEntry.GoalNode.Data, targetSubst, true);
                newEntry.GoalRoot.ReplaceSpecificNodes(backupEntry.GoalNode.Data, targetSubst, true);
                newEntry.IncreaseSubstCount(backupEntry.GoalNode.Data, targetSubst);
            }
        }

        private static void DUNIFYPROCESS(UnificationEntry entry, ref UnificationController misc, List<bool[][]> pathsToUnify)
        {
            if (entry.GivenNode != null && entry.GivenNode.Data.Markup != MarkupType.Undefined || entry.GoalNode != null && entry.GoalNode.Data.Markup != MarkupType.Undefined)
            {
                logDUNIFYFail("markup messed up", entry);
                throw new NotImplementedException("Something went wrong");
            }

            if (misc.IsUnificationFinished)
                return;

            if ((entry.SubstitutionCount + entry.WRCost) > misc.MaxAmountOfAppliedRules
                || misc.MinimalUnificationComplete && ((misc.SubstitutionCountMinimal && entry.SubstitutionCount > misc.MinimalUnificationSubstCount) || (misc.CostMinimal && entry.WRCost > misc.MinimalUnificationCost)))
            {
                misc.RemoveUnification(entry);
                return;
            }

            if (pathsToUnify != null && pathsToUnify.Count == 0)
                pathsToUnify = null;

            if (entry.GivenNode == null && entry.GoalNode != null)
            {
                SetMarkup(entry.GoalNode, MarkupType.WaveFront);
                return;
            }
            else
                if (entry.GivenNode != null && entry.GoalNode == null)
                {
                    SetMarkup(entry.GivenNode, MarkupType.WaveFront);
                    return;
                }
                else
                    if (entry.GivenNode == null && entry.GoalNode == null)
                        return;


            bool makeCopy = false;
            List<UnificationEntry> newEntries = new List<UnificationEntry>();
            UnificationEntry backupEntry = new UnificationEntry(entry);

            //DECOMPOSE/DELETE
            if (backupEntry.GivenNode.Data == backupEntry.GoalNode.Data)
            {
                UnificationEntry newEntry = entry;
                newEntry.IncreaseSimilarCount();

                DUNIFYSIBLINGS(newEntry, backupEntry, ref misc, pathsToUnify, makeCopy, "DECOMPOSE/DELETE");

                makeCopy = true;
                if (misc.ForcedDECOMPOSEDELETE)
                    return;
            }

            #region ELIMINATE
            //ELIMINATEL
            if (backupEntry.GivenNode.Data.TypeOfElement == ElementType.Variable && backupEntry.GoalNode.Find(backupEntry.GivenNode.Data) == null)
            {
                UnificationEntry newEntry = DUNIFYENTRY(entry, backupEntry, misc, newEntries, makeCopy);
                DUNIFYELIMINATE(newEntry, backupEntry, true);
                if (makeCopy && pathsToUnify != null)
                    DUNIFYPATHS(newEntry, backupEntry, ref misc, pathsToUnify);
                makeCopy = true;
            }

            //ELIMINATER
            if (backupEntry.GoalNode.Data.TypeOfElement == ElementType.Variable && backupEntry.GivenNode.Find(backupEntry.GoalNode.Data) == null)
            {
                UnificationEntry newEntry = DUNIFYENTRY(entry, backupEntry, misc, newEntries, makeCopy);
                DUNIFYELIMINATE(newEntry, backupEntry, false);
                if (makeCopy && pathsToUnify != null)
                    DUNIFYPATHS(newEntry, backupEntry, ref misc, pathsToUnify);
                makeCopy = true;
            }
            #endregion
            #region IMITATE
            //IMITATEL
            if (backupEntry.GivenNode.Data.TypeOfElement == ElementType.Variable && backupEntry.GoalNode.Data.TypeOfElement == ElementType.Function && backupEntry.GoalNode.Find(backupEntry.GivenNode.Data) == null)
            {
                UnificationEntry newEntry = DUNIFYENTRY(entry, backupEntry, misc, newEntries, makeCopy);
                DUNIFYIMITATE(newEntry, backupEntry, true);
                DUNIFYSIBLINGS(newEntry, backupEntry, ref misc, pathsToUnify, makeCopy, "IMITATEL");
                if (makeCopy && pathsToUnify != null)
                    DUNIFYPATHS(newEntry, backupEntry, ref misc, pathsToUnify);
                makeCopy = true;
            }

            //IMITATER
            if (backupEntry.GivenNode.Data.TypeOfElement == ElementType.Function && backupEntry.GoalNode.Data.TypeOfElement == ElementType.Variable && backupEntry.GivenNode.Find(backupEntry.GoalNode.Data) == null)
            {
                UnificationEntry newEntry = DUNIFYENTRY(entry, backupEntry, misc, newEntries, makeCopy);
                DUNIFYIMITATE(newEntry, backupEntry, false);
                DUNIFYSIBLINGS(newEntry, backupEntry, ref misc, pathsToUnify, makeCopy, "IMITATER");
                if (makeCopy && pathsToUnify != null)
                    DUNIFYPATHS(newEntry, backupEntry, ref misc, pathsToUnify);
                makeCopy = true;
            }
            #endregion
            #region HIDE
            //HIDELFORL
            if (backupEntry.GoalNode.Data.TypeOfElement == ElementType.Function && backupEntry.GoalNode.Left != null)// && backupEntry.GivenNode.Data.type!=ElementType.Variable)
            {
                UnificationEntry newEntry = DUNIFYENTRY(entry, backupEntry, misc, newEntries, makeCopy);

                newEntry.GoalNode.Data.Markup = MarkupType.WaveFront;
                SetMarkup(newEntry.GoalNode.Left, MarkupType.WaveFront);
                newEntry.IncreaseCost();

                try
                {
                    newEntry.GoalNode = newEntry.GoalNode.Right;
                    DUNIFYPROCESS(newEntry, ref misc, pathsToUnify);
                }
                catch (NotImplementedException)
                {
                    logDUNIFYFail("HIDELL DUNIFY(newTree, newTarget.Right) messed up" + makeCopy, backupEntry);
                    throw;
                }

                if (makeCopy && pathsToUnify != null)
                    DUNIFYPATHS(newEntry, backupEntry, ref misc, pathsToUnify);
                makeCopy = true;
            }

            //HIDELFORR
            if (backupEntry.GoalNode.Data.TypeOfElement == ElementType.Function && backupEntry.GoalNode.Right != null)// && backupEntry.GivenNode.Data.type != ElementType.Variable)
            {
                UnificationEntry newEntry = DUNIFYENTRY(entry, backupEntry, misc, newEntries, makeCopy);

                newEntry.GoalNode.Data.Markup = MarkupType.WaveFront;
                SetMarkup(newEntry.GoalNode.Right, MarkupType.WaveFront);
                newEntry.IncreaseCost();

                try
                {
                    newEntry.GoalNode = newEntry.GoalNode.Left;
                    DUNIFYPROCESS(newEntry, ref misc, pathsToUnify);
                }
                catch (NotImplementedException)
                {
                    logDUNIFYFail("HIDELR DUNIFY(newTree, newTarget.Left) messed up" + makeCopy, backupEntry);
                    throw;
                }

                if (makeCopy && pathsToUnify != null)
                    DUNIFYPATHS(newEntry, backupEntry, ref misc, pathsToUnify);
                makeCopy = true;
            }

            //HIDERFORL
            if (backupEntry.GivenNode.Data.TypeOfElement == ElementType.Function && backupEntry.GivenNode.Left != null)// && backupEntry.GoalNode.Data.type!=ElementType.Variable)
            {
                UnificationEntry newEntry = DUNIFYENTRY(entry, backupEntry, misc, newEntries, makeCopy);

                newEntry.GivenNode.Data.Markup = MarkupType.WaveFront;
                SetMarkup(newEntry.GivenNode.Left, MarkupType.WaveFront);
                newEntry.IncreaseCost();

                try
                {
                    newEntry.GivenNode = newEntry.GivenNode.Right;
                    DUNIFYPROCESS(newEntry, ref misc, pathsToUnify);
                }
                catch (NotImplementedException)
                {
                    logDUNIFYFail("HIDERL DUNIFY(newTree.Right, newTarget) messed up" + makeCopy, backupEntry);
                    throw;
                }

                if (makeCopy && pathsToUnify != null)
                    DUNIFYPATHS(newEntry, backupEntry, ref misc, pathsToUnify);
                makeCopy = true;
            }

            //HIDERFORR
            if (backupEntry.GivenNode.Data.TypeOfElement == ElementType.Function && backupEntry.GivenNode.Right != null)// && backupEntry.GoalNode.Data.type != ElementType.Variable)
            {
                UnificationEntry newEntry = DUNIFYENTRY(entry, backupEntry, misc, newEntries, makeCopy);

                newEntry.GivenNode.Data.Markup = MarkupType.WaveFront;
                SetMarkup(newEntry.GivenNode.Right, MarkupType.WaveFront);
                newEntry.IncreaseCost();

                try
                {
                    newEntry.GivenNode = newEntry.GivenNode.Left;
                    DUNIFYPROCESS(newEntry, ref misc, pathsToUnify);
                }
                catch (NotImplementedException)
                {
                    logDUNIFYFail("HIDERR DUNIFY(newTree.Left, newTarget) messed up" + makeCopy, backupEntry);
                    throw;
                }
                if (makeCopy && pathsToUnify != null)
                    DUNIFYPATHS(newEntry, backupEntry, ref misc, pathsToUnify);
                makeCopy = true;
            }
            #endregion

            if (!makeCopy)
            {
                if (entry.GivenNode.Data.TypeOfElement == ElementType.Constant && entry.GoalNode.Data.TypeOfElement == ElementType.Constant)
                {
                    misc.RemoveUnification(entry);
                    return;
                }
                else
                {
                    logDUNIFYFail("NO RULES APPLIED " + backupEntry.GivenNode.Data.TypeOfElement + " " + backupEntry.GoalNode.Data.TypeOfElement, backupEntry);
                    throw new InvalidOperationException("DUNIFY Gone bad " + backupEntry.GivenNode.Data + " " + backupEntry.GoalNode.Data);
                }
            }
            //unification ended
            if (misc.CostMinimal || misc.SubstitutionCountMinimal || misc.ProceedWithRippling)
            {
                foreach (UnificationEntry e in newEntries)
                    if (e.CompleteUnification())
                    {
                        bool suitableToLaunch = false;
                        if (!misc.MinimalUnificationComplete)
                        {
                            misc.MinimalUnificationComplete = true;
                            suitableToLaunch = true;
                        }

                        if (misc.CostMinimal && misc.SubstitutionCountMinimal)
                        {
                            if (e.WRCost <= misc.MinimalUnificationCost && e.SubstitutionCount <= misc.MinimalUnificationSubstCount)
                            {
                                misc.MinimalUnificationCost = e.WRCost;
                                misc.MinimalUnificationSubstCount = e.SubstitutionCount;
                                suitableToLaunch = true;
                            }
                        }
                        else
                        {
                            if (misc.CostMinimal || misc.SubstitutionCountMinimal)
                            {
                                if (misc.CostMinimal && e.WRCost <= misc.MinimalUnificationCost)
                                {
                                    misc.MinimalUnificationCost = e.WRCost;
                                    suitableToLaunch = true;
                                }
                                if (misc.SubstitutionCountMinimal && e.SubstitutionCount <= misc.MinimalUnificationSubstCount)
                                {
                                    misc.MinimalUnificationSubstCount = e.SubstitutionCount;
                                    suitableToLaunch = true;
                                }
                            }
                            else
                                suitableToLaunch = true;
                        }
                        if (misc.ProceedWithRippling && suitableToLaunch)
                            misc.StartRippling(e);
                    }
            }
        }
    }
}
