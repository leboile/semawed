﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semawed
{
    internal class Measure
    {
        private bool anySkeleton = false;
        private int[] m;
        private int count;
        public int Count
        {
            get
            {
                return count;
            }
        }

        private int buildMeasureArray(BinaryTree tree, List<int[]> measureArray, int currentDepth, BinaryTree lastSkeletonParent)
        {
            if (tree == null)
                return 0;
            int WFSize = 0;
            int depth = currentDepth;
            if (tree.Data.Markup == MarkupType.Undefined)
                throw new ArgumentException("BinaryTree is not marked properly", "tree");
            if (tree.Data.Markup == MarkupType.WaveFront)
            {
                WFSize++;
                currentDepth--;
            }
            else
            {
                if (lastSkeletonParent != null && tree.IsVirtualNode && tree.Data == lastSkeletonParent.Data)//to deal with binary tree artefacts
                    currentDepth--;
                anySkeleton = true;
                lastSkeletonParent = tree;
            }

            WFSize += buildMeasureArray(tree.Left, measureArray, currentDepth + 1, lastSkeletonParent) + buildMeasureArray(tree.Right, measureArray, currentDepth + 1, lastSkeletonParent);

            if (tree.Parent != null && tree.Parent.Data.Markup == MarkupType.WaveFront)
                return WFSize;
            else
            {
                int[] entry = new int[2];
                entry[0] = WFSize;
                entry[1] = depth;
                measureArray.Add(entry);
                if (tree.Parent == null && !anySkeleton && measureArray.Count == 1)
                    return -1;
                else
                    return 0;
            }
        }

        #region anotherNonWorkingAlgorithm
        //private void buildSkeletonMap(BinaryTree tree, HashSet<int> map, int currentDepth)
        //{
        //    if (tree == null)
        //        return;
        //    if (tree.Data.Markup == MarkupType.Skeleton)
        //        map.Add(currentDepth);
        //    buildSkeletonMap(tree.Left, map, currentDepth + 1);
        //    buildSkeletonMap(tree.Right, map, currentDepth + 1);
        //}

        //private void buildMeasureArray(BinaryTree tree, int[] measureArray, List<int> skMap)
        //{
        //    if (tree == null)
        //        return;
        //    if (tree.Data.Markup == MarkupType.Undefined)
        //        throw new ArgumentException("BinaryTree is not marked properly", "tree");
        //    if (tree.Data.Markup == MarkupType.WaveFront)
        //    {
        //        int depth = tree.Depth;
        //        int index = count-1;
        //        for(int i=0; i<skMap.Count; i++)
        //            if (skMap[i] >= depth)
        //            {
        //                if (i != 0)
        //                    index = count-i-1;
        //                break;
        //            }
        //        measureArray[index]++;
        //    }
        //    buildMeasureArray(tree.Left, measureArray, skMap); 
        //    buildMeasureArray(tree.Right, measureArray, skMap);
        //}

        //public Measure(BinaryTree tree)
        //{
        //    if (tree == null)
        //        throw new ArgumentNullException("tree");
        //    if (tree.Parent != null)
        //        throw new ArgumentException("Node must be the root of the tree.", "tree");
        //    HashSet<int> skMap = new HashSet<int>();
        //    buildSkeletonMap(tree, skMap, 1);
        //    if (skMap.Count == 0)
        //        throw new ArgumentException("Tree doesn't contain skeleton", "tree");
        //    count = skMap.Count;
        //    m = new int[count];
        //    List<int> skList = skMap.OrderBy(x => x).ToList();
        //    foreach (int i in skList)
        //        Console.WriteLine(i);
        //    buildMeasureArray(tree, m, skList);
        //}
        #endregion

        public Measure(BinaryTree tree)
        {
            if (tree == null)
                throw new ArgumentNullException("tree");
            if (tree.Parent != null)
                throw new ArgumentException("Node must be the root of the tree.", "tree");
            List<int[]> measureArray = new List<int[]>();
            int res = buildMeasureArray(tree, measureArray, 0, null);
            if (res == -1)
            {
                m = new int[1];
                m[0]=tree.GetTreeInfo().Total;
                return;
                throw new InvalidOperationException("Tree doesn't have any skeleton nodes");
            }
            List<int> nextArr = new List<int>();
            for (int i = 0; i < tree.GetTreeInfo().Depth; i++)
            {
                int val = 0;
                bool toInsert = false;
                foreach (int[] entry in measureArray)
                    if (entry[1] == i)
                    {
                        val += entry[0];
                        toInsert = true;
                    }
                if (toInsert)
                    nextArr.Add(val);
            }

            count = nextArr.Count;
            m = new int[count];
            for (int i = nextArr.Count - 1; i >= 0; i--)
                m[nextArr.Count - i - 1] = nextArr[i];
        }


        private Measure(int[] measure)//deprecated
        {
            count = measure.Count();
            m = measure;
        }

        public int WavefrontCount
        {
            get
            {
                int sum = 0;
                for (int i = 0; i < count; i++)
                    sum += m[i];
                return sum;
            }
        }

        public bool IsEnd
        {
            get
            {
                for (int i = 0; i < count - 1; i++)
                    if (m[i] > 0)
                        return false;
                return true;
            }
        }

        public int LastNumber
        {
            get
            {
                return m[count - 1];
            }
        }

        public static Measure operator +(Measure measure1, Measure measure2)
        {
            if (measure1 == null)
                throw new ArgumentNullException("measure1");
            if (measure2 == null)
                throw new ArgumentNullException("measure2");
            if (measure1.count != measure2.count)
                throw new InvalidOperationException("Measures cannot be summed due to different length " + measure1.ToString() + " and " + measure2.ToString());//or return null??
            int[] newMeasure = new int[measure1.count];
            for (int i = 0; i < measure1.count; i++)
                newMeasure[i] = measure1.m[i] + measure2.m[i];
            return new Measure(newMeasure);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            Measure arg = obj as Measure;
            if (arg == null)
                return false;
            return this == arg;
        }

        public static bool operator ==(Measure measure1, Measure measure2)
        {
            if ((object)measure1 == null && (object)measure2 == null)
                return true;
            if ((object)measure1 == null || (object)measure2 == null || measure1.count != measure2.count)
                return false;
            else
                for (int i = 0; i < measure1.count; i++)
                    if (measure1.m[i] != measure2.m[i])
                        return false;
            return true;
        }

        public static bool operator !=(Measure measure1, Measure measure2)
        {
            return !(measure1 == measure2);
        }

        public static bool operator >(Measure measure1, Measure measure2)
        {
            if (measure1 == null)
                throw new ArgumentNullException("measure1");
            if (measure2 == null)
                throw new ArgumentNullException("measure2");
            if (measure1.count != measure2.count)
                throw new InvalidOperationException("Measure cannot be compared due to difference in length " + measure1 + " and " + measure2);
            for (int i = 0; i < measure1.count; i++)
                if (measure1.m[i] > measure2.m[i])
                    return true;
                else
                    if (measure1.m[i] < measure2.m[i])
                        return false;
            return false;
        }

        public static bool operator <(Measure measure1, Measure measure2)
        {
            if (measure1 == null)
                throw new ArgumentNullException("measure1");
            if (measure2 == null)
                throw new ArgumentNullException("measure2");
            if (measure1.count != measure2.count)
                throw new InvalidOperationException("Measure cannot be compared due to difference in length " + measure1 + " and " + measure2);
            for (int i = 0; i < measure1.count; i++)
                if (measure1.m[i] < measure2.m[i])
                    return true;
                else
                    if (measure1.m[i] > measure2.m[i])
                        return false;
            return false;
        }

        public override string ToString()
        {
            if (m.Length == 0)
                return "Measure array is empty";
            string res = "";
            for (int i = 0; i < count; i++)
                res += m[i] + " ";
            return res;
        }

        public static Measure GetShrunk(BinaryTree oldTree, BinaryTree newTree, Skeleton oldSkeleton, Skeleton newSkeleton)
        {
            if (oldTree == null)
                throw new ArgumentNullException("oldTree");
            if (newTree == null)
                throw new ArgumentNullException("newTree");
            if (oldSkeleton == null)
                throw new ArgumentNullException("oldSkeleton");
            if (newSkeleton == null)
                throw new ArgumentNullException("newSkeleton");
            Measure newMeasure = new Measure(newTree);
            Measure oldMeasure = new Measure(oldTree);
            int d = newMeasure.Count - oldMeasure.Count;
            if (d == 0)
                return newMeasure;
            if (d < 0)
                return GetShrunk(newTree, oldTree, newSkeleton, oldSkeleton);
            List<int> newM = newMeasure.m.ToList();
            if (newMeasure.IsEnd)
            {
                newM.RemoveRange(0, d);
                return new Measure(newM.ToArray());
            }

            HashSet<int> uncommonNodesDepth = new HashSet<int>(newSkeleton.ToList().Select(x => x.ActualDepth));
            foreach (int depth in from s in oldSkeleton.ToList() select s.ActualDepth)
                uncommonNodesDepth.Remove(depth);

            List<int> indexesToRemove = uncommonNodesDepth.ToList();
            indexesToRemove.Sort((x, y) => x > y ? -1 : 1);

            foreach (int index in indexesToRemove)
            {
                if (index > 0)
                {
                    newM[index - 1] += newM[index];
                    newM.RemoveAt(index);
                }
                else
                    if (index == 0)
                    {
                        newM[0] += newM[1];
                        newM.RemoveAt(1);
                    }
            }

            if (newM.Count == oldMeasure.Count)//success
                return new Measure(newM.ToArray());
            else
            {
                StringBuilder ErrorString = new StringBuilder();
                ErrorString.AppendLine("old skeleton: ");
                ErrorString.AppendLine(oldSkeleton.ToString());
                ////Console.WriteLine("list: ");
                ////foreach (BinaryTree n in oldSkeleton.ToList())
                ////    Console.WriteLine(n.Data);
                ErrorString.AppendLine("new skeleton: ");
                ErrorString.AppendLine(newSkeleton.ToString());
                ////Console.WriteLine("list: ");
                ////foreach (BinaryTree n in newSkeleton.ToList())
                //    //Console.WriteLine(n.Data);
                //if (indexesToRemove.Count == 0)
                //    Console.WriteLine("indexesToRemove EMPTY");
                //foreach (int i in indexesToRemove)
                //    Console.WriteLine("i: " + i);
                ErrorString.AppendLine("Shrunk fail: ");
                ErrorString.AppendLine(oldMeasure.ToString());
                ErrorString.AppendLine(oldTree.ToString());
                ErrorString.AppendLine(newMeasure.ToString());
                ErrorString.AppendLine(newTree.ToString());
                ErrorString.AppendLine("newM" + new Measure(newM.ToArray()).ToString());
                Rippling.SystemLog.Add(ErrorString);
                //throw new NotImplementedException("Shrunk");
                if (newM.Count > oldMeasure.Count)
                    return new Measure(newM.GetRange(0, oldMeasure.Count).ToArray());
                else
                {
                    while (newM.Count < oldMeasure.Count)
                        newM.Add(0);
                    return new Measure(newM.ToArray());
                }
            }
        }

        public static Measure GetShrunk(RipplingEntry oldEntry, RipplingEntry newEntry)
        {
            if (oldEntry == null)
                throw new ArgumentNullException("oldEntry");
            if (newEntry == null)
                throw new ArgumentNullException("newEntry");

            return GetShrunk(oldEntry.GivenRoot, newEntry.GivenRoot, oldEntry.GivenSkeleton, newEntry.GivenSkeleton)
                 + GetShrunk(oldEntry.GoalRoot, newEntry.GoalRoot, oldEntry.GoalSkeleton, newEntry.GoalSkeleton);
        }

    }
}
