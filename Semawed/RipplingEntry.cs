﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semawed
{
    internal class RipplingEntry
    {
        private static bool equalityAndWF(BinaryTree target)
        {
            if (target.Data.Markup == MarkupType.WaveFront)
                return true;
            else
                return false;
        }
        private static Predicate<BinaryTree> equalityAndWFPredicate = new Predicate<BinaryTree>(equalityAndWF);

        private BinaryTree givenRoot;
        public BinaryTree GivenRoot
        {
            get
            {
                return givenRoot;
            }
        }

        private BinaryTree goalRoot;
        public BinaryTree GoalRoot
        {
            get
            {
                return goalRoot;
            }
        }

        private UnificationEntry relatedUEEntry;
        public UnificationEntry RelatedUnificationEntry
        {
            get
            {
                return relatedUEEntry;
            }
        }

        private RipplingController relatedController;
        internal RipplingController Controller
        {
            get
            {
                return relatedController;
            }
        }

        private Measure commonMeasure;
        public Measure CommonMeasure
        {
            get
            {
                if (IsSkeletonDepthPreserving)
                    return commonMeasure;
                else
                    return null;
            }
        }

        Skeleton givenSkeleton = null;
        Skeleton goalSkeleton = null;
        public Skeleton GivenSkeleton
        {
            get
            {
                if (givenSkeleton == null)
                    givenSkeleton = new Skeleton(GivenRoot);
                return givenSkeleton;
            }
        }

        public Skeleton GoalSkeleton
        {
            get
            {
                if (goalSkeleton == null)
                    goalSkeleton = new Skeleton(GoalRoot);
                return goalSkeleton;
            }
        }

        public bool IsSkeletonPreserving
        {
            get
            {
                return GivenSkeleton == GoalSkeleton;
            }
        }

        public bool IsSkeletonDepthPreserving
        {
            get
            {
                if (commonMeasure == null)
                    try
                    {
                        commonMeasure = new Measure(givenRoot) + new Measure(goalRoot);
                    }
                    catch (InvalidOperationException)
                    {
                        return false;
                    }
                return true;
            }
        }

        #region ruleapplying
        private List<string> rulesAppliedToGiven = new List<string>();
        private List<string> rulesAppliedToGoal = new List<string>();

        public IReadOnlyList<string> AppliedRulesToGiven
        {
            get
            {
                return rulesAppliedToGiven;
            }
        }

        public IReadOnlyList<string> AppliedRulesToGoal
        {
            get
            {
                return rulesAppliedToGoal;
            }
        }

        private bool ruleLoopCheck(List<string> appliedRules, WaveRule rule)
        {
            if (appliedRules.Count == 0)
                return true;
            string[] separators = { "###" };
            string lastId = appliedRules[appliedRules.Count - 1].Split(separators, StringSplitOptions.RemoveEmptyEntries)[0];
            if (rule.ReverseRuleId == lastId)
                return false;
            else
            {
                int counter = 0;
                if (appliedRules.Count > 3)
                {
                    foreach (string r in appliedRules)
                    {
                        string Id = r.Split(separators, StringSplitOptions.RemoveEmptyEntries)[0];
                        if (Id == rule.RuleId)
                            counter++;
                    }
                    if (counter > appliedRules.Count * 0.9)
                        return false;
                }
                return true;
            }
        }

        public bool ApplyRuleToGiven(WaveRule rule)
        {
            if (!ruleLoopCheck(rulesAppliedToGiven, rule))
                return false;
            string appliedRule;
            if (applyRule(rule, givenRoot, out appliedRule))
            {
                rulesAppliedToGiven.Add(appliedRule);
                commonMeasure = null;
                givenSkeleton = null;
                return true;
            }
            else
                return false;
        }

        public bool ApplyRuleToGoal(WaveRule rule)
        {
            if (!ruleLoopCheck(rulesAppliedToGiven, rule))
                return false;
            string appliedRule;
            if (applyRule(rule, goalRoot, out appliedRule))
            {
                rulesAppliedToGoal.Add(appliedRule);

                commonMeasure = null;
                goalSkeleton = null;
                return true;
            }
            else
                return false;
        }

        private static bool applyRule(WaveRule rule, BinaryTree target, out string appliedRule)
        {
            IReadOnlyList<BinaryTree> applyCandidates = target.FindAll(rule.LeftPart.Data);
            WaveRuleApplyHelper applier = new WaveRuleApplyHelper(rule);
            foreach (BinaryTree applyNode in applyCandidates)
            {
                if (applier.Apply(applyNode, out appliedRule))
                    return true;
            }
            appliedRule = null;
            return false;
        }
        #endregion

        public RipplingEntry(UnificationEntry entry)
        {
            if (entry == null)
                throw new ArgumentNullException("entry");
            if (!entry.UnificationComplete)
                throw new InvalidOperationException("Unification is not completed for specified entry");
            relatedUEEntry = entry;
            relatedController = null;
            givenRoot = entry.GivenRoot.DeepCopy();
            goalRoot = entry.GoalRoot.DeepCopy();
            commonMeasure = null;
        }

        internal RipplingEntry(UnificationEntry entry, RipplingController controller)
        {
            if (entry == null)
                throw new ArgumentNullException("entry");
            if (!entry.UnificationComplete)
                throw new InvalidOperationException("Unification is not completed for specified entry");
            relatedUEEntry = entry;
            relatedController = controller;
            givenRoot = entry.GivenRoot.DeepCopy();
            goalRoot = entry.GoalRoot.DeepCopy();
            commonMeasure = null;
        }

        private RipplingEntry(BinaryTree given, BinaryTree goal, UnificationEntry entry, Measure measure, List<string> appliedRulesToGiven, List<string> appliedRulesToGoal, RipplingController controller)
        {
            givenRoot = given.DeepCopy();
            goalRoot = goal.DeepCopy();
            relatedUEEntry = entry;
            commonMeasure = measure;
            relatedController = controller;
            foreach (var s in appliedRulesToGiven)
                rulesAppliedToGiven.Add(s);
            foreach (var s in appliedRulesToGoal)
                rulesAppliedToGoal.Add(s);
            //rulesAppliedToGiven.AddRange(appliedRulesToGiven);
            //rulesAppliedToGoal.AddRange(appliedRulesToGoal);
        }

        /// <summary>
        /// Copies current instance of RipplingEntry, but Skeleton copying is not implemented yet, so goal and given skeletons would be null.
        /// </summary>
        /// <returns></returns>
        public RipplingEntry DeepCopy()
        {
            return new RipplingEntry(givenRoot, goalRoot, relatedUEEntry, commonMeasure, rulesAppliedToGiven, rulesAppliedToGoal, relatedController);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public HashSet<Element> GetCommonElements()
        {
            HashSet<Element> commonElements = goalRoot.GetAllElements();
            commonElements.IntersectWith(givenRoot.GetAllElements());
            return commonElements;
        }

        /// <summary>
        /// No multithreading on this one. Returns non-empty list containing current object and all possible skeleton enlargements producing equal measures
        /// </summary>
        /// <returns></returns>
        public IReadOnlyList<RipplingEntry> GetSkeletonEnlargements()
        {
            List<RipplingEntry> result = new List<RipplingEntry>();
            result.Add(this);
            foreach (Element e in GetCommonElements())
            {
                IReadOnlyList<BinaryTree> givenEnlargementCandidates = GivenRoot.FindAll(e, equalityAndWFPredicate);
                IReadOnlyList<BinaryTree> goalEnlargementCandidates = GoalRoot.FindAll(e, equalityAndWFPredicate);

                foreach (BinaryTree t1 in givenEnlargementCandidates)
                    foreach (BinaryTree t2 in goalEnlargementCandidates)
                    {
                        t1.Data.Markup = MarkupType.Skeleton;
                        t2.Data.Markup = MarkupType.Skeleton;
                        Measure m1 = new Measure(givenRoot);
                        Measure m2 = new Measure(goalRoot);
                        if (m1.Count == m2.Count)
                            result.Add(new RipplingEntry(givenRoot, goalRoot, relatedUEEntry, m1 + m2, rulesAppliedToGiven, rulesAppliedToGoal, relatedController));
                        t1.Data.Markup = MarkupType.WaveFront;
                        t2.Data.Markup = MarkupType.WaveFront;
                    }
            }
            return result;
        }
    }
}
