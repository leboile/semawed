﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semawed
{
    [Serializable()]
    public enum ElementType
    {
        ServiceObject,
        Function,
        Variable,
        Constant
    }

    [Serializable()]
    public enum MarkupType
    {
        Undefined,
        WaveFront,
        Skeleton
    }

    [Serializable()]
    public abstract class Element
    {
        public static string[] CMathMLFunctions = { "factorial", "minus", "abs", "conjugate", "arg", "real", "imaginary", "floor", 
                                                      "ceiling", "not", "inverse", "ident", "domain", "codomain", "image", "sin", 
                                                      "cos", "tan", "sec", "csc", "cot", "sinh", "cosh", "tanh", "sech", "csch", 
                                                      "coth", "arcsin", "arccos", "arctan", "arccosh", "arccot", "arccoth", 
                                                      "arccsc", "arccsch", "arcsec", "arcsech", "arcsinh", "arctanh", "exp", "ln", "log",
                                                  "determinant", "transpose", "divergence", "grad", "curl", "laplacian", "card", "quotient", 
                                                  "divide", "minus", "power", "rem", "implies", "equivalent", "approx", "setdiff", 
                                                  "vectorproduct", "scalarproduct", "outerproduct", "plus", "times", "max", "min", 
                                                  "gcd", "lcm", "mean", "sdev", "variance", "median", "mode", 
                                                  "and", "or", "xor", "selector", "union", "intersect", "cartesianproduct", 
                                                  "fn", "compose", "int", "sum", "product", "diff", "partialdiff", "forall", "exists"};
        public static string[] CMathMLQualifiers = { "bvar", "lowlimit", "uplimit", "interval", "condition", "domainofapplication", "degree", "momentabou", "logbase" };
        protected string elementName;
        public string Name
        {
            get
            {
                return elementName;
            }
        }

        protected ElementType elementType;
        public ElementType TypeOfElement
        {
            get
            {
                return elementType;
            }
        }

        protected MarkupType elementMarkup = MarkupType.Undefined;
        public MarkupType Markup
        {
            get
            {
                return elementMarkup;
            }
            set
            {
                elementMarkup = value;
            }
        }

        public override bool Equals(object obj)
        {
            Element o = obj as Element;
            if (o == null)
                return false;
            return (elementName == o.elementName) && (elementType == o.elementType);
        }

        public bool Equals(object value, bool considerMarkup)
        {
            if (!considerMarkup)
                return Equals(value);
            Element o = value as Element;
            if (o == null)
                return false;
            return (elementName == o.elementName) && (elementType == o.elementType) && (elementMarkup == o.elementMarkup);
        }

        public static bool operator ==(Element obj1, Element obj2)
        {
            //if ((object)obj1 == null || (object)obj2 == null)
            //    return false;
            //return obj1.Equals(obj2);
            if ((object)obj1 == null && (object)obj2 == null)
                return true;
            else
                if ((object)obj1 != null && (object)obj2 != null)
                    return obj1.Equals(obj2);
                else
                    return false;
        }

        public static bool operator !=(Element obj1, Element obj2)
        {
            return !(obj1 == obj2);
        }

        public override int GetHashCode()//it is used in dictionary for example while doing trygetvalue
        {
            return (elementName + " " + elementType.ToString()).GetHashCode();//good enough
            //return base.GetHashCode();
        }

        public Element Copy()
        {
            switch (elementType)
            {
                case ElementType.Constant:
                    return new Constant(elementName, elementMarkup);
                case ElementType.Function:
                    return new Function(elementName, elementMarkup);
                case ElementType.ServiceObject:
                    return new ServiceObject(elementName, elementMarkup);
                case ElementType.Variable:
                    return new Variable(elementName, elementMarkup);
                default:
                    return null;
            }
        }

        public string ToCMathMLNode()
        {
            switch (elementType)
            {
                case ElementType.Constant:
                    return "<cn>" + elementName + "</cn>";
                case ElementType.Function:
                    if (Array.FindIndex(Function.CMathMLFunctions, x => x.Equals(elementName)) > -1)
                        return "<" + elementName + "/>";
                    else
                        return "<ci>" + elementName + "</ci>";
                case ElementType.Variable:
                    return "<ci>" + elementName + "</ci>";
                default:
                    return null;
            }
        }

        public static Element DeserializeFromString(string s)
        {
            char[] separators = { '#' };
            string[] split = s.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            char markupchar = Convert.ToChar(split[2]);
            MarkupType markup = markupchar == 'S' ? MarkupType.Skeleton : markupchar == 'U' ? MarkupType.Undefined : MarkupType.WaveFront;
            char type = Convert.ToChar(split[3]);
            switch (type)
            {
                case 'F':
                    return new Function(split[1], markup);
                case 'V':
                    return new Variable(split[1], markup);
                case 'C':
                    return new Constant(split[1], markup);
                case 'S':
                    return new ServiceObject(split[1], markup);
                default:
                    throw new InvalidOperationException("Deserialization went wrong");
            }
        }

        public static string SerializeToString(Element e)
        {
            char markup = (e.Markup == MarkupType.Skeleton ? 'S' : e.Markup == MarkupType.Undefined ? 'U' : 'W');
            char type = 'S';
            switch (e.elementType)
            {
                case ElementType.Constant:
                    type = 'C';
                    break;
                case ElementType.Function:
                    type = 'F';
                    break;
                case ElementType.Variable:
                    type = 'V';
                    break;
            }
            return "(#" + e.elementName + "#" + markup + "#" + type + "#)";
        }

        public bool TryGetTeXRepresentation(out string tex)
        {
            switch(Name)
            {
                case "plus":
                    tex = "+";
                    return true;
                case "minus":
                    tex = "-";
                    return true;
                case "times":
                    tex = "\\cdot";
                    return true;
                case "divide":
                    tex = "\\frac";
                    return true;
                case "sin":
                    tex = "\\sin";
                    return true;
                case "cos":
                    tex = "\\cos";
                    return true;
                case "power":
                    tex = "^";
                    return true;
                default:
                    if (this.TypeOfElement == ElementType.Constant)
                    {
                        tex = Name;
                        return true;
                    }
                    tex = null;
                    return false;
            }
        }

        public override string ToString()
        {
            return elementName + " " + elementMarkup;
        }

        public static bool IsQualifier(string q)
        {
            if (Array.FindIndex(Element.CMathMLQualifiers, x => x.Equals(q.ToLower())) > -1)
                return true;
            return false;
        }
    }

    [Serializable()]
    public class ServiceObject : Element
    {
        public ServiceObject(string name)
        {
            if (name == null)
                throw new ArgumentNullException("name");
            if (name.Length == 0)
                throw new ArgumentException("Empty string cannot be name", "name");
            elementName = name.Trim().ToLower();
            elementType = ElementType.ServiceObject;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToLower")]
        public ServiceObject(string name, MarkupType markup)
        {
            if (name == null)
                throw new ArgumentNullException("name");
            if (name.Length == 0)
                throw new ArgumentException("Empty string cannot be name", "name");
            elementName = name.Trim().ToLower();
            elementType = ElementType.ServiceObject;
            elementMarkup = markup;
        }
    }

    [Serializable()]
    public class Function : Element
    {
        private static string[] commutativeFunctions = { "plus", "times" };
        private bool commutative;
        public bool Commutative
        {
            get
            {
                return commutative;
            }
        }

        public static bool IsCommutative(Element e)
        {
            if (e.TypeOfElement != ElementType.Function)
                return false;
            if (commutativeFunctions.Contains(e.Name.Trim().ToLower()))
                return true;
            else
                return false;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToLower")]
        public Function(string name)
        {
            if (name == null)
                throw new ArgumentNullException("name");
            if (name.Length == 0)
                throw new ArgumentException("Empty string cannot be name", "name");
            elementName = name.Trim().ToLower();
            elementType = ElementType.Function;
            if (commutativeFunctions.Contains(elementName))
                commutative = true;
            else
                commutative = false;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToLower")]
        public Function(string name, MarkupType markup)
        {
            if (name == null)
                throw new ArgumentNullException("name");
            if (name.Length == 0)
                throw new ArgumentException("Empty string cannot be name", "name");
            elementName = name.Trim().ToLower();
            elementType = ElementType.Function;
            elementMarkup = markup;
            if (commutativeFunctions.Contains(elementName))
                commutative = true;
            else
                commutative = false;
        }
    }

    [Serializable()]
    public class Variable : Element
    {
        public Variable(string name)
        {
            if (name == null)
                throw new ArgumentNullException("name");
            if (name.Length == 0)
                throw new ArgumentException("Empty string cannot be name", "name");
            elementName = name.Trim();
            elementType = ElementType.Variable;
        }
        public Variable(string name, MarkupType markup)
        {
            if (name == null)
                throw new ArgumentNullException("name");
            if (name.Length == 0)
                throw new ArgumentException("Empty string cannot be name", "name");
            elementName = name.Trim();
            elementType = ElementType.Variable;
            elementMarkup = markup;
        }
    }

    [Serializable()]
    public class Constant : Element
    {
        public Constant(string name)
        {
            if (name == null)
                throw new ArgumentNullException("name");
            if (name.Length == 0)
                throw new ArgumentException("Empty string cannot be name", "name");
            elementName = name.Trim();
            elementType = ElementType.Constant;
        }

        public Constant(string name, MarkupType markup)
        {
            if (name == null)
                throw new ArgumentNullException("name");
            if (name.Length == 0)
                throw new ArgumentException("Empty string cannot be name", "name");
            elementName = name.Trim();
            elementType = ElementType.Constant;
            elementMarkup = markup;
        }
    }
}
