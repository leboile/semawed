﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Semawed
{

    internal class ElementEqualityComparer : IEqualityComparer<Element>
    {
        public bool Equals(Element x, Element y)
        {
            if (x == null && y == null)
                return true;
            else
                if (x == null || y == null)
                    return false;
            if (x.Equals(y))
                return x.Markup == y.Markup;
            else
                return false;
        }

        public int GetHashCode(Element obj)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");
            return obj.GetHashCode();
        }
    }

    internal class WaveRuleApplyHelper
    {
        private static int substLimit = 50;
        private WaveRule WR;
        private Dictionary<Element, BinaryTree> substTable; //key - elements from rule; value - elements from original tree
        public IReadOnlyDictionary<Element, BinaryTree> SubstitutionTable
        {
            get
            {
                return substTable;
            }
        }
        public WaveRuleApplyHelper(WaveRule rule)
        {
            WR = rule;
            substTable = new Dictionary<Element, BinaryTree>();
        }
        public override string ToString()
        {
            StringBuilder SB = new StringBuilder();
            SB.AppendLine("Dictionary for rule " + WR.ToString());
            foreach (KeyValuePair<Element, BinaryTree> pair in substTable)
                SB.AppendLine(pair.Key + " " + pair.Value.Data);
            return SB.ToString();
        }
        private bool checkAppliability(BinaryTree target, BinaryTree ruleLeftPart)
        {
            if (target == null && ruleLeftPart == null)
                return true;
            if (target == null || ruleLeftPart == null)
                return false;
            switch (ruleLeftPart.Data.TypeOfElement)
            {
                case ElementType.Variable:
                    BinaryTree val;
                    if (substTable.TryGetValue(ruleLeftPart.Data, out val))//this variable is already been substituted
                    {
                        if (BinaryTree.CheckStructure(val, target, new ElementEqualityComparer()))//but its ok then
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        if (ruleLeftPart.Data.Markup == MarkupType.WaveFront && !checkDirection(target, MarkupType.WaveFront))
                            return false;
                        substTable.Add(ruleLeftPart.Data.Copy(), target.DeepCopy());
                    }
                    return true;
                case ElementType.Function:
                case ElementType.Constant:
                    if (ruleLeftPart.Data == target.Data && ruleLeftPart.Data.Markup == target.Data.Markup)
                        return checkAppliability(target.Left, ruleLeftPart.Left) && checkAppliability(target.Right, ruleLeftPart.Right);
                    else
                        return false;
                default:
                    throw new NotImplementedException("Default while checking if rule can be applied");
            }
        }

        private bool checkDirection(BinaryTree tree, MarkupType d)
        {
            if (tree == null)
                return true;
            if (tree.Data.Markup != d)
                return false;
            else
                return checkDirection(tree.Left, d) && checkDirection(tree.Right, d);
        }

        public bool CanApply(BinaryTree tree)
        {
            substTable.Clear();
            bool answer = checkAppliability(tree, WR.LeftPart);// checkStructure(tree, WR.leftPart.DeepCopy());
            if (answer)
            {
                //new variable subst
                var rightPartVars = WR.RightPart.GetAllVariables();
                foreach(var pair in substTable)
                    if (pair.Key.TypeOfElement==ElementType.Variable)
                        rightPartVars.Remove((Variable)pair.Key);
                var treeVars = tree.Root.GetAllVariables();
                if (treeVars.Count >= rightPartVars.Count)
                {
                    var rightPartVarsList = rightPartVars.ToList();
                    var treeVarsList = treeVars.ToList();
                    for (int i = 0; i < rightPartVars.Count; i++ )
                        substTable.Add(rightPartVarsList[i], new BinaryTree(treeVarsList[i]));
                }
                else
                    return false;


                //Console.WriteLine("SUBSTTABLE: ");
                //foreach (KeyValuePair<Element, BinaryTree> wre in substTable)
                //{
                //    Console.WriteLine(wre.Key.name + " ---- " + wre.Value.Data.name);
                //}
                //Console.WriteLine("END SUBSTTABLE");
            }
            return answer;
        }

        public bool Apply(BinaryTree tree, out string appliedRule)
        {
            if (tree == null)
                throw new ArgumentNullException("tree");
            if (CanApply(tree))
            {
                BinaryTree left = tree.DeepCopy();
                BinaryTree substTree = WR.RightPart.DeepCopy();
                WaveRuleApplyHelper.Substitute(substTree, substTable);//now i reject your reality and substitute my own
                tree.ReplaceCurrentNodeWith(substTree);
                //BinaryTree left2 = WR.LeftPart.DeepCopy();
                //Substitute(left2, substTable);
                appliedRule = WR.ToCMathMLString();
                return true;
            }
            else
            {
                appliedRule = null;
                return false;
            }
        }

        //public static void Substitute(BinaryTree tree, Dictionary<Element, BinaryTree> substitutionTable)//so much room for optimization
        //{
        //    if (substitutionTable == null)
        //        throw new ArgumentNullException("substitutionTable");
        //    if (tree != null)
        //    {
        //        Substitute(tree.Left, substitutionTable);
        //        Substitute(tree.Right, substitutionTable);
        //    }
        //    else
        //        return;

        //    bool substAgain = false;
        //    BinaryTree value;
        //    if (tree.Left != null)
        //    {
        //        substitutionTable.TryGetValue(tree.Left.Data, out value);
        //        if (value != null)
        //        {
        //            MarkupType dir = tree.Left.Data.Markup;
        //            tree.Left = value.DeepCopy();
        //            substAgain = true;
        //            if (dir == MarkupType.WaveFront)
        //                Unification.SetMarkup(tree.Left, dir);
        //        }
        //        //else
        //        //    Console.WriteLine("Left NULL "+tree.Left.Data);
        //    }
        //    value = null;
        //    if (tree.Right != null)
        //    {
        //        substitutionTable.TryGetValue(tree.Right.Data, out value);
        //        if (value != null)
        //        {
        //            MarkupType dir = tree.Right.Data.Markup;
        //            tree.Right = value.DeepCopy();
        //            substAgain = true;
        //            if (dir == MarkupType.WaveFront)
        //                Unification.SetMarkup(tree.Right, dir);
        //        }
        //        //else
        //        //    Console.WriteLine("Right NULL "+tree.Right.Data);
        //    }
        //    if (substAgain)
        //        Substitute(tree, substitutionTable);
        //}
        public static void Substitute(BinaryTree tree, Dictionary<Element, BinaryTree> substitutionTable)//so much room for optimization
        {
            int counter = 0;
            substitute(tree, substitutionTable, counter);
        }

        private static void substitute(BinaryTree tree, Dictionary<Element, BinaryTree> substitutionTable, int counter)//so much room for optimization
        {
            if (substitutionTable == null)
                throw new ArgumentNullException("substitutionTable");
            if (counter > substLimit)
                throw new InvalidOperationException("Substitution depth limit of " + substLimit + " is reached");
            if (tree == null)
                return;

            bool substAgain = false;
            BinaryTree value;
            if (substitutionTable.TryGetValue(tree.Data, out value))
            {
                MarkupType dir = tree.Data.Markup;
                tree.ReplaceCurrentNodeWith(value.DeepCopy());
                substAgain = true;
                if (dir == MarkupType.WaveFront)
                    Unification.SetMarkup(tree, dir);
            }

            if (substAgain)
                substitute(tree, substitutionTable, counter+1);
            else
            {
                substitute(tree.Left, substitutionTable, counter);
                substitute(tree.Right, substitutionTable, counter);
            }
        }
    }

    [Serializable()]
    public class WaveRule
    {
        private BinaryTree leftPart;
        public BinaryTree LeftPart
        {
            get
            {
                return leftPart;
            }
        }
        private BinaryTree rightPart;
        public BinaryTree RightPart
        {
            get
            {
                return rightPart;
            }
        }
        private string description;
        public string Description
        {
            get
            {
                return description;
            }
        }
        private string reverseRuleId;
        public string ReverseRuleId
        {
            get
            {
                return reverseRuleId;
            }
        }
        private string ruleId;
        public string RuleId
        {
            get
            {
                return ruleId;
            }
        }

        public WaveRule(BinaryTree left, BinaryTree right, string descriptionText, string id, string reversedRuleId)
        {
            description = descriptionText;
            leftPart = left;
            rightPart = right;
            ruleId = id;
            reverseRuleId = reversedRuleId;
        }

        public static bool Save(string fileName, WaveRule rule)
        {
            Stream s = null;
            bool res;
            try
            {
                s = File.Create(fileName);
                BinaryFormatter serializer = new BinaryFormatter();
                serializer.Serialize(s, rule);
                res = true;
            }
            finally
            {
                if (s != null)
                    s.Close();
            }
            return res;
        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "1#")]
        public static bool Load(string fileName, ref WaveRule rule)
        {
            Stream s = null;
            bool res;
            try
            {
                s = File.OpenRead(fileName);
                BinaryFormatter deserializer = new BinaryFormatter();
                rule = (WaveRule)deserializer.Deserialize(s);
                res = true;
            }
            finally
            {
                if (s != null)
                    s.Close();
            }
            return res;
        }

        public override string ToString()
        {
            return ruleId+"###"+description + "###" + leftPart.ToString() + " --> " + rightPart.ToString();
        }

        public string ToCMathMLString()
        {
            return ruleId+"###"+description + "###" + leftPart.ToCMathML() + " --> " + rightPart.ToCMathML();
        }

        public static int CreateWaveRules(string first, string second, string name, string reversedRuleName, string path, bool allowEqual=false)
        {
            char[] notAllowedChars = {'3','4','5','6','7','8','9','0'};
            if (name.IndexOfAny(notAllowedChars)>0)
                throw new ArgumentException("name");
            Expression e1 = new Expression(File.ReadAllText(first), true);
            Expression e2 = new Expression(File.ReadAllText(second), true);
            IReadOnlyList<UnificationEntry> unif = Unification.Annotate(e1, e2, UnificationOption.CostAndSubstitutionMinimal);
            int rulesCount = 0;
            foreach (UnificationEntry e in unif)
            {
                if (e.UnificationComplete)
                {
                    try
                    {
                        Measure m1 = new Measure(e.GivenRoot);
                        Measure m2 = new Measure(e.GoalRoot);
                        if (m1 > m2)
                            rulesCount++;
                        else
                        {
                            if (allowEqual && m1 == m2)
                            {
                                Console.WriteLine("EQUAL MEASURE");
                                rulesCount++;
                            }
                            else
                            {
                                continue;
                            }
                        }
                        Rippling.SystemLog.Add("New rule for " + name);
                        Rippling.SystemLog.Add(e.GivenRoot.ToString());
                        Rippling.SystemLog.Add(e.GoalRoot.ToString());
                        WaveRule WR = new WaveRule(e.GivenRoot, e.GoalRoot, name + "#" + rulesCount, name, reversedRuleName);
                        WaveRule.Save(Path.Combine(path, name + rulesCount), WR);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            Console.WriteLine("Unifications count for " + name + " : " + unif.Count + " measure decreasing: " + rulesCount);
            return rulesCount;
        }

        public static int CreateWaveRules(string first, string second, string name, string path, bool allowEqual = false)
        {
            char[] notAllowedChars = { '3', '4', '5', '6', '7', '8', '9', '0' };
            if (name.IndexOfAny(notAllowedChars) > 0)
                throw new ArgumentException("name");
            Expression e1 = new Expression(File.ReadAllText(first), true);
            Expression e2 = new Expression(File.ReadAllText(second), true);
            IReadOnlyList<UnificationEntry> unif = Unification.Annotate(e1, e2, UnificationOption.CostAndSubstitutionMinimal);
            int rulesCount = 0;
            foreach (UnificationEntry e in unif)
            {
                if (e.UnificationComplete)
                {
                    try
                    {
                        Measure m1 = new Measure(e.GivenRoot);
                        Measure m2 = new Measure(e.GoalRoot);
                        if (m1 > m2)
                            rulesCount++;
                        else
                        {
                            if (allowEqual && m1 == m2)
                            {
                                Console.WriteLine("EQUAL MEASURE");
                                rulesCount++;
                            }
                            else
                            {
                                continue;
                            }
                        }
                        Rippling.SystemLog.Add("New rule for " + name);
                        Rippling.SystemLog.Add(e.GivenRoot.ToString());
                        Rippling.SystemLog.Add(e.GoalRoot.ToString());
                        WaveRule WR = new WaveRule(e.GivenRoot, e.GoalRoot, name + "#" + rulesCount, name, name+"_REVERSED");
                        WaveRule.Save(Path.Combine(path, name + rulesCount), WR);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            Console.WriteLine("Unifications count for " + name + " : " + unif.Count + " measure decreasing: " + rulesCount);
            return rulesCount;
        }
    }
}
