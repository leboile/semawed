﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semawed
{
    public enum UnificationOption
    {
        NoRestrictions,
        CostMinimal,
        SubstitutionMinimal,
        CostAndSubstitutionMinimal
    }

    internal class UEEventArgs : EventArgs
    {
        private UnificationEntry entry;
        public UnificationEntry Entry
        {
            get
            {
                return entry;
            }
        }

        public UEEventArgs(UnificationEntry newEntry)
        {
            entry = newEntry;
        }
    }

    internal class UnificationController
    {
        //event raised when new unificationentry is completed
        public event EventHandler<UEEventArgs> NewUnificationEntryEvent;
        public event EventHandler<EventArgs> UnificationFinishedEvent;
        private List<UnificationEntry> results;

        private bool costMinimal;
        public bool CostMinimal
        {
            get
            {
                return costMinimal;
            }
        }

        private bool substMinimal;
        public bool SubstitutionCountMinimal
        {
            get
            {
                return substMinimal;
            }
        }

        private bool finishUnificationFlag;
        public bool IsUnificationFinished
        {
            get
            {
                return finishUnificationFlag;
            }
        }

        int maxAmountOfRules;
        public int MaxAmountOfAppliedRules
        {
            get
            {
                return maxAmountOfRules;
            }
        }

        private bool useConveyor;
        public bool ProceedWithRippling
        {
            get
            {
                return useConveyor;
            }
        }

        private bool forceDD;
        public bool ForcedDECOMPOSEDELETE
        {
            get
            {
                return forceDD;
            }
        }
        private bool minimalUnificationComplete;
        public bool MinimalUnificationComplete
        {
            get
            {
                return minimalUnificationComplete;
            }
            set
            {
                minimalUnificationComplete = value;
            }
        }
        private int minimalUnificationSubstCount;
        public int MinimalUnificationSubstCount
        {
            get
            {
                return minimalUnificationSubstCount;
            }
            set
            {
                minimalUnificationSubstCount = value;
            }
        }

        private int minimalUnificationCost;
        internal int MinimalUnificationCost
        {
            get
            {
                return minimalUnificationCost;
            }
            set
            {
                minimalUnificationCost = value;
            }
        }

        public UnificationController(UnificationOption minimal, bool forcedd, int max, bool conveyor)
        {
            forceDD = forcedd;
            useConveyor = conveyor;
            finishUnificationFlag = false;
            maxAmountOfRules = max;
            switch (minimal)
            {
                case UnificationOption.NoRestrictions:
                    substMinimal = false;
                    costMinimal = false;
                    break;
                case UnificationOption.CostMinimal:
                    substMinimal = false;
                    costMinimal = true;
                    break;
                case UnificationOption.SubstitutionMinimal:
                    substMinimal = true;
                    costMinimal = false;
                    break;
                case UnificationOption.CostAndSubstitutionMinimal:
                    substMinimal = true;
                    costMinimal = true;
                    break;
            }
            minimalUnificationComplete = false;
            minimalUnificationSubstCount = int.MaxValue;
            minimalUnificationCost = int.MaxValue;
            results = new List<UnificationEntry>();
        }

        /// <summary>
        /// Method for adding to storage unification entry which is not completely unificated yet
        /// </summary>
        /// <param name="newEntry">UnificationEntry to be added</param>
        public void AddUnification(UnificationEntry newEntry)
        {
            if (!useConveyor)
                results.Add(newEntry);
        }

        public void RemoveUnification(UnificationEntry entry)
        {
            results.Remove(entry);
        }

        public void FinishUnificationProcess()
        {
            finishUnificationFlag = true;
            RaiseNewUnificationFinishedEvent(new EventArgs());
        }

        private void RaiseNewUEEntryEvent(UEEventArgs e)
        {
            EventHandler<UEEventArgs> handler = NewUnificationEntryEvent;
            // Event will be null if there are no subscribers
            if (handler != null)
            {
                handler(this, e);//event raised
            }
        }

        private void RaiseNewUnificationFinishedEvent(EventArgs e)
        {
            EventHandler<EventArgs> handler = UnificationFinishedEvent;
            // Event will be null if there are no subscribers
            if (handler != null)
            {
                handler(this, e);//event raised
            }
        }

        public void StartRippling(UnificationEntry e)
        {
            if (!useConveyor)
                throw new InvalidOperationException("Rippling is not used for current parameters");
            if (!e.UnificationComplete)
                throw new ArgumentException("e", "Unification is not completed for specified entry");
            RaiseNewUEEntryEvent(new UEEventArgs(e));
        }

        public List<UnificationEntry> GetResults()//returning copy of the list, but objects
        {
            if (useConveyor)
                throw new InvalidOperationException("Annotations prcoess is using conveyor. No unification results are stored");
            if (!finishUnificationFlag)
                throw new InvalidOperationException("Unification is not finished yet");
            return new List<UnificationEntry>(results);
        }
    }
}
