﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Semawed;
namespace UnitTest
{
    [TestClass]
    public class ElementTest
    {

        [TestMethod]
        public void VariablesConstructorsTest()
        {
            try
            {
                Variable f4 = new Variable("", MarkupType.Skeleton);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentException));
            }

            try
            {
                Variable f3 = new Variable("");
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentException));
            }

            try
            {
                Variable f2 = new Variable(null);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            }

            try
            {
                Variable f = new Variable(null, MarkupType.Skeleton);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            }
        }

        [TestMethod]
        public void FunctionConstructorsTest()
        {
            try
            {
                Function f4 = new Function("", MarkupType.Skeleton);
                Assert.Fail();
            }
            catch(Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentException));
            }

            try
            {
                Function f3 = new Function("");
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentException));
            }

            try
            {
                Function f2 = new Function(null);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            }

            try
            {
                Function f = new Function(null, MarkupType.Skeleton);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            }
            //commutativness
            Function f5 = new Function("times", MarkupType.Skeleton);
            Assert.IsTrue(f5.Commutative);
            Function f6 = new Function("times");
            Assert.IsTrue(f6.Commutative);
            Function f7 = new Function("ts");
            Assert.IsFalse(f7.Commutative);
        }

        [TestMethod]
        public void FunctionEqualsAndCopyTest()
        {
            Function f = new Function("f", MarkupType.Skeleton);
            Function f1 = new Function("f1", MarkupType.Skeleton);
            Function f2 = new Function("f", MarkupType.WaveFront);
            Assert.IsTrue(f.Equals(f2) && f.Equals(f2,false));
            Assert.IsFalse(f.Equals(f1));
            Assert.IsFalse(f.Equals(f2, true));
            Assert.IsTrue(f.Copy().Equals(f2.Copy()) && f.Copy().Equals(f2.Copy(), false));
            Assert.IsFalse(f.Copy().Equals(f1) || f.Equals(f1.Copy()));
            Assert.IsFalse(f.Equals(f2.Copy(), true));
            Assert.IsFalse(f.Equals(null));
            Assert.IsFalse(f.Equals(null, true));
        }

        [TestMethod]
        public void VariableEqualsAndCopyTest()
        {
            Variable f = new Variable("f", MarkupType.Skeleton);
            Variable f1 = new Variable("f1", MarkupType.Skeleton);
            Variable f2 = new Variable("f", MarkupType.WaveFront);
            Assert.IsTrue(f.Equals(f2) && f.Equals(f2, false));
            Assert.IsFalse(f.Equals(f1));
            Assert.IsFalse(f.Equals(f2, true));
            Assert.IsTrue(f.Copy().Equals(f2.Copy()) && f.Copy().Equals(f2.Copy(), false));
            Assert.IsFalse(f.Copy().Equals(f1) || f.Equals(f1.Copy()));
            Assert.IsFalse(f.Equals(f2.Copy(), true));
            Assert.IsFalse(f.Equals(null));
            Assert.IsFalse(f.Equals(null, true));
        }

        [TestMethod]
        public void ConstantEqualsAndCopyTest()
        {
            Constant f = new Constant("f", MarkupType.Skeleton);
            Constant f1 = new Constant("f1", MarkupType.Skeleton);
            Constant f2 = new Constant("f", MarkupType.WaveFront);
            Assert.IsTrue(f.Equals(f2) && f.Equals(f2, false));
            Assert.IsFalse(f.Equals(f1));
            Assert.IsFalse(f.Equals(f2, true));
            Assert.IsTrue(f.Copy().Equals(f2.Copy()) && f.Copy().Equals(f2.Copy(), false));
            Assert.IsFalse(f.Copy().Equals(f1) || f.Equals(f1.Copy()));
            Assert.IsFalse(f.Equals(f2.Copy(), true));
            Assert.IsFalse(f.Equals(null));
            Assert.IsFalse(f.Equals(null, true));
        }

        [TestMethod]
        public void ServiceObjectEqualsAndCopyTest()
        {
            ServiceObject f = new ServiceObject("f", MarkupType.Skeleton);
            ServiceObject f1 = new ServiceObject("f1", MarkupType.Skeleton);
            ServiceObject f2 = new ServiceObject("f", MarkupType.WaveFront);
            Assert.IsTrue(f.Equals(f2) && f.Equals(f2, false));
            Assert.IsFalse(f.Equals(f1));
            Assert.IsFalse(f.Equals(f2, true));
            Assert.IsTrue(f.Copy().Equals(f2.Copy()) && f.Copy().Equals(f2.Copy(), false));
            Assert.IsFalse(f.Copy().Equals(f1) || f.Equals(f1.Copy()));
            Assert.IsFalse(f.Equals(f2.Copy(), true));
            Assert.IsFalse(f.Equals(null));
            Assert.IsFalse(f.Equals(null, true));
        }

        [TestMethod]
        public void ElementSerializationTest()
        {
            Variable v = new Variable("x", MarkupType.WaveFront);
            Console.WriteLine(Element.SerializeToString(v));
            Console.WriteLine(Element.DeserializeFromString(Element.SerializeToString(v)));
            Assert.AreEqual(Element.DeserializeFromString(Element.SerializeToString(v)), v);
        }
    }
}
