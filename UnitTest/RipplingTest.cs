﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Semawed;
namespace UnitTest
{
    [TestClass]
    public class RipplingTest
    {
        string txt9 = "<math><apply><times/><apply><plus/><ci>x</ci><ci>y</ci></apply><apply><plus/><ci>z</ci><ci>w</ci></apply></apply></math>";
        string txt10 = "<math><apply><plus/><apply><plus/><apply><times/><ci>x</ci><ci>z</ci></apply><apply><times/><ci>y</ci><ci>z</ci></apply></apply><apply><plus/><apply><times/><ci>x</ci><ci>w</ci></apply><apply><times/><ci>y</ci><ci>w</ci></apply></apply></apply></math>";//xz+yz+xw+yw
        string txt12 = "<math><apply><plus/><apply><times/><apply><plus/><ci>a</ci><ci>b</ci></apply><ci>c</ci></apply><apply><times/><apply><plus/><ci>a</ci><ci>b</ci></apply><ci>d</ci></apply></apply></math>";//(a+b)*c+(a+b)*d
        string expr = "<math><apply><plus/><apply><times/><apply><plus/><ci>x</ci><ci>y</ci></apply><ci>z</ci></apply><apply><times/><ci>x</ci><ci>w</ci></apply><apply><times/><ci>y</ci><ci>w</ci></apply></apply></math>";//(x+y)z+xw+yw
        string distrib1 = "<math><apply><times/><apply><plus/><ci>a</ci><ci>b</ci></apply><ci>c</ci></apply></math>";
        string distrib2 = "<math><apply><plus/><apply><times/><ci>a</ci><ci>c</ci></apply><apply><times/><ci>b</ci><ci>c</ci></apply></apply></math>";

        [TestMethod]
        public void RipplingProofTimeLimit1()
        {
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            DateTime start = DateTime.Now;
            ProofResult result = Rippling.PerformProof(new Expression(txt9), new Expression(txt10), 1.0, ProofMode.Slow, StopConditions.LimitReached, new TimeSpan(0, 0, 0, 0, 500));
            TimeSpan timeSpent = DateTime.Now - start;
            Assert.AreEqual(500, timeSpent.TotalMilliseconds, 200);
            Assert.AreEqual(true, result.IsSuccess);
        }

        [TestMethod]
        public void RipplingProofTimeLimit2()
        {
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            DateTime start = DateTime.Now;
            ProofResult result = Rippling.PerformProof(new Expression(txt9), new Expression(txt10), 1.0, ProofMode.Full, StopConditions.LimitReached, new TimeSpan(0, 0, 0, 0, 500));
            TimeSpan timeSpent = DateTime.Now - start;
            Assert.AreEqual(500, timeSpent.TotalMilliseconds, 200);
            Assert.AreEqual(true, result.IsSuccess);
        }

        [TestMethod]
        public void RipplingProofTimeLimitAndMemoryTest()
        {
            List<System.Threading.Tasks.Task> l = new List<System.Threading.Tasks.Task>();
            System.Threading.Tasks.Task.WaitAll(l.ToArray());
            //long mbBeforeExecution = GC.GetTotalMemory(false) / (1024 * 1024);
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            double averageTime = 0;
            for (int i = 0; i < 100; i++)
            {
                DateTime start = DateTime.Now;
                ProofResult result = Rippling.PerformProof(new Expression(txt9), new Expression(txt10), 1.0, ProofMode.Full, StopConditions.LimitReached, new TimeSpan(0, 0, 0, 0, 200));
                TimeSpan timeSpent = DateTime.Now - start;
                if (i == 0)
                {
                    Assert.AreEqual(200, timeSpent.TotalMilliseconds, 100);
                    Assert.AreEqual(true, result.IsSuccess);
                }
                else
                    Console.WriteLine(timeSpent.TotalMilliseconds + " " + result.IsSuccess);
                averageTime += timeSpent.TotalMilliseconds;
            }
            averageTime = averageTime / 100;
            Assert.AreEqual(200, averageTime, 100);
            Console.WriteLine("Avg time: " + averageTime);
            Console.WriteLine("After: " + (GC.GetTotalMemory(false) / (double)(1024 * 1024)));
        }

        [TestMethod]
        public void RipplingProofInstantVariablesTest1()
        {
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            bool result = false;
            IReadOnlyCollection<UnificationEntry> unifs = Unification.Annotate(new Expression(txt9, true), new Expression(txt10, true), UnificationOption.CostAndSubstitutionMinimal);
            int i = 1;
            foreach (UnificationEntry entry in unifs)
            {
                if (Rippling.TestRipplingStep(new RipplingEntry(entry)))
                {
                    Console.WriteLine("Rippling#" + i + " out of " + unifs.Count);
                    Console.WriteLine(entry.GivenRoot.ToString());
                    Console.WriteLine("---");
                    Console.WriteLine(entry.GoalRoot.ToString());
                    Console.WriteLine("Proof found!");
                    result = true;
                    break;
                }
                else
                    Console.WriteLine("Failed");
                i++;
            }
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RipplingProofInstantVariablesTest2()
        {
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            bool result = false;
            IReadOnlyCollection<UnificationEntry> unifs = Unification.Annotate(new Expression(expr, true), new Expression(txt10, true), UnificationOption.CostAndSubstitutionMinimal);
            int i = 1;
            foreach (UnificationEntry entry in unifs)
            {
                if (Rippling.TestRipplingStep(new RipplingEntry(entry)))
                {
                    Console.WriteLine("Rippling#" + i + " out of " + unifs.Count);
                    Console.WriteLine(entry.GivenRoot.ToString());
                    Console.WriteLine("---");
                    Console.WriteLine(entry.GoalRoot.ToString());
                    Console.WriteLine("Proof found!");
                    result = true;
                    break;
                }
                else
                    Console.WriteLine("Failed");
                i++;
            }
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RipplingConveyorProofTest1()
        {
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            Console.WriteLine(new Expression(expr, false).Root.ToString());
            Assert.AreEqual(1.0, Rippling.PerformProof(new Expression(expr, false), new Expression(txt10, false), 1.0, ProofMode.Fast, StopConditions.LimitReached).Similarity);
        }

        [TestMethod]
        public void RipplingConveyorProofTest1Reversed()
        {
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            Assert.AreEqual(1.0, Rippling.PerformProof(new Expression(txt10, false), new Expression(expr, false), 1.0, ProofMode.Slow, StopConditions.LimitReached).Similarity);
        }

        [TestMethod]
        public void RipplingConveyorProofTest2()
        {
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            Assert.AreEqual(1.0, Rippling.PerformProof(new Expression(expr, false), new Expression(txt12, false), 1.0, ProofMode.Slow, StopConditions.LimitReached).Similarity);
        }

        [TestMethod]
        public void RipplingConveyorProofTest2Reversed()
        {
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            Assert.AreEqual(1.0, Rippling.PerformProof(new Expression(txt12, false), new Expression(expr, false), 1.0, ProofMode.Slow, StopConditions.LimitReached).Similarity);
        }

        [TestMethod]
        public void RipplingConveyorProofTest3()
        {
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            Assert.AreEqual(1.0, Rippling.PerformProof(new Expression(txt9, false), new Expression(txt10, false), 1.0, ProofMode.Fast, StopConditions.UnificationFinishOrFirstResultOrLimitReached).Similarity);
        }

        [TestMethod]
        public void RipplingConveyorProofTest3Reversed()
        {
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            ProofResult result = Rippling.PerformProof(new Expression(txt10, false), new Expression(txt9, false), 1.0, ProofMode.Fast, StopConditions.UnificationFinishOrFirstResultOrLimitReached);
            Assert.AreEqual(1.0, result.Similarity);
        }

        [TestMethod]
        public void RipplingProofTest1()
        {
            Console.WriteLine("Before: " + GC.GetTotalMemory(false) / (1024 * 1024));
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            bool result = false;
            IReadOnlyCollection<UnificationEntry> unifs = Unification.Annotate(new Expression(expr, false), new Expression(txt10, false), UnificationOption.SubstitutionMinimal);
            Console.WriteLine(unifs.Count);
            int i = 1;
            foreach (UnificationEntry entry in unifs)
            {
                if (Rippling.TestRipplingStep(new RipplingEntry(entry)))
                {
                    Console.WriteLine("Rippling#" + i + " out of " + unifs.Count);
                    Console.WriteLine(entry.GivenRoot.ToString());
                    Console.WriteLine("---");
                    Console.WriteLine(entry.GoalRoot.ToString());
                    Console.WriteLine("Proof found!");
                    result = true;
                    break;
                }
                else
                    Console.WriteLine("Failed");
                i++;
            }
            Assert.IsTrue(result);
            Console.WriteLine("After: " + GC.GetTotalMemory(false) / (1024 * 1024));
        }

        [TestMethod]
        public void RipplingProofForDiplomaTest1()
        {
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            DateTime before = DateTime.Now;
            bool result = false;
            IReadOnlyCollection<UnificationEntry> unifs = Unification.Annotate(new Expression(expr, true), new Expression(txt10,true), UnificationOption.SubstitutionMinimal);
            Console.WriteLine(unifs.Count);
            Console.WriteLine("unification after" + (DateTime.Now - before).TotalMilliseconds);
            int i = 1;
            RipplingController controller = new RipplingController(1.0);
            foreach (UnificationEntry entry in unifs)
            {
                RipplingEntry re = new RipplingEntry(entry, controller);
                Rippling.RipplingStep1(re);
                if (controller.IsFinished)
                {
                    Console.WriteLine("Rippling#" + i + " out of " + unifs.Count);
                    Console.WriteLine("Substitutions " + entry.SubstitutionCount + " " + entry.Substitutions.Count);
                    foreach (var subst in entry.Substitutions)
                        Console.WriteLine(subst.Key + " changed to " + subst.Value.Data);
                    Console.WriteLine(entry.GivenRoot.ToString());
                    Console.WriteLine("---");
                    Console.WriteLine(entry.GoalRoot.ToString());
                    Console.WriteLine("Proof found!");
                    Console.WriteLine("Proof:");
                    foreach (var r in controller.MaxEntry.AppliedRulesToGiven)
                    {
                        Console.WriteLine("ToGiven: ");
                        Console.WriteLine(r);
                    }
                    foreach (var r in controller.MaxEntry.AppliedRulesToGoal)
                    {
                        Console.WriteLine("ToGoal: ");
                        Console.WriteLine(r);
                    }

                    result = true;
                    break;
                }
                else
                    Console.WriteLine("Failed");
                i++;
            }
            Console.WriteLine("Finish after" + (DateTime.Now - before).TotalMilliseconds);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RipplingProofForDiplomaTest2()
        {
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            ProofResult res = Rippling.PerformProof(new Expression(expr, true), new Expression(txt10, true), 1.0, ProofMode.Fast, StopConditions.LimitReached);
            Assert.AreEqual(1.0, res.Similarity);
            foreach (var subst in res.Substitutions)
                Console.WriteLine(subst.Key + " changed to " + subst.Value);
            foreach (var r in res.RulesAppliedToLeft)
            {
                Console.WriteLine("ToGiven: ");
                Console.WriteLine(r);
            }
            foreach (var r in res.RulesAppliedToRight)
            {
                Console.WriteLine("ToGoal: ");
                Console.WriteLine(r);
            }
        }


        [TestMethod]
        public void RipplingProofTest1Reversed()
        {
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            bool result = false;
            IReadOnlyCollection<UnificationEntry> unifs = Unification.Annotate(new Expression(txt10, false), new Expression(expr, false), UnificationOption.SubstitutionMinimal);
            int i = 1;
            foreach (UnificationEntry entry in unifs)
            {
                if (Rippling.TestRipplingStep(new RipplingEntry(entry)))
                {
                    Console.WriteLine("Rippling#" + i + " out of " + unifs.Count);
                    Console.WriteLine(entry.GivenRoot.ToString());
                    Console.WriteLine("---");
                    Console.WriteLine(entry.GoalRoot.ToString());
                    Console.WriteLine("Proof found!");
                    result = true;
                    break;
                }
                else
                    Console.WriteLine("Failed");
                i++;
            }
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RipplingProofTest2()
        {
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            bool result = false;
            IReadOnlyCollection<UnificationEntry> unifs = Unification.Annotate(new Expression(txt9, false), new Expression(txt10, false), UnificationOption.SubstitutionMinimal);
            int i = 1;
            foreach (UnificationEntry entry in unifs)
            {
                if (Rippling.TestRipplingStep(new RipplingEntry(entry)))
                {
                    Console.WriteLine("Rippling#" + i + " out of " + unifs.Count);
                    Console.WriteLine(entry.GivenRoot.ToString());
                    Console.WriteLine("---");
                    Console.WriteLine(entry.GoalRoot.ToString());
                    Console.WriteLine("Proof found!");
                    result = true;
                    break;
                }
                else
                    Console.WriteLine("Failed");
                i++;
            }
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RipplingProofTest2Reversed()
        {
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            bool result = false;
            IReadOnlyCollection<UnificationEntry> unifs = Unification.Annotate(new Expression(txt10, false), new Expression(txt9, false), UnificationOption.SubstitutionMinimal);
            int i = 1;
            foreach (UnificationEntry entry in unifs)
            {
                if (Rippling.TestRipplingStep(new RipplingEntry(entry)))
                {
                    Console.WriteLine("Rippling#" + i + " out of " + unifs.Count);
                    Console.WriteLine(entry.GivenRoot.ToString());
                    Console.WriteLine("---");
                    Console.WriteLine(entry.GoalRoot.ToString());
                    Console.WriteLine("Proof found!");
                    result = true;
                    break;
                }
                else
                    Console.WriteLine("Failed");
                i++;
            }
            Assert.IsTrue(result);
        }


        [TestMethod]
        public void RipplingProofTest3()
        {
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            bool result = false;
            IReadOnlyCollection<UnificationEntry> unifs = Unification.Annotate(new Expression(txt10, false), new Expression(txt12, false), UnificationOption.CostAndSubstitutionMinimal);
            int i = 1;
            foreach (UnificationEntry entry in unifs)
            {
                if (Rippling.TestRipplingStep(new RipplingEntry(entry)))
                {
                    Console.WriteLine("Rippling#" + i + " out of " + unifs.Count);
                    Console.WriteLine(entry.GivenRoot.ToString());
                    Console.WriteLine("---");
                    Console.WriteLine(entry.GoalRoot.ToString());
                    Console.WriteLine("Proof found!");
                    result = true;
                    break;
                }
                else
                    Console.WriteLine("Failed");
                i++;
            }
            Assert.IsTrue(result);
        }


        [TestMethod]
        public void RipplingProofTest3Reversed()
        {
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            bool result = false;
            IReadOnlyCollection<UnificationEntry> unifs = Unification.Annotate(new Expression(txt10, false), new Expression(txt12, false), UnificationOption.CostAndSubstitutionMinimal);
            int i = 1;
            foreach (UnificationEntry entry in unifs)
            {
                if (Rippling.TestRipplingStep(new RipplingEntry(entry)))
                {
                    Console.WriteLine("Rippling#" + i + " out of " + unifs.Count);
                    Console.WriteLine(entry.GivenRoot.ToString());
                    Console.WriteLine("---");
                    Console.WriteLine(entry.GoalRoot.ToString());
                    Console.WriteLine("Proof found!");
                    result = true;
                    break;
                }
                else
                    Console.WriteLine("Failed");
                i++;
            }
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RipplingProofTest4()
        {
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            bool result = false;
            IReadOnlyCollection<UnificationEntry> unifs = Unification.Annotate(new Expression(expr, false), new Expression(txt12, false), UnificationOption.SubstitutionMinimal);
            int i = 1;
            foreach (UnificationEntry entry in unifs)
            {
                if (Rippling.TestRipplingStep(new RipplingEntry(entry)))
                {
                    Console.WriteLine("Rippling#" + i + " out of " + unifs.Count);
                    Console.WriteLine(entry.GivenRoot.ToString());
                    Console.WriteLine("---");
                    Console.WriteLine(entry.GoalRoot.ToString());
                    Console.WriteLine("Proof found!");
                    result = true;
                    break;
                }
                else
                    Console.WriteLine("Failed");
                i++;
            }
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RipplingProofTest4Reversed()
        {
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            bool result = false;
            IReadOnlyCollection<UnificationEntry> unifs = Unification.Annotate(new Expression(txt12, false), new Expression(expr, false), UnificationOption.SubstitutionMinimal);
            int i = 1;
            foreach (UnificationEntry entry in unifs)
            {
                if (Rippling.TestRipplingStep(new RipplingEntry(entry)))
                {
                    Console.WriteLine("Rippling#" + i + " out of " + unifs.Count);
                    Console.WriteLine(entry.GivenRoot.ToString());
                    Console.WriteLine("---");
                    Console.WriteLine(entry.GoalRoot.ToString());
                    Console.WriteLine("Proof found!");
                    result = true;
                    break;
                }
                else
                    Console.WriteLine("Failed");
                i++;
            }
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RipplingDistribProofTest1()
        {
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            bool result = false;
            IReadOnlyCollection<UnificationEntry> unifs = Unification.Annotate(new Expression(distrib1), new Expression(distrib2), UnificationOption.NoRestrictions);
            int i = 1;
            foreach (UnificationEntry entry in unifs)
            {
                if (Rippling.TestRipplingStep(new RipplingEntry(entry)))
                {
                    Console.WriteLine("Rippling#" + i + " out of " + unifs.Count);
                    Console.WriteLine(entry.GivenRoot.ToString());
                    Console.WriteLine("---");
                    Console.WriteLine(entry.GoalRoot.ToString());
                    Console.WriteLine("Proof found!");
                    result = true;
                    break;
                }
                else
                    Console.WriteLine("Failed");
                i++;
            }
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RipplingDistribProofTest2()
        {
            Assert.IsTrue(Rippling.LoadRules("rules") > 0);
            bool result = false;
            IReadOnlyCollection<UnificationEntry> unifs = Unification.Annotate(new Expression(distrib2), new Expression(distrib1), UnificationOption.NoRestrictions);
            int i = 1;
            foreach (UnificationEntry entry in unifs)
            {
                if (Rippling.TestRipplingStep(new RipplingEntry(entry)))
                {
                    Console.WriteLine("Rippling#" + i + " out of " + unifs.Count);
                    Console.WriteLine(entry.GivenRoot.ToString());
                    Console.WriteLine("---");
                    Console.WriteLine(entry.GoalRoot.ToString());
                    Console.WriteLine("Proof found!");
                    result = true;
                    break;
                }
                else
                    Console.WriteLine("Failed");
                i++;
            }
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void FuzzyJaccardMeasureTest1()
        {
            BinaryTree root1 = new BinaryTree(new Function("root"));
            root1.Add(new Function("node"));
            root1.Left.Add(new Variable("A"));
            root1.Left.Add(new Variable("B"));
            root1.Add(new Function("node"));
            root1.Right.Add(new Variable("C"));
            root1.Right.Add(new Function("node"));
            root1.Right.Right.Add(new Variable("D"));
            root1.Right.Right.Add(new Variable("E"));
            Assert.AreEqual(5, root1.GetAllLeafNodes().Count);
            Console.WriteLine(root1);
            Assert.AreEqual(root1, Rippling.getLeastCommonAncestor(root1.Left.Left, root1.Right.Right.Right));
            Assert.AreEqual(root1.Left, Rippling.getLeastCommonAncestor(root1.Left.Left, root1.Left.Right));
            Assert.AreEqual(root1.Right, Rippling.getLeastCommonAncestor(root1.Right.Left, root1.Right.Right.Right));

            BinaryTree root2 = new BinaryTree(new Function("root"));
            root2.Add(new Function("node"));
            root2.Left.Add(new Variable("A"));
            root2.Left.Add(new Function("node"));
            root2.Left.Right.Add(new Variable("C"));
            root2.Left.Right.Add(new Variable("D"));
            root2.Add(new Function("node"));
            root2.Right.Add(new Variable("E"));
            root2.Right.Add(new Function("node"));
            root2.Right.Right.Add(new Variable("F"));
            root2.Right.Right.Add(new Variable("B"));
            Assert.AreEqual(6, root2.GetAllLeafNodes().Count);

            Console.WriteLine(root2);
            Dictionary<BinaryTree, double> weightMatrix1 = new Dictionary<BinaryTree, double>();
            weightMatrix1.Add(root1, 0);
            weightMatrix1.Add(root1.DeepCopy(), 0);
            Assert.AreEqual(0.395, Rippling.FuzzyJaccardSimilarity(root1, root2, 1.0, false), 0.001);
        }

        [TestMethod]
        public void FuzzyJaccardMeasureTest2()
        {
            BinaryTree root1 = new BinaryTree(new Function("root"));
            root1.Add(new Function("node1"));
            root1.Left.Add(new Variable("A"));
            root1.Left.Add(new Variable("B"));
            root1.Add(new Function("node1"));
            root1.Right.Add(new Variable("C"));
            root1.Right.Add(new Function("node1"));
            root1.Right.Right.Add(new Variable("D"));
            root1.Right.Right.Add(new Variable("E"));
            Assert.AreEqual(5, root1.GetAllLeafNodes().Count);
            Console.WriteLine(root1);
            Assert.AreEqual(root1, Rippling.getLeastCommonAncestor(root1.Left.Left, root1.Right.Right.Right));
            Assert.AreEqual(root1.Left, Rippling.getLeastCommonAncestor(root1.Left.Left, root1.Left.Right));
            Assert.AreEqual(root1.Right, Rippling.getLeastCommonAncestor(root1.Right.Left, root1.Right.Right.Right));

            BinaryTree root2 = new BinaryTree(new Function("root"));
            root2.Add(new Function("node2"));
            root2.Left.Add(new Variable("A"));
            root2.Left.Add(new Function("node2"));
            root2.Left.Right.Add(new Variable("C"));
            root2.Left.Right.Add(new Variable("D"));
            root2.Add(new Function("node2"));
            root2.Right.Add(new Variable("E"));
            root2.Right.Add(new Function("node2"));
            root2.Right.Right.Add(new Variable("F"));
            root2.Right.Right.Add(new Variable("B"));
            Assert.AreEqual(6, root2.GetAllLeafNodes().Count);
            Console.WriteLine(root2);
            Assert.AreEqual(0.395, Rippling.FuzzyJaccardSimilarity(root1, root2, 1.0, false), 0.001);
        }

        [TestMethod]
        public void FuzzyJaccardMeasureTest3()
        {
            Expression e1 = new Expression(txt10, true);
            Expression e2 = new Expression(txt9, true);
            Assert.AreEqual(1, Rippling.FuzzyJaccardSimilarity(e1.Root, e1.Root, 1.0, true), 0.001);
            Assert.IsTrue(Rippling.FuzzyJaccardSimilarity(e1.Root, e2.Root, 0.5, true) < Rippling.FuzzyJaccardSimilarity(e1.Root, e2.Root, 1.0, true));
            Assert.IsTrue(Rippling.FuzzyJaccardSimilarity(e1.Root, e2.Root, 0.1, true) < Rippling.FuzzyJaccardSimilarity(e1.Root, e2.Root, 0.5, true));
        }
    }
}
