﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Semawed;
namespace UnitTest
{
    [TestClass]
    public class WaveRuleTest
    {
        string path = "rules";
        [TestMethod]
        public void CtorsAndSerializationTest()
        {
            Rippling.LoadRules(path);
        }

        [TestMethod]
        public void WaveRuleCreation()
        {
            
            try
            {
                Directory.Delete(path, true);
            }
            catch { }
            Directory.CreateDirectory(path);
            int a = WaveRule.CreateWaveRules("WR/0divide1.txt", "WR/0divide2.txt", "divide1", path);
            int b = WaveRule.CreateWaveRules("WR/0divide2.txt", "WR/0divide1.txt", "divide2", path);
            Console.WriteLine(a + " " + b);
            Assert.AreEqual(WaveRule.CreateWaveRules("WR/1sumcommutativity1.txt", "WR/1sumcommutativity2.txt", "sumcommut1", path), 0);
            Assert.AreEqual(WaveRule.CreateWaveRules("WR/1sumcommutativity2.txt", "WR/1sumcommutativity1.txt", "sumcommut2", path), 0);
            Assert.IsTrue(WaveRule.CreateWaveRules("WR/2sumassoc1.txt", "WR/2sumassoc2.txt", "sumassoc1", path) > 0);
            Assert.IsTrue(WaveRule.CreateWaveRules("WR/2sumassoc2.txt", "WR/2sumassoc1.txt", "sumassoc2", path) > 0);
            WaveRule.CreateWaveRules("WR/3zero1.txt", "WR/3zero2.txt", "zero1", path);
            WaveRule.CreateWaveRules("WR/3zero2.txt", "WR/3zero1.txt", "zero2", path);
            WaveRule.CreateWaveRules("WR/4suminverse1.txt", "WR/4suminverse2.txt", "suminverse1", path);
            WaveRule.CreateWaveRules("WR/4suminverse2.txt", "WR/4suminverse1.txt", "suminverse2", path);
            WaveRule.CreateWaveRules("WR/5mulcommutativity1.txt", "WR/5mulcommutativity2.txt", "mulcommut1", path);
            WaveRule.CreateWaveRules("WR/5mulcommutativity2.txt", "WR/5mulcommutativity1.txt", "mulcommut2", path);
            WaveRule.CreateWaveRules("WR/6mulassoc1.txt", "WR/6mulassoc2.txt", "mulassoc1", path);
            WaveRule.CreateWaveRules("WR/6mulassoc2.txt", "WR/6mulassoc1.txt", "mulassoc2", path);
            WaveRule.CreateWaveRules("WR/7one1.txt", "WR/7one2.txt", "one1", path);
            WaveRule.CreateWaveRules("WR/7one2.txt", "WR/7one1.txt", "one2", path);
            WaveRule.CreateWaveRules("WR/8mulinverse1.txt", "WR/8mulinverse2.txt", "mulinverse1", path);
            WaveRule.CreateWaveRules("WR/8mulinverse2.txt", "WR/8mulinverse1.txt", "mulinverse2", path);
            WaveRule.CreateWaveRules("WR/9distrib1.txt", "WR/9distrib2.txt", "distrib1", path);
            WaveRule.CreateWaveRules("WR/9distrib2.txt", "WR/9distrib1.txt", "distrib2", path);
            WaveRule.CreateWaveRules("WR/10distribL1.txt", "WR/10distribL2.txt", "distribL1", path);
            WaveRule.CreateWaveRules("WR/10distribL2.txt", "WR/10distribL1.txt", "distribL2", path);
            WaveRule.CreateWaveRules("WR/11eq1.txt", "WR/11eqL2.txt", "eqL1", path);
            WaveRule.CreateWaveRules("WR/11eqL2.txt", "WR/11eq1.txt", "eqL2", path);
            WaveRule.CreateWaveRules("WR/11eq1.txt", "WR/11eqR2.txt", "eqR1", path);
            WaveRule.CreateWaveRules("WR/11eqR2.txt", "WR/11eq1.txt", "eqR2", path);
            WaveRule.CreateWaveRules("WR/12eqzero1.txt", "WR/12eqzero2.txt", "eqzero1", path);
            WaveRule.CreateWaveRules("WR/12eqzero2.txt", "WR/12eqzero1.txt", "eqzero2", path);
            WaveRule.CreateWaveRules("WR/13eqsum1.txt", "WR/13eqsumL2.txt", "eqsumL1", path);
            WaveRule.CreateWaveRules("WR/13eqsumL2.txt", "WR/13eqsum1.txt", "eqsumL2", path);
            WaveRule.CreateWaveRules("WR/13eqsum1.txt", "WR/13eqsumR2.txt", "eqsumR1", path);
            WaveRule.CreateWaveRules("WR/13eqsumR2.txt", "WR/13eqsum1.txt", "eqsumR2", path);
            WaveRule.CreateWaveRules("WR/14eqtimes1.txt", "WR/14eqtimesL2.txt", "eqtimesL1", path);
            WaveRule.CreateWaveRules("WR/14eqtimesL2.txt", "WR/14eqtimes1.txt", "eqtimesL2", path);
            WaveRule.CreateWaveRules("WR/14eqtimes1.txt", "WR/14eqtimesR2.txt", "eqtimesR1", path);
            WaveRule.CreateWaveRules("WR/14eqtimesR2.txt", "WR/14eqtimes1.txt", "eqtimesR2", path);
            WaveRule.CreateWaveRules("WR/15powertimes1.txt", "WR/15powertimes2.txt", "powertimes1", path);
            WaveRule.CreateWaveRules("WR/15powertimes2.txt", "WR/15powertimes1.txt", "powertimes2", path);
            WaveRule.CreateWaveRules("WR/16powerdivide1.txt", "WR/16powerdivide2.txt", "powerdivide1", path);
            WaveRule.CreateWaveRules("WR/16powerdivide2.txt", "WR/16powerdivide1.txt", "powerdivide2", path);
            WaveRule.CreateWaveRules("WR/17trig1.txt", "WR/17trig2.txt", "trig1", path);
            WaveRule.CreateWaveRules("WR/17trig2.txt", "WR/17trig1.txt", "trig2", path);
            WaveRule.CreateWaveRules("WR/18tan1.txt", "WR/18tan2.txt", "tan1", path);
            WaveRule.CreateWaveRules("WR/18tan2.txt", "WR/18tan1.txt", "tan2", path);
            WaveRule.CreateWaveRules("WR/19cotan1.txt", "WR/19cotan2.txt", "cotan1", path);
            WaveRule.CreateWaveRules("WR/19cotan2.txt", "WR/19cotan1.txt", "cotan2", path);
            WaveRule.CreateWaveRules("WR/20sinsum1.txt", "WR/20sinsum2.txt", "sinsum1", path);
            WaveRule.CreateWaveRules("WR/20sinsum2.txt", "WR/20sinsum1.txt", "sinsum2", path);
            WaveRule.CreateWaveRules("WR/21sinminus1.txt", "WR/21sinminus2.txt", "sinminus1", path);
            WaveRule.CreateWaveRules("WR/21sinminus2.txt", "WR/21sinminus1.txt", "sinminus2", path);
            WaveRule.CreateWaveRules("WR/22cossum1.txt", "WR/22cossum2.txt", "cossum1", path);
            WaveRule.CreateWaveRules("WR/22cossum2.txt", "WR/22cossum1.txt", "cossum2", path);
            WaveRule.CreateWaveRules("WR/23cosminus1.txt", "WR/23cosminus2.txt", "cosminus1", path);
            WaveRule.CreateWaveRules("WR/23cosminus2.txt", "WR/23cosminus1.txt", "cosminus2", path);
            WaveRule.CreateWaveRules("WR/24sinoddity1.txt", "WR/24sinoddity2.txt", "sinoddity1", path);
            WaveRule.CreateWaveRules("WR/24sinoddity2.txt", "WR/24sinoddity1.txt", "sinoddity2", path);
            WaveRule.CreateWaveRules("WR/25cosoddity1.txt", "WR/25cosoddity2.txt", "cosoddity1", path);
            WaveRule.CreateWaveRules("WR/25cosoddity2.txt", "WR/25cosoddity1.txt", "cosoddity2", path);
            WaveRule.CreateWaveRules("WR/26tanoddity1.txt", "WR/26tanoddity2.txt", "tanoddity1", path);
            WaveRule.CreateWaveRules("WR/26tanoddity2.txt", "WR/26tanoddity1.txt", "tanoddity2", path);
            WaveRule.CreateWaveRules("WR/27cotanoddity1.txt", "WR/27cotanoddity2.txt", "cotanoddity1", path);
            WaveRule.CreateWaveRules("WR/27cotanoddity2.txt", "WR/27cotanoddity1.txt", "cotanoddity2", path);
            WaveRule.CreateWaveRules("WR/28log1.txt", "WR/28log2.txt", "log1", path);
            WaveRule.CreateWaveRules("WR/28log2.txt", "WR/28log1.txt", "log2", path);
            WaveRule.CreateWaveRules("WR/29logtimes1.txt", "WR/29logtimes2.txt", "logtimes1", path);
            WaveRule.CreateWaveRules("WR/29logtimes2.txt", "WR/29logtimes1.txt", "logtimes2", path);
            WaveRule.CreateWaveRules("WR/30logdivide1.txt", "WR/30logdivide2.txt", "logdivide1", path);
            WaveRule.CreateWaveRules("WR/30logdivide2.txt", "WR/30logdivide1.txt", "logdivide2", path);
            WaveRule.CreateWaveRules("WR/31logpower1.txt", "WR/31logpower2.txt", "logpower1", path);
            WaveRule.CreateWaveRules("WR/31logpower2.txt", "WR/31logpower1.txt", "logpower2", path);
            WaveRule.CreateWaveRules("WR/32ln1.txt", "WR/32ln2.txt", "ln1", path);
            WaveRule.CreateWaveRules("WR/32ln2.txt", "WR/32ln1.txt", "ln2", path);
            WaveRule.CreateWaveRules("WR/33lntimes1.txt", "WR/33lntimes2.txt", "lntimes1", path);
            WaveRule.CreateWaveRules("WR/33lntimes2.txt", "WR/33lntimes1.txt", "lntimes2", path);
            WaveRule.CreateWaveRules("WR/34lndivide1.txt", "WR/34lndivide2.txt", "lndivide1", path);
            WaveRule.CreateWaveRules("WR/34lndivide2.txt", "WR/34lndivide1.txt", "lndivide2", path);
            WaveRule.CreateWaveRules("WR/35lnpower1.txt", "WR/35lnpower2.txt", "lnpower1", path);
            WaveRule.CreateWaveRules("WR/35lnpower2.txt", "WR/35lnpower1.txt", "lnpower2", path);
            WaveRule.CreateWaveRules("WR/36minusminus1.txt", "WR/36minusminus2.txt", "minusminus1", path);
            WaveRule.CreateWaveRules("WR/36minusminus2.txt", "WR/36minusminus1.txt", "minusminus2", path);
            //LOGBASE
        }

        [TestMethod]
        public void ApplyTestSumAssociativity()
        {
            BinaryTree rPart = new BinaryTree(new Function("plus", MarkupType.WaveFront));
            rPart.Add(new Function("plus", MarkupType.Skeleton));
            rPart.Left.Add(new Variable("a", MarkupType.Skeleton));
            rPart.Left.Add(new Variable("b", MarkupType.Skeleton));
            rPart.Add(new Variable("c", MarkupType.WaveFront));

            BinaryTree lPart = new BinaryTree(new Function("plus", MarkupType.Skeleton));
            lPart.Add(new Variable("a", MarkupType.Skeleton));
            lPart.Add(new Function("plus", MarkupType.WaveFront));
            lPart.Right.Add(new Variable("b", MarkupType.Skeleton));
            lPart.Right.Add(new Variable("c", MarkupType.WaveFront));
            WaveRule wr = new WaveRule(lPart, rPart, "description", "lalal", "lalal_reversed");
            WaveRuleApplyHelper helper = new WaveRuleApplyHelper(wr);

            BinaryTree t1 = new BinaryTree(new Function("plus", MarkupType.Skeleton));
            t1.Add(new Function("times", MarkupType.Skeleton));
            t1.Left.Add(new Variable("x", MarkupType.Skeleton));
            t1.Left.Add(new Variable("y", MarkupType.Skeleton));
            t1.Add(new Function("plus", MarkupType.WaveFront));
            t1.Right.Add(new Function("times", MarkupType.Skeleton));
            t1.Right.Left.Add(new Variable("z", MarkupType.Skeleton));
            t1.Right.Left.Add(new Variable("z", MarkupType.Skeleton));
            t1.Right.Add(new Function("times", MarkupType.WaveFront));
            t1.Right.Right.Add(new Variable("w", MarkupType.WaveFront));
            t1.Right.Right.Add(new Variable("w", MarkupType.WaveFront));

            Assert.IsTrue(helper.CanApply(t1));
            string appliedRule;
            Assert.IsTrue(helper.Apply(t1, out appliedRule));
            //Assert.AreEqual("((x times y) plus ((z times z) plus (w times w))) --> (((x times y) plus (z times z)) plus (w times w))", appliedRule);
            BinaryTree t2 = new BinaryTree(new Function("plus", MarkupType.Skeleton));
            t2.Add(new Function("plus", MarkupType.WaveFront));
            t2.Left.Add(new Function("times", MarkupType.Skeleton));
            t2.Left.Left.Add(new Variable("x", MarkupType.Skeleton));
            t2.Left.Left.Add(new Variable("y", MarkupType.Skeleton));
            t2.Left.Add(new Function("times", MarkupType.WaveFront));
            t2.Left.Right.Add(new Variable("z", MarkupType.WaveFront));
            t2.Left.Right.Add(new Variable("z", MarkupType.WaveFront));
            t2.Add(new Function("times", MarkupType.Skeleton));
            t2.Right.Add(new Variable("w", MarkupType.Skeleton));
            t2.Right.Add(new Variable("w", MarkupType.Skeleton));
            Assert.IsTrue(t1 == t2);
        }

        [TestMethod]
        public void ApplyTestSimpleProof()
        {
            //associativity
            //BinaryTree rPart2 = new BinaryTree(new Function("plus", MarkupType.WaveFront));
            //rPart2.Add(new Function("plus", MarkupType.Skeleton));
            //rPart2.Left.Add(new Variable("a", MarkupType.Skeleton));
            //rPart2.Left.Add(new Variable("b", MarkupType.Skeleton));
            //rPart2.Add(new Variable("c", MarkupType.WaveFront));

            //BinaryTree lPart2 = new BinaryTree(new Function("plus", MarkupType.Skeleton));
            //lPart2.Add(new Variable("a", MarkupType.Skeleton));
            //lPart2.Add(new Function("plus", MarkupType.WaveFront));
            //lPart2.Right.Add(new Variable("b", MarkupType.Skeleton));
            //lPart2.Right.Add(new Variable("c", MarkupType.WaveFront));
            //WaveRule wr2 = new WaveRule(lPart2, rPart2, "lalal");
            //WaveRuleApplyHelper helper2 = new WaveRuleApplyHelper(wr2);

            //induction
            BinaryTree rPart = new BinaryTree(new Function("s", MarkupType.WaveFront));
            rPart.Add(new Function("plus", MarkupType.Skeleton));
            rPart.Left.Add(new Variable("a", MarkupType.Skeleton));
            rPart.Left.Add(new Variable("b", MarkupType.Skeleton));

            BinaryTree lPart = new BinaryTree(new Function("plus", MarkupType.Skeleton));
            lPart.Add(new Function("s", MarkupType.WaveFront));
            lPart.Left.Add(new Variable("a", MarkupType.Skeleton));
            lPart.Add(new Variable("b", MarkupType.Skeleton));

            WaveRule wr = new WaveRule(lPart, rPart, "description", "lalal", "lalal_reversed");
            WaveRuleApplyHelper helper = new WaveRuleApplyHelper(wr);

            BinaryTree t1 = new BinaryTree(new Function("plus", MarkupType.Skeleton));
            t1.Add(new Function("s", MarkupType.WaveFront));
            t1.Left.Add(new Variable("x", MarkupType.Skeleton));
            t1.Add(new Function("plus", MarkupType.Skeleton));
            t1.Right.Add(new Variable("y", MarkupType.Skeleton));
            t1.Right.Add(new Variable("z", MarkupType.Skeleton));

            BinaryTree t2 = new BinaryTree(new Function("plus", MarkupType.Skeleton));
            t2.Add(new Function("plus", MarkupType.Skeleton));
            t2.Left.Add(new Function("s", MarkupType.WaveFront));
            t2.Left.Left.Add(new Variable("x", MarkupType.Skeleton));
            t2.Left.Add(new Variable("y", MarkupType.Skeleton));
            t2.Add(new Variable("z", MarkupType.Skeleton));
            string appliedRule1;
            Assert.IsTrue(helper.Apply(t1, out appliedRule1));
            string appliedRule2;
            Assert.IsFalse(helper.Apply(t1, out appliedRule2));
            string appliedRule3;
            Assert.IsFalse(helper.Apply(t2, out appliedRule3));
            string appliedRule4;
            Assert.IsTrue(helper.Apply(t2.Left, out appliedRule4));
            string appliedRule5;
            Assert.IsTrue(helper.Apply(t2, out appliedRule5));
            Assert.IsTrue(t1.Root.Data == t2.Root.Data);
        }
        //[TestMethod]
        //public void RulesApplyingTest()
        //{
        //    Rippling.LoadRules("C:\\Users\\adm\\repos\\sempomat\\mathSearchWeb\\RuleCreator\\bin\\Debug\\rules");
        //    Expression ex1 = new Expression(txt9, "uniq1_");
        //    Expression ex2 = new Expression(txt10, "uniq2_");
        //    IReadOnlyList<UnificationEntry> unifs = Unification.Annotate(ex1, ex2, UnificationOption.SubstitutionMinimal);
        //    int counter = 0;
        //    Console.WriteLine(unifs.Count);
        //    int avgSkeletonCountBeforeApply = 0;
        //    int avgSkeletonCountAfterApply = 0;
        //    int avgWFCountAfterApply = 0;
        //    int avgWFCountBeforeAPply = 0;
        //    int secondApply = 0;
        //    int candidatesCount = 0;
        //    int secondCandidatesCount = 0;
        //    int successfullApplysCounter = 0;
        //    int messagesCount = 0;
        //    int noSkeletonCount = 0;
        //    int unificationApplyCheckCount = 0;
        //    int similiarCount = 0;
        //    int maxSkeletonCount = 0;
        //    int unificationsCount = 0;
        //    foreach (UnificationEntry e in unifs)
        //    {
        //        bool ruleApplied = false;
        //        try
        //        {
        //            new Skeleton(e.GoalRoot.DeepCopy());
        //            new Skeleton(e.GivenRoot.DeepCopy());
        //        }
        //        catch (NotSupportedException)
        //        {
        //            noSkeletonCount++;
        //        }

        //        foreach (WaveRule WR in Rippling.Rules)
        //        {
        //            WaveRuleApplyHelper applier = new WaveRuleApplyHelper(WR);
        //            try
        //            {
        //                List<BinaryTree> candidates = new List<BinaryTree>();
        //                e.GivenRoot.DeepCopy().FindAll(WR.leftPart.Data, candidates);
        //                e.GoalRoot.DeepCopy().FindAll(WR.leftPart.Data, candidates);
        //                candidatesCount += candidates.Count;
        //                foreach (BinaryTree t in candidates)
        //                    if (applier.isAppliable(t))
        //                    {
        //                        counter++;
        //                        BinaryTree tCopy = t.DeepCopy();
        //                        if (applier.applyRule(tCopy))
        //                        {
        //                            if (new Skeleton(tCopy) == new Skeleton(t.DeepCopy()))
        //                            {
        //                                avgSkeletonCountBeforeApply += t.GetTreeInfo().SkeletonCount;
        //                                avgSkeletonCountAfterApply += tCopy.GetTreeInfo().SkeletonCount;
        //                                avgWFCountAfterApply += tCopy.GetTreeInfo().WFCount;
        //                                avgWFCountBeforeAPply += t.GetTreeInfo().WFCount;
        //                                if (tCopy.GetTreeInfo().SkeletonCount > maxSkeletonCount)
        //                                    maxSkeletonCount = tCopy.GetTreeInfo().SkeletonCount;
        //                                successfullApplysCounter++;
        //                                if (ruleApplied == false)
        //                                {
        //                                    ruleApplied = true;
        //                                    unificationsCount++;
        //                                }
        //                                BinaryTree entryT = t.DeepCopy();
        //                                UnificationEntry entry = new UnificationEntry(entryT, t.DeepCopy());
        //                                if (entry.ApplyRuleToGiven(WR))
        //                                {
        //                                    unificationApplyCheckCount++;
        //                                    if (entry.GivenRoot == tCopy)
        //                                        similiarCount++;
        //                                    else
        //                                    {
        //                                        Console.WriteLine("ENTRY: " + entry.GivenRoot.ToString());
        //                                        Console.WriteLine("T: " + tCopy.ToString());
        //                                        return;
        //                                    }
        //                                }

        //                                foreach (WaveRule waverule in Rippling.Rules)
        //                                {
        //                                    List<BinaryTree> secondcandidates = new List<BinaryTree>();
        //                                    tCopy.FindAll(waverule.leftPart.Data, secondcandidates);
        //                                    secondCandidatesCount += secondcandidates.Count;
        //                                    foreach (BinaryTree tSecond in secondcandidates)
        //                                        if (new WaveRuleApplyHelper(waverule).isAppliable(tSecond))
        //                                            secondApply++;
        //                                }
        //                            }
        //                            else
        //                                noSkeletonCount++;
        //                        }
        //                        else
        //                        {
        //                            if (messagesCount < 5)
        //                            {
        //                                Console.WriteLine("Message#" + messagesCount);
        //                                Console.WriteLine("Rule: " + WR.description);
        //                                Console.WriteLine(WR.ToString());
        //                                Console.WriteLine("before: " + t.ToString());
        //                                Console.WriteLine("after: " + tCopy.ToString());
        //                                Console.WriteLine("Skeleton preservance: " + (new Skeleton(tCopy) == new Skeleton(t)));
        //                            }
        //                            messagesCount++;
        //                        }
        //                    }
        //            }
        //            catch (NotSupportedException)
        //            {
        //                noSkeletonCount++;
        //            }
        //            catch { }
        //        }
        //    }
        //    Console.WriteLine("No skeleton: " + noSkeletonCount);
        //    Console.WriteLine("Candidates count: " + candidatesCount);
        //    Console.WriteLine("Appliable: " + counter);
        //    Console.WriteLine("Useful unifications: " + unificationsCount);
        //    Console.WriteLine("Successfull applies: " + successfullApplysCounter + " second apply: " + secondApply + " second candidates: " + secondCandidatesCount);
        //    Console.WriteLine("Avg skeleton count after apply: " + avgSkeletonCountAfterApply / (double)successfullApplysCounter);
        //    Console.WriteLine("Avg skeleton count before apply: " + avgSkeletonCountBeforeApply / (double)successfullApplysCounter);
        //    Console.WriteLine("Max skeleton: " + maxSkeletonCount);
        //    Console.WriteLine("Avg WF count after apply: " + avgWFCountAfterApply / (double)successfullApplysCounter);
        //    Console.WriteLine("Avg WF count before apply: " + avgWFCountBeforeAPply / (double)successfullApplysCounter);
        //    Console.WriteLine("Unification entry successfull applies: " + unificationApplyCheckCount + " and same result have " + similiarCount);
        //    Assert.IsTrue(counter > 0);
        //}
    }
}
