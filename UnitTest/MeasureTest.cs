﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Semawed;

namespace UnitTest
{
    [TestClass]
    public class MeasureTest
    {
        [TestMethod]
        public void MeasureConstructorTest()
        {
            BinaryTree t1 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.WaveFront));
            t1.Left.Add(new Variable("a", MarkupType.Skeleton));
            t1.Left.Add(new Variable("b", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.Skeleton));
            t1.Right.Add(new Variable("c", MarkupType.Skeleton));
            t1.Right.Add(new Variable("d", MarkupType.Skeleton));
            Measure m = new Measure(t1);
            Assert.AreEqual(m.ToString(), "0 3 ");

            BinaryTree t2 = new BinaryTree(new Function("p", MarkupType.Skeleton));
            t2.Add(new Function("h", MarkupType.WaveFront));
            t2.Left.Add(new Function("g", MarkupType.WaveFront));
            t2.Left.Left.Add(new Function("f", MarkupType.Skeleton));
            t2.Left.Left.Left.Add(new Variable("c", MarkupType.Skeleton));
            Measure m2 = new Measure(t2);
            Assert.AreEqual("0 2 0 ", m2.ToString());

            BinaryTree t3 = new BinaryTree(new Function("p", MarkupType.Skeleton));
            t3.Add(new Function("h", MarkupType.WaveFront));
            t3.Left.Add(new Function("g", MarkupType.Skeleton));
            t3.Left.Left.Add(new Function("f", MarkupType.Skeleton));
            t3.Left.Left.Left.Add(new Variable("c", MarkupType.Skeleton));
            Measure m3 = new Measure(t3);
            Assert.AreEqual("0 0 1 0 ", m3.ToString());

            BinaryTree t4 = new BinaryTree(new Function("rem", MarkupType.Skeleton));
            t4.Add(new Function("minus", MarkupType.WaveFront));
            t4.Add(new Function("s", MarkupType.Skeleton));
            t4.Right.Add(new Variable("n", MarkupType.Skeleton));
            t4.Left.Add(new Variable("m", MarkupType.Skeleton));
            t4.Left.Add(new Function("s", MarkupType.WaveFront));
            t4.Left.Right.Add(new Variable("n", MarkupType.WaveFront));
            Measure m4 = new Measure(t4);
            Assert.AreEqual("0 3 0 ", m4.ToString());

            BinaryTree oldGiven = new BinaryTree(new Function("plus", MarkupType.Skeleton));
            oldGiven.Add(new Function("times", MarkupType.WaveFront));
            oldGiven.Left.Add(new Variable("a", MarkupType.WaveFront));
            oldGiven.Left.Add(new Variable("a", MarkupType.Skeleton));
            oldGiven.Add(new Function("plus", MarkupType.WaveFront));
            oldGiven.Right.Add(new Function("times", MarkupType.WaveFront));
            oldGiven.Right.Add(new Function("times", MarkupType.WaveFront));
            oldGiven.Right.Left.Add(new Variable("a", MarkupType.WaveFront));
            oldGiven.Right.Left.Add(new Variable("b", MarkupType.WaveFront));
            oldGiven.Right.Right.Add(new Variable("a", MarkupType.WaveFront));
            oldGiven.Right.Right.Add(new Variable("c", MarkupType.WaveFront));
            Measure m5 = new Measure(oldGiven);
            Console.WriteLine(m5.ToString());
            //Assert.AreEqual("5 4 ", m5.ToString());
            Assert.AreEqual("9 0 ", m5.ToString());

            BinaryTree oldGoal = new BinaryTree(new Function("times", MarkupType.WaveFront));
            oldGoal.Add(new Variable("a", MarkupType.WaveFront));
            oldGoal.Add(new Function("plus", MarkupType.Skeleton));
            oldGoal.Right.Add(new Variable("a", MarkupType.Skeleton));
            oldGoal.Right.Add(new Function("plus", MarkupType.WaveFront));
            oldGoal.Right.Right.Add(new Variable("c", MarkupType.WaveFront));
            oldGoal.Right.Right.Add(new Variable("b", MarkupType.WaveFront));
            Measure m6 = new Measure(oldGoal);
            Assert.AreEqual("3 2 ", m6.ToString());

            BinaryTree newGiven = new BinaryTree(new Function("plus", MarkupType.Skeleton));
            newGiven.Add(new Function("times", MarkupType.WaveFront));
            newGiven.Left.Add(new Variable("a", MarkupType.WaveFront));
            newGiven.Left.Add(new Variable("a", MarkupType.Skeleton));
            newGiven.Add(new Function("plus", MarkupType.Skeleton));
            newGiven.Right.Add(new Function("times", MarkupType.WaveFront));
            newGiven.Right.Add(new Function("times", MarkupType.WaveFront));
            newGiven.Right.Left.Add(new Variable("a", MarkupType.WaveFront));
            newGiven.Right.Left.Add(new Variable("b", MarkupType.Skeleton));
            newGiven.Right.Right.Add(new Variable("a", MarkupType.WaveFront));
            newGiven.Right.Right.Add(new Variable("c", MarkupType.Skeleton));
            Measure m7 = new Measure(newGiven);
            Assert.AreEqual("4 2 0 ", m7.ToString());

            BinaryTree newGoal = new BinaryTree(new Function("times", MarkupType.WaveFront));
            newGoal.Add(new Variable("a", MarkupType.WaveFront));
            newGoal.Add(new Function("plus", MarkupType.Skeleton));
            newGoal.Right.Add(new Variable("a", MarkupType.Skeleton));
            newGoal.Right.Add(new Function("plus", MarkupType.Skeleton));
            newGoal.Right.Right.Add(new Variable("b", MarkupType.Skeleton));
            newGoal.Right.Right.Add(new Variable("c", MarkupType.Skeleton));
            Measure m8 = new Measure(newGoal);
            Assert.AreEqual("0 0 2 ", m8.ToString());

            BinaryTree t9 = new BinaryTree(new Function("plus", MarkupType.Skeleton));
            t9.Add(new Function("times", MarkupType.WaveFront));
            t9.Left.Add(new Variable("a", MarkupType.WaveFront));
            t9.Left.Add(new Variable("a", MarkupType.Skeleton));
            t9.Add(new Function("times", MarkupType.WaveFront));
            t9.Right.Add(new Variable("a", MarkupType.WaveFront));
            t9.Right.Add(new Function("plus", MarkupType.Skeleton));
            t9.Right.Right.Add(new Variable("b", MarkupType.Skeleton));
            t9.Right.Right.Add(new Variable("c", MarkupType.Skeleton));
            Measure m9 = new Measure(t9);
            Assert.AreEqual("0 4 0 ", m9.ToString());
            t9.Data.Markup = MarkupType.Undefined;

            try
            {
                Measure fail = new Measure(t9);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentException));
            }

            try
            {
                Measure fail = new Measure(null);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            }

            BinaryTree t10 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t10.Add(new Function("plus", MarkupType.WaveFront));
            t10.Left.Add(new Variable("a", MarkupType.WaveFront));
            t10.Left.Add(new Variable("b", MarkupType.WaveFront));
            t10.Add(new Function("plus", MarkupType.WaveFront));
            t10.Right.Add(new Variable("c", MarkupType.WaveFront));
            t10.Right.Add(new Variable("d", MarkupType.WaveFront));

            try
            {
                Measure m10 = new Measure(t10);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(InvalidOperationException));
            }

            BinaryTree t11 = new BinaryTree(new Function("plus", MarkupType.Skeleton));
            t11.Add(new Function("times", MarkupType.Skeleton));
            t11.Add(new Function("times", MarkupType.Skeleton));
            t11.Left.Add(new Function("plus", MarkupType.Skeleton));
            t11.Left.Add(new Variable("c2", MarkupType.WaveFront));
            t11.Left.Left.Add(new Variable("b2", MarkupType.WaveFront));
            t11.Left.Left.Add(new Variable("b1", MarkupType.WaveFront));
            t11.Right.Add(new Function("plus", MarkupType.WaveFront));
            t11.Right.Add(new Variable("d3", MarkupType.Skeleton));
            t11.Right.Left.Add(new Variable("b2", MarkupType.Skeleton));
            t11.Right.Left.Add(new Variable("b1", MarkupType.WaveFront));
            Measure m11 = new Measure(t11);
            Assert.AreEqual(4, m11.Count);
            Assert.AreEqual("2 3 0 0 ", m11.ToString());

            BinaryTree t12 = new BinaryTree(new Function("p", MarkupType.Skeleton));
            t12.Add(new Variable("a", MarkupType.Skeleton));
            t12.Add(new Function("f", MarkupType.Skeleton));
            t12.Right.Add(new Variable("b", MarkupType.WaveFront));
            t12.Right.Add(new Variable("c", MarkupType.WaveFront));
            Measure m12 = new Measure(t12);
            Assert.AreEqual("2 0 0 ", m12.ToString());
        }

        [TestMethod]
        public void MeasureTrivialTest()
        {
            BinaryTree t1 = new BinaryTree(new Constant("0", MarkupType.Skeleton));
            Measure m1 = new Measure(t1);
            Assert.AreEqual("0 ", m1.ToString());
        }

        [TestMethod]
        public void MeasureEqualsTest()
        {
            BinaryTree t1 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.WaveFront));
            t1.Left.Add(new Variable("a", MarkupType.Skeleton));
            t1.Left.Add(new Variable("b", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.Skeleton));
            t1.Right.Add(new Variable("c", MarkupType.Skeleton));
            t1.Right.Add(new Variable("d", MarkupType.Skeleton));
            Measure m = new Measure(t1);
            Assert.AreEqual(m.ToString(), "0 3 ");

            BinaryTree t2 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t2.Add(new Variable("a", MarkupType.WaveFront));
            t2.Add(new Function("plus", MarkupType.WaveFront));
            t2.Right.Add(new Variable("a", MarkupType.Skeleton));
            t2.Right.Add(new Function("plus", MarkupType.Skeleton));
            t2.Right.Right.Add(new Variable("b", MarkupType.Skeleton));
            t2.Right.Right.Add(new Variable("c", MarkupType.Skeleton));
            Measure m2 = new Measure(t2);
            Assert.AreEqual("0 3 ", m2.ToString());

            Assert.IsTrue(m2.Equals(m));
            Assert.IsTrue(m.Equals(m));
            Assert.IsTrue(m == m2);
            Assert.IsTrue(m != (m + m2));
            Assert.IsTrue(m != null);
            Assert.IsFalse(m != m2);
            Assert.IsFalse(m.Equals(null));
            Assert.IsFalse(m == null);
        }

        [TestMethod]
        public void MeasureOperatorsTest()
        {
            BinaryTree t1 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.WaveFront));
            t1.Left.Add(new Variable("a", MarkupType.Skeleton));
            t1.Left.Add(new Variable("b", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.Skeleton));
            t1.Right.Add(new Variable("c", MarkupType.Skeleton));
            t1.Right.Add(new Variable("d", MarkupType.Skeleton));
            Measure m = new Measure(t1);
            Assert.AreEqual(m.ToString(), "0 3 ");

            BinaryTree t2 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t2.Add(new Variable("a", MarkupType.WaveFront));
            t2.Add(new Function("plus", MarkupType.WaveFront));
            t2.Right.Add(new Variable("a", MarkupType.WaveFront));
            t2.Right.Add(new Function("plus", MarkupType.Skeleton));
            t2.Right.Right.Add(new Variable("b", MarkupType.Skeleton));
            t2.Right.Right.Add(new Variable("c", MarkupType.Skeleton));
            Measure m2 = new Measure(t2);
            Assert.AreEqual("0 4 ", m2.ToString());

            BinaryTree t3 = new BinaryTree(new Function("times", MarkupType.Skeleton));
            t3.Add(new Function("plus", MarkupType.Skeleton));
            t3.Left.Add(new Variable("a", MarkupType.Skeleton));
            t3.Left.Add(new Variable("b", MarkupType.WaveFront));
            t3.Add(new Function("plus", MarkupType.Skeleton));
            t3.Right.Add(new Variable("c", MarkupType.Skeleton));
            t3.Right.Add(new Variable("d", MarkupType.Skeleton));
            Measure m3 = new Measure(t3);
            Assert.AreEqual(m3.ToString(), "1 0 0 ");

            Assert.AreEqual("0 7 ", (m + m2).ToString());
            Assert.IsTrue(m2 > m);
            Assert.IsFalse(m > m2);
            Assert.IsTrue(m < m2);
            Assert.IsFalse(m2 < m);
            Assert.IsFalse(m3.IsEnd);
            Assert.IsTrue(m.IsEnd);
            Assert.IsTrue(m2.IsEnd);
            Assert.IsTrue((m + m2).IsEnd);
            Assert.IsTrue((m + m2).LastNumber == 7);
            Assert.IsTrue(m.Count == 2 && m2.Count == 2 && (m + m2).Count == 2);

            try
            {
                bool res = m3 > m;
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(InvalidOperationException));
            }

            try
            {
                bool res = m > m3;
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(InvalidOperationException));
            }

            try
            {
                Measure m4 = m + m3;
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(InvalidOperationException));
            }

            try
            {
                bool res = m > null;
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            }

            try
            {
                bool res = null > m;
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            }

            try
            {
                bool res = m < null;
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            }

            try
            {
                bool res = null < m;
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            }

            try
            {
                Measure res = null + m;
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            }

            try
            {
                Measure res = m + null;
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            }
        }

        [TestMethod]
        public void MeasureShrunkTest1()
        {
            BinaryTree t1 = new BinaryTree(new Function("p", MarkupType.Skeleton));
            t1.Add(new Function("h", MarkupType.WaveFront));
            t1.Left.Add(new Function("g", MarkupType.WaveFront));
            t1.Left.Left.Add(new Function("f", MarkupType.Skeleton));
            t1.Left.Left.Left.Add(new Variable("c", MarkupType.Skeleton));

            BinaryTree t2 = new BinaryTree(new Function("p", MarkupType.Skeleton));
            t2.Add(new Function("h", MarkupType.WaveFront));
            t2.Left.Add(new Function("g", MarkupType.Skeleton));
            t2.Left.Left.Add(new Function("f", MarkupType.Skeleton));
            t2.Left.Left.Left.Add(new Variable("c", MarkupType.Skeleton));
            Assert.AreEqual("0 1 0 ", Measure.GetShrunk(t1, t2, new Skeleton(t1), new Skeleton(t2)).ToString());
        }

        [TestMethod]
        public void MeasureShrunkTest2()
        {
            BinaryTree t1 = new BinaryTree(new Function("p", MarkupType.Skeleton));
            t1.Add(new Function("h", MarkupType.WaveFront));
            t1.Left.Add(new Function("g", MarkupType.WaveFront));
            t1.Left.Left.Add(new Function("f", MarkupType.Skeleton));
            t1.Left.Left.Left.Add(new Variable("c", MarkupType.Skeleton));

            BinaryTree t2 = t1.DeepCopy();
            Assert.AreEqual(new Measure(t1).ToString(), Measure.GetShrunk(t1, t2, new Skeleton(t1), new Skeleton(t2)).ToString());
        }

        [TestMethod]
        public void MeasureShrunkBundyDRAFTTest1()
        {
            BinaryTree oldGiven = new BinaryTree(new Function("plus", MarkupType.Skeleton));
            oldGiven.Add(new Function("times", MarkupType.WaveFront));
            oldGiven.Left.Add(new Variable("a", MarkupType.WaveFront));
            oldGiven.Left.Add(new Variable("a", MarkupType.Skeleton));
            oldGiven.Add(new Function("plus", MarkupType.WaveFront));
            oldGiven.Right.Add(new Function("times", MarkupType.WaveFront));
            oldGiven.Right.Add(new Function("times", MarkupType.WaveFront));
            oldGiven.Right.Left.Add(new Variable("a", MarkupType.WaveFront));
            oldGiven.Right.Left.Add(new Variable("b", MarkupType.WaveFront));
            oldGiven.Right.Right.Add(new Variable("a", MarkupType.WaveFront));
            oldGiven.Right.Right.Add(new Variable("c", MarkupType.WaveFront));

            BinaryTree oldGoal = new BinaryTree(new Function("times", MarkupType.WaveFront));
            oldGoal.Add(new Variable("a", MarkupType.WaveFront));
            oldGoal.Add(new Function("plus", MarkupType.Skeleton));
            oldGoal.Right.Add(new Variable("a", MarkupType.Skeleton));
            oldGoal.Right.Add(new Function("plus", MarkupType.WaveFront));
            oldGoal.Right.Right.Add(new Variable("c", MarkupType.WaveFront));
            oldGoal.Right.Right.Add(new Variable("b", MarkupType.WaveFront));

            BinaryTree newGiven = oldGiven.DeepCopy();
            newGiven.Right.Data.Markup = MarkupType.Skeleton;
            newGiven.Right.Left.Right.Data.Markup = MarkupType.Skeleton;
            newGiven.Right.Right.Right.Data.Markup = MarkupType.Skeleton;

            BinaryTree newGoal = new BinaryTree(new Function("times", MarkupType.WaveFront));
            newGoal.Add(new Variable("a", MarkupType.WaveFront));
            newGoal.Add(new Function("plus", MarkupType.Skeleton));
            newGoal.Right.Add(new Variable("a", MarkupType.Skeleton));
            newGoal.Right.Add(new Function("plus", MarkupType.Skeleton));
            newGoal.Right.Right.Add(new Variable("b", MarkupType.Skeleton));
            newGoal.Right.Right.Add(new Variable("c", MarkupType.Skeleton));

            Assert.AreEqual(2, new Measure(oldGiven).Count);
            Assert.AreEqual(2, new Measure(oldGoal).Count);
            Assert.AreEqual(3, new Measure(newGiven).Count);
            Assert.AreEqual(3, new Measure(newGoal).Count);
            Measure shrunkGoal = Measure.GetShrunk(oldGoal, newGoal, new Skeleton(oldGiven), new Skeleton(newGiven));
            Measure shrunkGiven = Measure.GetShrunk(oldGiven, newGiven, new Skeleton(oldGiven), new Skeleton(newGiven));
            Assert.AreEqual(2, shrunkGoal.Count);
            Assert.AreEqual(2, shrunkGiven.Count);
            Assert.AreEqual("6 2 ", (shrunkGiven + shrunkGoal).ToString());

            UnificationEntry oldEntry = new UnificationEntry(oldGiven, oldGoal);
            Assert.IsTrue(oldEntry.CompleteUnification());

            UnificationEntry newEntry = new UnificationEntry(newGiven, newGoal);
            Assert.IsTrue(newEntry.CompleteUnification());

            RipplingEntry oldRipplingEntry = new RipplingEntry(oldEntry);
            RipplingEntry newRipplingEntry = new RipplingEntry(newEntry);
            Assert.AreEqual((shrunkGiven + shrunkGoal).ToString(), Measure.GetShrunk(oldRipplingEntry, newRipplingEntry).ToString());
            return;
        }

        [TestMethod]
        public void MeasureShrunkBundyDRAFTTest2()
        {
            BinaryTree oldGiven = new BinaryTree(new Function("even", MarkupType.Skeleton));
            oldGiven.Add(new Constant("0", MarkupType.Skeleton));
 

            BinaryTree oldGoal = new BinaryTree(new Function("even", MarkupType.Skeleton));
            oldGoal.Add(new Function("s", MarkupType.WaveFront));
            oldGoal.Left.Add(new Constant("0", MarkupType.Skeleton));

            BinaryTree newGiven = new BinaryTree(new Function("even", MarkupType.Skeleton));
            newGiven.Add(new Function("s", MarkupType.WaveFront));
            newGiven.Left.Add(new Function("s", MarkupType.Skeleton));
            newGiven.Left.Left.Add(new Constant("0", MarkupType.Skeleton));

            BinaryTree newGoal = new BinaryTree(new Function("even", MarkupType.Skeleton));
            newGoal.Add(new Function("s", MarkupType.Skeleton));
            newGoal.Left.Add(new Constant("0", MarkupType.Skeleton));

            Assert.AreEqual(2, new Measure(oldGiven).Count);
            Assert.AreEqual(2, new Measure(oldGoal).Count);
            Assert.AreEqual(3, new Measure(newGiven).Count);
            Assert.AreEqual(3, new Measure(newGoal).Count);
            Measure shrunkGoal = Measure.GetShrunk(oldGoal, newGoal, new Skeleton(oldGiven), new Skeleton(newGiven));
            Measure shrunkGiven = Measure.GetShrunk(oldGiven, newGiven, new Skeleton(oldGiven), new Skeleton(newGiven));
            Assert.AreEqual(2, shrunkGoal.Count);
            Assert.AreEqual(2, shrunkGiven.Count);
            Assert.AreEqual("1 0 ", (shrunkGiven + shrunkGoal).ToString());

            UnificationEntry oldEntry = new UnificationEntry(oldGiven, oldGoal);
            Assert.IsTrue(oldEntry.CompleteUnification());

            UnificationEntry newEntry = new UnificationEntry(newGiven, newGoal);
            Assert.IsTrue(newEntry.CompleteUnification());

            RipplingEntry oldRipplingEntry = new RipplingEntry(oldEntry);
            RipplingEntry newRipplingEntry = new RipplingEntry(newEntry);
            Assert.AreEqual((shrunkGiven + shrunkGoal).ToString(), Measure.GetShrunk(oldRipplingEntry, newRipplingEntry).ToString());
            return;
        }
    }
}
