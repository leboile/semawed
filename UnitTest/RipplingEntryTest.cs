﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Semawed;
namespace UnitTest
{
    [TestClass]
    public class RipplingEntryTest
    {
        [TestMethod]
        public void RipplingEntryCtorTest()
        {
            BinaryTree t1 = new BinaryTree(new Function("times", MarkupType.Skeleton));
            t1.Add(new Function("plus", MarkupType.WaveFront));
            t1.Left.Add(new Variable("a", MarkupType.WaveFront));
            t1.Left.Add(new Variable("b", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.Skeleton));
            t1.Right.Add(new Variable("c", MarkupType.Skeleton));
            t1.Right.Add(new Variable("d", MarkupType.Skeleton));

            BinaryTree t2 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t2.Add(new Function("plus", MarkupType.WaveFront));
            t2.Left.Add(new Variable("a", MarkupType.WaveFront));
            t2.Left.Add(new Variable("b", MarkupType.WaveFront));
            t2.Add(new Function("times", MarkupType.Skeleton));
            t2.Right.Add(new Function("plus", MarkupType.Skeleton));
            t2.Right.Left.Add(new Variable("c", MarkupType.Skeleton));
            t2.Right.Left.Add(new Variable("d", MarkupType.Skeleton));
            t2.Right.Add(new Variable("a", MarkupType.WaveFront));
            UnificationEntry UE = new UnificationEntry(t1, t2);
            try
            {
                RipplingEntry failEntry = new RipplingEntry(UE);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(InvalidOperationException));
            }

            try
            {
                RipplingEntry failEntry = new RipplingEntry(null);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            }

            Assert.IsTrue(UE.CompleteUnification());
            RipplingEntry testEntry = new RipplingEntry(UE);
            Assert.ReferenceEquals(testEntry.RelatedUnificationEntry, UE);
            Assert.AreEqual(t1, testEntry.GivenRoot);
            Assert.AreEqual(t2, testEntry.GoalRoot);
            Assert.AreEqual(new Measure(t1) + new Measure(t2), testEntry.CommonMeasure);
            HashSet<Element> commonElements = new HashSet<Element>();
            commonElements.Add(new Function("times"));
            commonElements.Add(new Function("plus"));
            commonElements.Add(new Variable("a"));
            commonElements.Add(new Variable("b"));
            commonElements.Add(new Variable("c"));
            commonElements.Add(new Variable("d"));
            commonElements.SymmetricExceptWith(testEntry.GetCommonElements());
            Assert.AreEqual(0, commonElements.Count);
        }

        [TestMethod]
        public void RipplingEntrySkeletonEnlargementsTest()
        {
            BinaryTree t1 = new BinaryTree(new Function("times", MarkupType.Skeleton));
            t1.Add(new Function("plus", MarkupType.WaveFront));
            t1.Left.Add(new Variable("a", MarkupType.WaveFront));
            t1.Left.Add(new Variable("b", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.Skeleton));
            t1.Right.Add(new Variable("c", MarkupType.Skeleton));
            t1.Right.Add(new Variable("d", MarkupType.Skeleton));
            Skeleton sk = new Skeleton(t1);

            BinaryTree t2 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t2.Add(new Function("plus", MarkupType.WaveFront));
            t2.Left.Add(new Variable("a", MarkupType.WaveFront));
            t2.Left.Add(new Variable("b", MarkupType.WaveFront));
            t2.Add(new Function("times", MarkupType.Skeleton));
            t2.Right.Add(new Function("plus", MarkupType.Skeleton));
            t2.Right.Left.Add(new Variable("c", MarkupType.Skeleton));
            t2.Right.Left.Add(new Variable("d", MarkupType.Skeleton));
            t2.Right.Add(new Variable("a", MarkupType.WaveFront));

            UnificationEntry UE = new UnificationEntry(t1, t2);
            Assert.IsTrue(UE.CompleteUnification());
            RipplingEntry testEntry = new RipplingEntry(UE);
            IReadOnlyList<RipplingEntry> skeletonEnlargements = testEntry.GetSkeletonEnlargements();
            Assert.IsTrue(testEntry.GetCommonElements().Contains(new Variable("a")));
            //Assert.AreEqual(5, skeletonEnlargements.Count);

            BinaryTree t3 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t3.Add(new Function("plus", MarkupType.WaveFront));
            t3.Left.Add(new Variable("a", MarkupType.Skeleton));
            t3.Left.Add(new Variable("b", MarkupType.WaveFront));
            t3.Add(new Function("plus", MarkupType.Skeleton));
            t3.Right.Add(new Variable("c", MarkupType.Skeleton));
            t3.Right.Add(new Variable("d", MarkupType.Skeleton));

            BinaryTree t4 = t3.DeepCopy();
            UnificationEntry UE2 = new UnificationEntry(t3, t4);
            Assert.IsTrue(UE2.CompleteUnification());
            RipplingEntry testEntry2 = new RipplingEntry(UE2);
            IReadOnlyList<RipplingEntry> skeletonEnlargements2 = testEntry2.GetSkeletonEnlargements();
            Assert.AreEqual(4, skeletonEnlargements2.Count);
            foreach (RipplingEntry entry in skeletonEnlargements2)
            {
                Assert.IsTrue(t4 == entry.GivenRoot && t4 == entry.GoalRoot);//cause change in Markup doesnt lead to BinaryTree inequality
                Assert.IsTrue(entry.IsSkeletonPreserving);
            }

        }
    }
}
