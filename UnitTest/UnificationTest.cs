﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Semawed;
namespace UnitTest
{
    [TestClass]
    public class UnificationTest
    {
        string txt7 = "<math><apply><plus/><apply><plus/><ci>a</ci><ci>b</ci></apply><ci>c</ci></apply></math>";
        string txt8 = "<math><apply><plus/><ci>a</ci><apply><plus/><ci>b</ci><ci>c</ci></apply></apply></math>";
    
        [TestMethod]
        public void DifferenceUnificationNoRestrictionsTest()
        {
            IReadOnlyList<UnificationEntry> unifs = Unification.Annotate(new Expression(txt7, false), new Expression(txt8, false), UnificationOption.NoRestrictions);
            //List<UnificationEntry> unifs = Markup.Annotate(new Expression(txt7,true), new Expression(txt8,true), false);
            Console.WriteLine(unifs.Count);
            int minimalCost = int.MaxValue;
            int count = 0;
            foreach (UnificationEntry UE in unifs)
            {
                count++;
                if (UE.WRCost < minimalCost)
                    minimalCost = UE.WRCost;

                Assert.IsTrue(UE.CompleteUnification() && (new Skeleton(UE.GoalRoot) == new Skeleton(UE.GivenRoot)));
            }
            Console.WriteLine("Minimal cost: " + minimalCost);
        }

        [TestMethod]
        public void DifferenceUnificationSubstitutionMinimalTest()
        {
           IReadOnlyList<UnificationEntry> unifs = Unification.Annotate(new Expression(txt7, false), new Expression(txt8, false), UnificationOption.SubstitutionMinimal);
            //List<UnificationEntry> unifs = Markup.Annotate(new Expression(txt7,true), new Expression(txt8,true), false);
            Console.WriteLine(unifs.Count);
            int minimalCost = int.MaxValue;
            int count = 0;
            foreach (UnificationEntry UE in unifs)
            {
                count++;
                if (UE.WRCost < minimalCost)
                    minimalCost = UE.WRCost;

                Assert.IsTrue(UE.CompleteUnification() && (new Skeleton(UE.GoalRoot) == new Skeleton(UE.GivenRoot)));
            }
            Console.WriteLine("Minimal cost: " + minimalCost);
        }

        [TestMethod]
        public void DifferenceUnificationCostMinimalTest()
        {
            IReadOnlyList<UnificationEntry> unifs = Unification.Annotate(new Expression(txt7, false), new Expression(txt8, false), UnificationOption.CostMinimal);
            //List<UnificationEntry> unifs = Markup.Annotate(new Expression(txt7,true), new Expression(txt8,true), false);
            Console.WriteLine(unifs.Count);
            int minimalCost = int.MaxValue;
            int count = 0;
            foreach (UnificationEntry UE in unifs)
            {
                count++;
                if (UE.WRCost < minimalCost)
                    minimalCost = UE.WRCost;

                Assert.IsTrue(UE.CompleteUnification() && (new Skeleton(UE.GoalRoot) == new Skeleton(UE.GivenRoot)));
            }
            Console.WriteLine("Minimal cost: " + minimalCost);
        }

        [TestMethod]
        public void DifferenceUnificationCostAndSubstitutionMinimalTest()
        {
            IReadOnlyList<UnificationEntry> unifs = Unification.Annotate(new Expression(txt7, false), new Expression(txt8, false), UnificationOption.CostAndSubstitutionMinimal);
            //List<UnificationEntry> unifs = Markup.Annotate(new Expression(txt7,true), new Expression(txt8,true), false);
            Console.WriteLine(unifs.Count);
            int minimalCost = int.MaxValue;
            int count = 0;
            foreach (UnificationEntry UE in unifs)
            {
                count++;
                if (UE.WRCost < minimalCost)
                    minimalCost = UE.WRCost;
                Console.WriteLine(UE.ToString());
                Assert.IsTrue(UE.CompleteUnification() && (new Skeleton(UE.GoalRoot) == new Skeleton(UE.GivenRoot)));
            }
            Console.WriteLine("Minimal cost: " + minimalCost);
        }
    }
}
