﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Semawed;
namespace UnitTest
{
    [TestClass]
    public class SkeletonTest
    {

        [TestMethod]
        public void SkeletonCtorsAndEqualsTest()
        {
            BinaryTree t1 = new BinaryTree(new Function("plus", MarkupType.Skeleton));
            t1.Add(new Function("plus", MarkupType.WaveFront));
            t1.Left.Add(new Variable("a00", MarkupType.WaveFront));
            t1.Left.Add(new Variable("a10", MarkupType.Skeleton));
            t1.Add(new Variable("b11", MarkupType.Skeleton));
            Skeleton sk1 = new Skeleton(t1);

            BinaryTree t2 = new BinaryTree(new Function("plus", MarkupType.Skeleton));
            t2.Add(new Variable("a10", MarkupType.Skeleton));
            t2.Add(new Function("plus", MarkupType.WaveFront));
            t2.Right.Add(new Variable("b10", MarkupType.WaveFront));
            t2.Right.Add(new Variable("b10", MarkupType.WaveFront));
            Skeleton sk2 = new Skeleton(t2);

            BinaryTree t3 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t3.Add(new Function("plus", MarkupType.WaveFront));
            t3.Left.Add(new Variable("a", MarkupType.Skeleton));
            t3.Left.Add(new Variable("b", MarkupType.WaveFront));
            t3.Add(new Function("plus", MarkupType.Skeleton));
            t3.Right.Add(new Variable("c", MarkupType.Skeleton));
            t3.Right.Add(new Variable("d", MarkupType.Skeleton));
            Skeleton sk3 = new Skeleton(t3);

            BinaryTree t4 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t4.Add(new Variable("a", MarkupType.WaveFront));
            t4.Add(new Function("plus", MarkupType.Skeleton));
            t4.Right.Add(new Variable("a", MarkupType.Skeleton));
            t4.Right.Add(new Function("plus", MarkupType.WaveFront));
            t4.Right.Right.Add(new Variable("c", MarkupType.Skeleton));
            t4.Right.Right.Add(new Variable("d", MarkupType.Skeleton));
            Skeleton sk4 = new Skeleton(t4);
  
            BinaryTree t5 = new BinaryTree(new Function("times", MarkupType.Skeleton));
            t5.Add(new Function("plus", MarkupType.WaveFront));
            t5.Left.Add(new Variable("a", MarkupType.WaveFront));
            t5.Left.Add(new Variable("b", MarkupType.WaveFront));
            t5.Add(new Function("plus", MarkupType.Skeleton));
            t5.Right.Add(new Variable("c", MarkupType.Skeleton));
            t5.Right.Add(new Variable("d", MarkupType.Skeleton));
            Skeleton sk5 = new Skeleton(t5);

            BinaryTree t6 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t6.Add(new Function("plus", MarkupType.WaveFront));
            t6.Left.Add(new Variable("a", MarkupType.WaveFront));
            t6.Left.Add(new Variable("b", MarkupType.WaveFront));
            t6.Add(new Function("times", MarkupType.Skeleton));
            t6.Right.Add(new Function("plus", MarkupType.Skeleton));
            t6.Right.Left.Add(new Variable("c", MarkupType.Skeleton));
            t6.Right.Left.Add(new Variable("d", MarkupType.Skeleton));
            t6.Right.Add(new Variable("a", MarkupType.WaveFront));
            Skeleton sk6 = new Skeleton(t6);
            Assert.AreEqual("timesSkeletonplusSkeletoncSkeletondSkeleton-----------", sk6.ToString().Replace(" ", "").Replace("\n", "").Replace("\r", ""));
            Assert.AreEqual("timesSkeletonplusSkeletoncSkeletondSkeleton-----------", sk5.ToString().Replace(" ", "").Replace("\n", "").Replace("\r", ""));
            //Assert.IsTrue(sk4.Equals(sk3));
            //Assert.IsTrue(sk4 == sk3);
            Assert.IsTrue(sk1 != null);
            Assert.IsTrue(null != sk2);
            //Assert.IsFalse(sk3 != sk4);
            Assert.IsFalse(sk1.Equals(sk2));
            Assert.IsTrue(sk5.Equals(sk6));
            Assert.IsTrue(sk6 == sk5);
            Assert.IsFalse(sk5 != sk6);

            //wrong depth was calculated on this one
            //BinaryTree t7 = new BinaryTree(new Function("p", MarkupType.Skeleton));
            //t7.Add(new Variable("a", MarkupType.Skeleton));
            //t7.Add(new Function("f", MarkupType.Skeleton));
            //t7.Right.Add(new Variable("b", MarkupType.WaveFront));
            //t7.Right.Add(new Variable("c", MarkupType.WaveFront));
            //Skeleton sk7 = new Skeleton(t7);
            //Assert.AreEqual(3, sk7.GetDepth());
        }

        [TestMethod]
        public void EqualSkeletonsButNotRecognizedAsOne()
        {
            //BinaryTree t1 = new BinaryTree(new Function("times", MarkupType.Skeleton));
            //t1.Add(new Function("plus", MarkupType.WaveFront));
            //t1.Left.Add(new Variable("a", MarkupType.Skeleton));
            //t1.Left.Add(new Variable("b", MarkupType.WaveFront));
            //t1.Add(new Function("plus", MarkupType.Skeleton));
            //t1.Right.Add(new Variable("c", MarkupType.Skeleton));
            //t1.Right.Add(new Variable("d", MarkupType.Skeleton));
            //Skeleton sk = new Skeleton(t1);
            //Console.WriteLine(sk.GetDepth().ToString());

            //BinaryTree t2 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            //t2.Add(new Function("plus", MarkupType.WaveFront));
            //t2.Left.Add(new Variable("a", MarkupType.WaveFront));
            //t2.Left.Add(new Variable("b", MarkupType.WaveFront));
            //t2.Add(new Function("times", MarkupType.Skeleton));
            //t2.Right.Add(new Function("plus", MarkupType.Skeleton));
            //t2.Right.Left.Add(new Variable("c", MarkupType.Skeleton));
            //t2.Right.Left.Add(new Variable("d", MarkupType.Skeleton));
            //t2.Right.Add(new Variable("a", MarkupType.Skeleton));


            //BinaryTree t3 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            //t3.Add(new Function("plus", MarkupType.WaveFront));
            //t3.Left.Add(new Variable("a", MarkupType.Skeleton));
            //t3.Left.Add(new Variable("b", MarkupType.WaveFront));
            //t3.Add(new Function("plus", MarkupType.Skeleton));
            //t3.Right.Add(new Variable("c", MarkupType.Skeleton));
            //t3.Right.Add(new Variable("d", MarkupType.Skeleton));
            //Skeleton sk3 = new Skeleton(t3);

            //BinaryTree t4 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            //t4.Add(new Variable("a", MarkupType.WaveFront));
            //t4.Add(new Function("plus", MarkupType.Skeleton));
            //t4.Right.Add(new Variable("a", MarkupType.Skeleton));
            //t4.Right.Add(new Function("plus", MarkupType.WaveFront));
            //t4.Right.Right.Add(new Variable("c", MarkupType.Skeleton));
            //t4.Right.Right.Add(new Variable("d", MarkupType.Skeleton));
            //Skeleton sk4 = new Skeleton(t4);
        }
    }
}
