﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Semawed;
namespace UnitTest
{
    [TestClass]
    public class UnificationEntryTest
    {
        [TestMethod]
        public void UECtorsTest()
        {
            BinaryTree t1 = new BinaryTree(new Function("times", MarkupType.Skeleton));
            t1.Add(new Function("plus", MarkupType.WaveFront));
            t1.Left.Add(new Variable("a", MarkupType.WaveFront));
            t1.Left.Add(new Variable("b", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.Skeleton));
            t1.Right.Add(new Variable("c", MarkupType.Skeleton));
            t1.Right.Add(new Variable("d", MarkupType.Skeleton));
            Skeleton sk1 = new Skeleton(t1);
            Measure m1 = new Measure(t1);

            BinaryTree t2 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t2.Add(new Function("plus", MarkupType.WaveFront));
            t2.Left.Add(new Variable("a", MarkupType.WaveFront));
            t2.Left.Add(new Variable("b", MarkupType.WaveFront));
            t2.Add(new Function("times", MarkupType.Skeleton));
            t2.Right.Add(new Function("plus", MarkupType.Skeleton));
            t2.Right.Left.Add(new Variable("c", MarkupType.Skeleton));
            t2.Right.Left.Add(new Variable("d", MarkupType.Skeleton));
            t2.Right.Add(new Variable("a", MarkupType.WaveFront));
            Skeleton sk2 = new Skeleton(t2);
            Measure m2 = new Measure(t2);

            UnificationEntry entry = new UnificationEntry(t1, t2);
            //try
            //{
            //    Measure fail = entry.CommonMeasure;
            //    Assert.Fail();
            //}
            //catch (Exception ex)
            //{
            //    Assert.IsInstanceOfType(ex, typeof(InvalidOperationException));
            //}

            try
            {
                Skeleton fail = entry.GivenSkeleton;
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(InvalidOperationException));
            }

            try
            {
                Skeleton fail = entry.GoalSkeleton;
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(InvalidOperationException));
            }

            try
            {
                UnificationEntry fail = new UnificationEntry(t1,null);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            }

            try
            {
                UnificationEntry fail = new UnificationEntry(null, t1);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            }

            Assert.IsTrue(entry.CompleteUnification());
            //Assert.IsTrue(entry.CommonMeasure == (m1 + m2));
            Assert.IsTrue(entry.GivenSkeleton.Equals(sk1));
            Assert.IsTrue(entry.GoalSkeleton.Equals(sk2));
            Assert.IsTrue(entry.GoalRoot == t2);
            Assert.IsTrue(entry.GivenRoot == t1);
            Assert.IsTrue(entry.UnificationComplete == entry.CompleteUnification());

            BinaryTree t3 = new BinaryTree(new Function("p", MarkupType.Skeleton));
            t3.Add(new Function("h", MarkupType.WaveFront));
            t3.Left.Add(new Function("g", MarkupType.Skeleton));
            t3.Left.Left.Add(new Function("f", MarkupType.Skeleton));
            t3.Left.Left.Left.Add(new Variable("c", MarkupType.Skeleton));
            UnificationEntry nonSkPreserving = new UnificationEntry(t1, t3);
            Assert.IsFalse(nonSkPreserving.CompleteUnification());
            Assert.IsFalse(nonSkPreserving.UnificationComplete);
        }
    }
}
