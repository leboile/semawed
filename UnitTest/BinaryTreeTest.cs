﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Semawed;

namespace UnitTest
{
    [TestClass]
    public class BinaryTreeTest
    {
        [TestMethod]
        public void BinaryTreeConstructors()
        {
            try
            {
                BinaryTree t = new BinaryTree(null);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            }

            BinaryTree blankTree = new BinaryTree(new Variable("test"));
            Assert.IsTrue(blankTree.Data.Equals(new Variable("test")));
            Assert.IsFalse(blankTree.IsLeftNode);
            Assert.IsFalse(blankTree.IsRightNode);
            Assert.IsNull(blankTree.Left);
            Assert.IsNull(blankTree.Right);
            Assert.IsNull(blankTree.Parent);
            Assert.IsNull(blankTree.Sibling);
            Assert.IsTrue(Assert.ReferenceEquals(blankTree, blankTree.Root));
            Assert.AreEqual(0, blankTree.Depth);
            Assert.AreEqual(1, blankTree.GetTreeInfo().Depth);
        }

        [TestMethod]
        public void BinaryTreeAddTest()
        {
            BinaryTree blankTree = new BinaryTree(new Variable("test"));
            try
            {
                blankTree.Add(blankTree.Data);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(InvalidOperationException));
            }

            BinaryTree notBlankTree = new BinaryTree(new Function("test"));
            try
            {
                notBlankTree.Add(null);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentException));
            }

            int first = notBlankTree.Add(new Variable("t1"));
            int second = notBlankTree.Add(new Variable("t2"));
            int third = notBlankTree.Add(new Variable("t3"));
            Assert.IsTrue(first == -1 && second == 1 && third == 0);
            Assert.IsFalse(notBlankTree.Right.IsLeftNode);
            Assert.IsFalse(notBlankTree.Left.IsRightNode);
            Assert.IsTrue(notBlankTree.Left.IsLeftNode);
            Assert.IsTrue(notBlankTree.Right.IsRightNode);
            Assert.IsNotNull(notBlankTree.Left);
            Assert.IsNotNull(notBlankTree.Right);
            Assert.IsTrue(Assert.ReferenceEquals(notBlankTree.Left.Parent, notBlankTree));
            Assert.IsTrue(Assert.ReferenceEquals(notBlankTree.Right.Parent, notBlankTree));
            Assert.IsTrue(Assert.ReferenceEquals(notBlankTree.Left.Sibling, notBlankTree.Right));
            Assert.IsTrue(Assert.ReferenceEquals(notBlankTree.Right.Sibling, notBlankTree.Left));
            Assert.IsTrue(Assert.ReferenceEquals(notBlankTree.Left.Parent, notBlankTree));
        }

        [TestMethod]
        public void BinaryTreeGuaranteedAddTest()
        {
            BinaryTree blankTree = new BinaryTree(new Variable("test"));
            try
            {
                blankTree.GuaranteedAdd(blankTree.Data);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(InvalidOperationException));
            }

            BinaryTree notBlankTree = new BinaryTree(new Function("test"));
            try
            {
                notBlankTree.Add(null);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentException));
            }
            BinaryTree l = notBlankTree.GuaranteedAdd(new Variable("t1"));
            BinaryTree r = notBlankTree.GuaranteedAdd(new Variable("t2"));
            Assert.AreEqual(notBlankTree.Left, l);
            Assert.AreEqual(notBlankTree.Right, r);
            BinaryTree addedNode = notBlankTree.GuaranteedAdd(new Variable("t3"));
            Assert.AreEqual(addedNode.Data, new Variable("t3"));
            Assert.IsTrue(addedNode.Sibling.Data.Equals(new Variable("t2")));
            Assert.IsTrue(Assert.ReferenceEquals(addedNode.Sibling, r));
            Assert.IsTrue(addedNode.Parent.Data == addedNode.Root.Data);
        }

        [TestMethod]
        public void BinaryTreePathTest()
        {
            BinaryTree t1 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.WaveFront));
            t1.Left.Add(new Variable("a", MarkupType.Skeleton));
            t1.Left.Add(new Variable("b", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.Skeleton));
            t1.Right.Add(new Variable("c", MarkupType.Skeleton));
            t1.Right.Add(new Variable("d", MarkupType.Skeleton));

            Assert.IsTrue(Assert.ReferenceEquals(t1.Left.Right.Root, t1));
            Assert.IsTrue(Assert.ReferenceEquals(t1.Root, t1));
            Assert.IsTrue(Assert.ReferenceEquals(t1.Left.Right, t1.Left.Left.Sibling));
            Assert.IsTrue(Assert.ReferenceEquals(t1.Left.Left, t1.Left.Right.Sibling));
            Assert.IsTrue(Assert.ReferenceEquals(t1.Left, t1.Right.Sibling));
            Assert.IsTrue(Assert.ReferenceEquals(t1.Left.Left, t1.GetNodeUsingPath(t1.Left.Left.GetPathFromRoot())));
            Assert.IsTrue(Assert.ReferenceEquals(t1.Right.Left, t1.GetNodeUsingPath(t1.Right.Left.GetPathFromRoot())));
            Assert.IsTrue(Assert.ReferenceEquals(t1, t1.GetNodeUsingPath(t1.GetPathFromRoot())));
            Assert.IsNull(t1.GetNodeUsingPath(null));

            try
            {
                bool[] fakePath = {false,false,true,true};
                t1.GetNodeUsingPath(fakePath);
                Assert.Fail();
            }
            catch(Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentException));
            }
        }

        [TestMethod]
        public void BinaryTreeEqualsTest()
        {
            BinaryTree t1 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.WaveFront));
            t1.Left.Add(new Variable("a", MarkupType.Skeleton));
            t1.Left.Add(new Variable("b", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.Skeleton));
            t1.Right.Add(new Variable("c", MarkupType.Skeleton));
            t1.Right.Add(new Variable("d", MarkupType.Skeleton));

            BinaryTree t2 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t2.Add(new Function("plus", MarkupType.WaveFront));
            t2.Left.Add(new Variable("a", MarkupType.Skeleton));
            t2.Left.Add(new Variable("b", MarkupType.WaveFront));
            t2.Add(new Function("plus", MarkupType.Skeleton));
            t2.Right.Add(new Variable("c", MarkupType.Skeleton));
            t2.Right.Add(new Variable("d", MarkupType.Skeleton));

            BinaryTree t3 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t3.Add(new Function("times", MarkupType.WaveFront));
            t3.Left.Add(new Variable("a", MarkupType.Skeleton));
            t3.Left.Add(new Variable("b", MarkupType.WaveFront));
            t3.Add(new Function("plus", MarkupType.Skeleton));
            t3.Right.Add(new Variable("c", MarkupType.Skeleton));
            t3.Right.Add(new Variable("d", MarkupType.Skeleton));
            Assert.IsTrue(t1.Equals(t2));
            Assert.IsTrue(t2.Equals(t1));
            Assert.IsFalse(t1.Equals(t3));
            Assert.IsFalse(t1.Equals(null));
            Assert.IsTrue(t1 == t2);
            Assert.IsFalse(t1 == null);
            Assert.IsTrue(t1 != t3);
            Assert.IsTrue(t1 != null);
            Assert.IsTrue(t1.Equals(t1));
        }

        [TestMethod]
        public void BinaryTreeDeepCopyTest()
        {
            BinaryTree t1 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.WaveFront));
            t1.Left.Add(new Variable("a", MarkupType.Skeleton));
            t1.Left.Add(new Variable("b", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.Skeleton));
            t1.Right.Add(new Variable("c", MarkupType.Skeleton));
            t1.Right.Add(new Variable("d", MarkupType.Skeleton));
            BinaryTree foo;
            Assert.AreEqual(t1.Left.Left.Data, t1.Left.Left.DeepCopyWholeTree(out foo).Data);
            Assert.AreEqual(t1.Right.Left.Data, t1.Right.Left.DeepCopyWholeTree(out foo).Data);
            Assert.AreEqual(t1.Left.Right.Data, t1.Left.Right.DeepCopyWholeTree(out foo).Data);
            Assert.AreNotSame(t1.Left.Right, t1.Left.Right.DeepCopyWholeTree(out foo));
            Assert.IsTrue(Assert.ReferenceEquals(t1.DeepCopyWholeTree(out foo), foo));
            BinaryTree t2 = t1.DeepCopy();
            Assert.AreEqual(t1, t2);
        }

        [TestMethod]
        public void BinaryTreeGetTest()
        {
            BinaryTree t1 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.WaveFront));
            t1.Left.Add(new Variable("a", MarkupType.Skeleton));
            t1.Left.Add(new Variable("b", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.Skeleton));
            t1.Right.Add(new Variable("c", MarkupType.Skeleton));
            t1.Right.Add(new Variable("d", MarkupType.Skeleton));
            Assert.AreEqual(1, t1.Left.Depth);
            HashSet<Element> elems = t1.GetAllElements();
            Assert.IsTrue(elems.Count == 6);
            HashSet<Variable> vars = t1.GetAllVariables();
            Assert.IsTrue(vars.Count == 4);
            BinaryTreeInfo info = t1.GetTreeInfo();
            Assert.AreEqual(3, info.Depth);
            Assert.IsTrue(info.SkeletonCount == 4);
            Assert.IsTrue(info.WFCount == 3);
            Assert.IsTrue(info.UndefinedCount == 0);
            Assert.IsTrue(info.Total == 7);
        }

        [TestMethod]
        public void BinaryTreeActualDepthTest()
        {
            BinaryTree t1 = new BinaryTree(new Function("times", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.WaveFront));
            t1.Left.Add(new Variable("a", MarkupType.Skeleton));
            t1.Left.Add(new Variable("b", MarkupType.WaveFront));
            BinaryTree node1 = t1.Left.GuaranteedAdd(new Variable("c", MarkupType.WaveFront));
            t1.Add(new Function("plus", MarkupType.Skeleton));
            t1.Right.Add(new Variable("d", MarkupType.Skeleton));
            t1.Right.Add(new Variable("e", MarkupType.Skeleton));
            BinaryTree node2 = t1.Right.GuaranteedAdd(new Variable("f", MarkupType.Skeleton));
            Console.WriteLine(t1.ToPlainString());
            Assert.AreEqual(3, node1.Depth);
            Assert.AreEqual(3, node2.Depth);
            Assert.AreEqual(2, node1.ActualDepth);
            Assert.AreEqual(2, node2.ActualDepth);
        }
    }
}
