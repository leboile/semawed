# README #

http://istina.msu.ru/media/publications/article/94a/7ce/8516453/thesis.pdf (Russian)

You can try this search system in action: http://195.209.147.206:8080/ 

Searched is performed on relatively small subset of cs:IR subject area of Cornell University Library arXiv.org. The "showstopper" of search quality (not performance) becomes the un-ability to perform presentation-to-semantic math conversion by automatic means which leads to catastrophically low quality of indexed math expressions. For example, math expression U(a) is transformed to U * a (U times a) by automatic conversion tool LateXML (http://dlmf.nist.gov/LaTeXML/).

### Solution Structure ###
-FrontEnd is an ASP.NET application, which is a web front-end of search system. I was not really trying to do "product" search front-end, all it needed is to be functional and quite presentable. FrontEnd is responsible for user input of queries and passing it to search service.

-ServiceHost is an Windows Client Foundation based service which is ought to provide responses to FE queries by usage of math proof engine on database with search index. 

-Semawed is a math proof engine based on Rippling formal proof heuristics. It is compiled as an linked library which exposes Rippling.PerformProof method responsible for proof attempt of equality of two math expressions. If attempt is successful then 1.0 double value is returned, but if attempt was not successful (most of the time it is so) then double value between 0.0 and 1.0 is returned, which is indicating the "degree" of two math expressions likeness.


(c) Roman Shirokiy, 2015. Any commercial usage of this project is prohibited and no warranty is granted.